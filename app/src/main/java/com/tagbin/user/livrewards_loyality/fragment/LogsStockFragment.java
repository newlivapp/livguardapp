package com.tagbin.user.livrewards_loyality.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.GetStockModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.adapter.StockRVAdapter;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nav on 24-Jul-17.
 */

public class LogsStockFragment extends Fragment {
    final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    final Date date = new Date();
    Context context;
    TextView current_date;
    RecyclerView stock_rv;
    StockRVAdapter stockRVAdapter;
    List<GetStockModel> stockList;
    String todaydate = dateFormat.format(date);
    SwipeRefreshLayout swipeRefreshLayout;
    RelativeLayout empty_rl;
    String userid;
    RequestListener stockListner = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response GetStock", responseObject.toString());
            stockList.clear();
            Type collectionType = new TypeToken<List<GetStockModel>>() {
            }.getType();
            List<GetStockModel> ca = (List<GetStockModel>) new Gson().fromJson(responseObject.toString(), collectionType);
            stockList.addAll(ca);
            if (getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (stockList.isEmpty()) {
                        empty_rl.setVisibility(View.VISIBLE);
                        stock_rv.setVisibility(View.GONE);
                    } else {
                        empty_rl.setVisibility(View.GONE);
                        stock_rv.setVisibility(View.VISIBLE);
                    }
                    swipeRefreshLayout.setRefreshing(false);
                    stockRVAdapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    if ((Activity) getContext() != null && getContext() != null)
                        ((Activity) getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (swipeRefreshLayout != null)
                                    swipeRefreshLayout.setRefreshing(false);
                                Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();
                            }
                        });
                } else if (errorCode == 401) {
                    if ((Activity) getContext() != null && getContext() != null)
                        ((Activity) getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (swipeRefreshLayout != null)
                                    swipeRefreshLayout.setRefreshing(false);
                                // logout();
                            }
                        });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    if (((Activity) getContext()) != null && getContext() != null) {
                        ((Activity) getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (swipeRefreshLayout != null)
                                    swipeRefreshLayout.setRefreshing(false);
                                if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                    Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getContext(), "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }

            } else {
                if (getActivity() != null && getContext() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (swipeRefreshLayout != null)
                                swipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }
    };

    @SuppressLint("ValidFragment")
    public LogsStockFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragmnet_logs_stock, container, false);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stock_rv = (RecyclerView) getView().findViewById(R.id.stock_rv);
        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swiperefresh);
        empty_rl = (RelativeLayout) getView().findViewById(R.id.empty_rl);
        stockList = new ArrayList<GetStockModel>();
        final PrefManager pref = new PrefManager(getContext());
        userid= String.valueOf(pref.getLoginModel().getData().getId());
        Controller.GetStock(getContext(),userid,stockListner);
        stockRVAdapter = new StockRVAdapter(getContext(), stockList);
        stock_rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        stock_rv.setAdapter(stockRVAdapter);
        stock_rv.setHasFixedSize(true);
        current_date = (TextView) getView().findViewById(R.id.current_date);
        current_date.setText("Stock as on : " + todaydate);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Controller.GetStock(getContext(),userid, stockListner);
            }
        });

    }
}
