package com.tagbin.user.livrewards_loyality.Model;

/**
 * Created by Nav on 01-Aug-17.
 */

public class GetStockModel {
    int count_product;
    String product__product;

    public int getCount_product() {
        return count_product;
    }

    public void setCount_product(int count_product) {
        this.count_product = count_product;
    }

    public String getProduct__product() {
        return product__product;
    }

    public void setProduct__product(String product__product) {
        this.product__product = product__product;
    }
}
