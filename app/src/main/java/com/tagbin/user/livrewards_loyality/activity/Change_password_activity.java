package com.tagbin.user.livrewards_loyality.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.LoginModel;
import com.tagbin.user.livrewards_loyality.Model.LoginResponseModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;

import org.json.JSONException;

import java.text.ParseException;

public class Change_password_activity extends AppCompatActivity {
    public Button confirm_button;
    public TextView message, phone_no, resendOtp_tv;
    public EditText new_password, confirm_password, otp;
    String strlogin, strpassword, strotp, batPassword;
    ProgressBar progressBar;
    boolean flag = false;
    PrefManager pref;
    RequestListener responseListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response ForgotPassword", responseObject.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(Change_password_activity.this, "OTP is send successfully", Toast.LENGTH_SHORT).show();

                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(Change_password_activity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(Change_password_activity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Change_password_activity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(Change_password_activity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };
    RequestListener loginlistener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response login", responseObject.toString());
            final LoginResponseModel responsemodel = JsonUtils.objectify(responseObject.toString(), LoginResponseModel.class);
            pref.saveLoginModel(responsemodel);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    Intent intent = new Intent(Change_password_activity.this, ProductDetailActivity.class);
                    startActivity(intent);
                    pref.setForgot(false);
                }
            });

        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(Change_password_activity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(Change_password_activity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Change_password_activity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(Change_password_activity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };
    RequestListener passwordListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response setPassword", responseObject.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (getIntent().getStringExtra("firstTime") != null && getIntent().getStringExtra("firstTime").equals("firstTimeUser")) {
                        LoginModel loginModel = null;
                        String batCode = pref.getLoginModel().getData().getUsername();
                        if (flag == false) {
                            loginModel = new LoginModel();
                            loginModel.setBat_code(batCode);
                            loginModel.setPassword(batPassword);
                            loginModel.setJob_type("login");
                        }
                        PrefManager pref = new PrefManager(Change_password_activity.this);
                        pref.saveLoginModel1(loginModel);
                        progressBar.setVisibility(View.VISIBLE);
                        Controller.manualLogin(Change_password_activity.this, loginModel, loginlistener);

                    } else if (getIntent().getStringExtra("batcode") != null && getIntent().getStringExtra("batcode").equals("batcode_activity")) {
                        Intent intent = new Intent(Change_password_activity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        pref.setForgot(false);
                        finish();
                    }
                }
            });

        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(Change_password_activity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(Change_password_activity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Change_password_activity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(Change_password_activity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password_activity);
        message = (TextView) findViewById(R.id.message_logo);
        phone_no = (TextView) findViewById(R.id.phone_no);
        confirm_button = (Button) findViewById(R.id.confirm_button);
        otp = (EditText) findViewById(R.id.otp_text);
        resendOtp_tv = (TextView) findViewById(R.id.resendOtp_tv);
        new_password = (EditText) findViewById(R.id.passwordone);
        confirm_password = (EditText) findViewById(R.id.passwordtwo);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        pref = new PrefManager(Change_password_activity.this);
        if (pref.getLoginModel() != null) {
            Log.d("PrefLoginModel", "" + pref.getLoginModel().getData().getPhone());
            phone_no.setText("" + pref.getLoginModel().getData().getPhone());
        } else if (getIntent().getStringExtra("phone") != null) {
            phone_no.setText("" + getIntent().getStringExtra("phone"));
        }
        if (getIntent().getStringExtra("login") != null && getIntent().getStringExtra("login").equals("login_Activity")) {
            message.setText("Set New Password");
        } else if (getIntent().getStringExtra("firstTime") != null && getIntent().getStringExtra("firstTime").equals("firstTimeUser")) {
            message.setText("Set New Password");
        } else if (getIntent().getStringExtra("batcode") != null && getIntent().getStringExtra("batcode").equals("batcode_activity")) {
            message.setText("Forgot Password");
        } else if (getIntent().getStringExtra("change_pass") != null && getIntent().getStringExtra("change_pass").equals("change")) {
            LoginModel loginModel = new LoginModel();
            if (pref.getLoginModel() != null) {
                loginModel.setBat_code(pref.getLoginModel().getData().getUsername());
            }
            Controller.BatCode(Change_password_activity.this, loginModel, responseListner);
            message.setText("Set New Password");
        }
        SpannableString content = new SpannableString("RE-Send OTP");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        resendOtp_tv.setText(content);
        resendOtp_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginModel loginModel = new LoginModel();
                if (getIntent().getStringExtra("batcode") != null && getIntent().getStringExtra("batcode").equals("batcode_activity")) {
                    loginModel.setBat_code(getIntent().getStringExtra("username"));
                } else {
                    if (pref.getLoginModel().getData() != null) {
                        loginModel.setBat_code(pref.getLoginModel().getData().getUsername());
                    }
                }
                progressBar.setVisibility(View.VISIBLE);
                Controller.BatCode(Change_password_activity.this, loginModel, responseListner);

            }
        });

        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strlogin = new_password.getText().toString();
                strpassword = confirm_password.getText().toString();
                strotp = otp.getText().toString();
                if (strlogin.equals("")) {
                    new_password.setError("Enter password");
                } else if (strpassword.equals("")) {
                    confirm_password.setError("Re-Enter the password");
                } else if (strotp.equals("")) {
                    otp.setError("Enter OTP");
                } else if (!strlogin.equals(strpassword)) {
                    Toast.makeText(Change_password_activity.this, "Both password is not same", Toast.LENGTH_SHORT).show();
                } else {
                    batPassword = strpassword;
                    LoginModel loginModel = new LoginModel();
                    if (flag == false) {
                        if (pref.getLoginModel() != null && pref.getLoginModel().getData() != null) {
                            loginModel.setBat_code(pref.getLoginModel().getData().getUsername());
                        } else {
                            loginModel.setBat_code(getIntent().getStringExtra("username"));
                        }

                        loginModel.setPassword(strpassword);
                        loginModel.setVerification_otp(strotp);
                    }
                    PrefManager pref = new PrefManager(Change_password_activity.this);
                    pref.saveLoginModel1(loginModel);
                    progressBar.setVisibility(View.VISIBLE);
                    Controller.setPassword(Change_password_activity.this, loginModel, passwordListner);

                }
            }
        });
    }

    private void logout() {
        PrefManager pref = new PrefManager(this);
        pref.clearSession();
        Intent go = new Intent(this, LoginActivity.class);
        startActivity(go);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getIntent().getStringExtra("change_pass") != null && getIntent().getStringExtra("change_pass").equals("change")) {
            finish();
        } else if (getIntent().getStringExtra("firstTime") != null && getIntent().getStringExtra("firstTime").equals("firstTimeUser")) {
            Intent intent = new Intent(Change_password_activity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(Change_password_activity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
