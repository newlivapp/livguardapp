package com.tagbin.user.livrewards_loyality.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.AddWarrantyModel;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.FileUploadModel;
import com.tagbin.user.livrewards_loyality.Model.LoginResponseModel;
import com.tagbin.user.livrewards_loyality.Model.LoyalityModel;
import com.tagbin.user.livrewards_loyality.Model.ProductWarrantyDetailModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;

import org.json.JSONException;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ConfirmWarrantyActivity extends AppCompatActivity {
    EditText qrCode, customer_name, contact_no;
    ProgressBar progressBar;
    TextView dateView;
    Toolbar toolbar;
    ImageView back_button;
    Button register;
    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int day = calendar.get(Calendar.DAY_OF_MONTH);
    ProductWarrantyDetailModel productWarrantyDetailModel;
    String image_path, unique_code, strCustomerName, strContactNo, strVehicleNo, segment_id, vehicle_manufacturer_id, vehicle_model_id, user_id, remarks, product_category;
    String pincode, state, city;
    PrefManager prefManager;

    LoginResponseModel loginResponseModel;
    RequestListener loyalityListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response", responseObject.toString());
            final LoyalityModel loyalityModel = JsonUtils.objectify(responseObject.toString(), LoyalityModel.class);
            Intent intent = new Intent(ConfirmWarrantyActivity.this, ProductDetailActivity.class);
            startActivity(intent);
            finish();

        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener addWarrantyListener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    register.setEnabled(true);
                }
            });
            Log.d("response", responseObject.toString());
            productWarrantyDetailModel = JsonUtils.objectify(responseObject.toString(), ProductWarrantyDetailModel.class);
            if (responseObject.toString().equals("Serial No Already Scanned !")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ConfirmWarrantyActivity.this, "This Product is Already Scanned", Toast.LENGTH_SHORT).show();
                    }
                });

            } else if (responseObject.toString().equals("Serial is out of scheme")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ConfirmWarrantyActivity.this, "This Product is Out of Scheme", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                if (productWarrantyDetailModel == null || productWarrantyDetailModel.getDealer_product().isEmpty()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(ConfirmWarrantyActivity.this, responseObject.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        progressBar.setVisibility(View.GONE);

                        //Custome Analytics Fabric
                        Answers.getInstance().logCustom(new CustomEvent("Tertiary")
                                .putCustomAttribute("BAT_CODE", prefManager.getLoginModel().getData().getUsername() + "")
                                .putCustomAttribute("USER_NAME", prefManager.getLoginModel().getData().getFirst_name())
                                .putCustomAttribute("UNIQUE_NUMBER", qrCode.getText().toString().trim()));

                        final Dialog dialog = new Dialog(ConfirmWarrantyActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.popup_massage);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        final Button addmore = (Button) dialog.findViewById(R.id.addmore);
                        final TextView close = (TextView) dialog.findViewById(R.id.close);
                        final TextView msg = (TextView) dialog.findViewById(R.id.bettarymsg);
                        addmore.setText("Add More Warranty");

                        if (productWarrantyDetailModel == null) {
                            return;
                        }
                        msg.setText("Warranty of " + productWarrantyDetailModel.getDealer_product() + " battery has been added successfully");
                        //Call
                        addmore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Intent intent = new Intent(ConfirmWarrantyActivity.this, ScannerActivity.class);
                                intent.putExtra("AddWarranty", "NewWarranty");
                                startActivity(intent);
                                finish();
                            }
                        });
                        close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                LoyalityModel loyalityModel = new LoyalityModel();
                                loyalityModel.setUser_id(user_id);
                                Controller.getloyality(ConfirmWarrantyActivity.this, loyalityModel, loyalityListner);
                            }
                        });
                        if (ConfirmWarrantyActivity.this != null)
                            ConfirmWarrantyActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!(ConfirmWarrantyActivity.this).isFinishing()) {
                                        try {
                                            dialog.show();
                                        } catch (WindowManager.BadTokenException e) {
                                            Log.e("WindowManagerBad ", e.toString());
                                        } catch (Exception e) {
                                            Log.e("Exception ", e.toString());
                                        }
                                    }
                                }
                            });
                    }
                });

            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    register.setEnabled(true);
                }
            });
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(ConfirmWarrantyActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(ConfirmWarrantyActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ConfirmWarrantyActivity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ConfirmWarrantyActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_warranty);
        toolbar = findViewById(R.id.toolbar);
        back_button = toolbar.findViewById(R.id.back_button);
        back_button.setOnClickListener(backClick);
        qrCode = findViewById(R.id.qrcode);
        customer_name = findViewById(R.id.customer_name);
        contact_no = findViewById(R.id.contact_no);
        progressBar = findViewById(R.id.progressbar);
        dateView = findViewById(R.id.date);
        register = findViewById(R.id.add_button);

        unique_code = getIntent().getStringExtra("qrCode");
        strCustomerName = getIntent().getStringExtra("customer_name");
        strContactNo = getIntent().getStringExtra("contact_no");
        segment_id = getIntent().getStringExtra("segment_id");
        pincode = getIntent().getStringExtra("pincode");
        state = getIntent().getStringExtra("state");
        city = getIntent().getStringExtra("city");
        vehicle_manufacturer_id = getIntent().getStringExtra("vehicle_manufacturer_id");
        vehicle_model_id = getIntent().getStringExtra("vehicle_model_id");
        image_path = getIntent().getStringExtra("image_path");
        strVehicleNo = getIntent().getStringExtra("vehicle_no");
        remarks = getIntent().getStringExtra("remarks");
        product_category = getIntent().getStringExtra("product_category");

        qrCode.setText(unique_code);
        customer_name.setText(strCustomerName);
        contact_no.setText(strContactNo);
        dateView.setText("");
        prefManager = new PrefManager(ConfirmWarrantyActivity.this);
        loginResponseModel = prefManager.getLoginModel();
        user_id = String.valueOf(loginResponseModel.getData().getId());


        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        final String[] toDate = {dateFormat.format(date)};
        dateView.setText(dateFormat.format(date));
        dateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        final String select_date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                        dateView.setText(select_date);
                        toDate[0] =select_date;
                    }
                }, year, month, day);
                int min_date = month - 1;
                dpd.setMaxDate(calendar);
                //dpd.setMinDate(calendar.MARCH == 2)
                dpd.show(getFragmentManager(), "DATE_PICKER_TAG");
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register.setEnabled(false);
                AddWarrantyModel addWarrantyModel = new AddWarrantyModel();
                addWarrantyModel.setUser_id(user_id);
                addWarrantyModel.setUnique_code(unique_code);
                addWarrantyModel.setSell_date(toDate[0]);
                addWarrantyModel.setCustomer_name(strCustomerName);
                addWarrantyModel.setPincode(pincode);
                addWarrantyModel.setState(state);
                addWarrantyModel.setCity(city);
                addWarrantyModel.setCustomer_phone(strContactNo);
                addWarrantyModel.setCar_segment(segment_id);
                addWarrantyModel.setVehicle_manufacturer(segment_id);
                addWarrantyModel.setVehicle_model(segment_id);
                addWarrantyModel.setVehicle_number(strVehicleNo);
                addWarrantyModel.setRemarks(remarks);
//                if (image_path != null) {
//                    FileUploadModel fileUploadModel = new FileUploadModel();
//                    fileUploadModel.setFile(new File(image_path));
//                    Controller.warranty(ConfirmWarrantyActivity.this, addWarrantyModel, addWarrantyListener);
//                    progressBar.setVisibility(View.VISIBLE);
//                } else {
                    Controller.warranty(ConfirmWarrantyActivity.this, addWarrantyModel, addWarrantyListener);
                    progressBar.setVisibility(View.VISIBLE);
//                }
            }
        });
    }

    private void logout() {
        PrefManager pref = new PrefManager(this);
        pref.clearSession();
        Intent go = new Intent(this, LoginActivity.class);
        startActivity(go);
    }
}
