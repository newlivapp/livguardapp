package com.tagbin.user.livrewards_loyality.helper;

import android.app.Dialog;
import android.content.Context;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 04-04-2017.
 */

public class MyUtils {
    private static Toast toast;
    private static Dialog mDialog;

    public static void showToast(Context mContext, String msg) {
        if (toast != null)
            toast.cancel();
        toast = Toast.makeText(mContext, msg, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static String getValidDateFormat(String date) {
        try {
            DateFormat f1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date d = f1.parse(date);
            DateFormat f2 = new SimpleDateFormat("dd-MM-yyyy");
            return f2.format(d).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getYearlyFormat(String date) {
        try {
            DateFormat f1 = new SimpleDateFormat("dd-MM-yyyy");
            Date d = f1.parse(date);
            DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd");
            return f2.format(d).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getslashFormat(String date) {
        try {
            DateFormat f1 = new SimpleDateFormat("yyy-MM-dd");
            Date d = f1.parse(date);
            DateFormat f2 = new SimpleDateFormat("dd/MM/yyyy");
            return f2.format(d).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getdaydashFormat(String date) {
        try {
            DateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");
            Date d = f1.parse(date);
            DateFormat f2 = new SimpleDateFormat("dd-MM-yyyy");
            return f2.format(d).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getYearlydashFormat(String date) {
        try {
            DateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");
            Date d = f1.parse(date);
            DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd");
            return f2.format(d).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void removeCustomeProgressDialog() {
        try {
            if (mDialog != null) {
                if (mDialog.isShowing()) {
                    mDialog.dismiss();
                }
            }
            mDialog = null;
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean isValidEmail(String emailstring) {
        if (null == emailstring || emailstring.length() == 0) {
            return false;
        }
        Pattern emailPattern = Pattern
                .compile("/^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/");
        Matcher emailMatcher = emailPattern.matcher(emailstring);
        return emailMatcher.matches();
    }
}
