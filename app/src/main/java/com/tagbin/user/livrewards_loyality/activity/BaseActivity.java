package com.tagbin.user.livrewards_loyality.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.tagbin.user.livrewards_loyality.Model.APIErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;

public class BaseActivity extends AppCompatActivity {

    public void handelError(final int errorCode, final String message) {
        Log.d("message", message);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (message == "Network Not Available") {
                    Toast.makeText(BaseActivity.this, " Network Not Available", Toast.LENGTH_SHORT).show();
                } else {
                    if (errorCode >= 400 && errorCode < 500) {
                        if (errorCode == 403) {
                            Toast.makeText(BaseActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        } else if (errorCode == 401) {
                            logout();
                        } else {
                            ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                            if (errorResponseModel == null || errorResponseModel.getErr() == null || errorResponseModel.getErr().isEmpty()) {
                                APIErrorResponseModel apiErrorResponseModel = JsonUtils.objectify(message, APIErrorResponseModel.class);
                                if (apiErrorResponseModel != null && apiErrorResponseModel.getMsg() != null && !apiErrorResponseModel.getMsg().isEmpty()) {
                                    Toast.makeText(BaseActivity.this, apiErrorResponseModel.getMsg(), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                Toast.makeText(BaseActivity.this, "Something is wrong.! Please try again later.", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(BaseActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(BaseActivity.this, "Something is wrong.! Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    private void logout() {
        PrefManager.getInstance(this).clearSession();
        Intent go = new Intent(this, LoginActivity.class);
        startActivity(go);
        finish();
    }
}
