package com.tagbin.user.livrewards_loyality.activity;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.AddProductModel;
import com.tagbin.user.livrewards_loyality.Model.DealeProductModel;
import com.tagbin.user.livrewards_loyality.Model.ErrorModel;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.LoginResponseModel;
import com.tagbin.user.livrewards_loyality.Model.ProductListModel;
import com.tagbin.user.livrewards_loyality.Model.PurchaseProductModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.fragment.PicModeSelectDialogFragment;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.MyUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;
import com.tagbin.user.livrewards_loyality.helper.ProfileImage.GOTOConstants;
import com.tagbin.user.livrewards_loyality.helper.ProfileImage.ImageCropActivity;

import org.json.JSONException;

import java.text.ParseException;

public class add_product_activity extends AppCompatActivity {
    private static int SELECT_PICTURE = 1;
    private static String[] PERMISSIONS_LOCATION = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    final int REQUEST_LOCATION = 2;
    EditText comment;
    TextView qrcode;
    Button button_add;
    ImageView view_previous, scanner_icon;
    String result;
    ProgressBar progressBar;
    Toolbar toolbar;
    ImageView back_button;
    ImageView productImage;
    TextView uploadImageTv;
    DealeProductModel dealeProductModel;
    String product_category = "";
    String user_id = "";
    PrefManager prefManager;
    Context context;
    View.OnClickListener uploadClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (hasLocationPermissionGranted())
                showAddProfilePicDialog1();
            else
                requestLocationPermission();
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
    RequestListener addProductListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            if (product_category.equals("E-RICKSHAW")) {
                Log.d("response Add E-RICKSHAW", responseObject.toString());
            } else {
                Log.d("response addProductL", responseObject.toString());
            }
            final PurchaseProductModel ProductModel = JsonUtils.objectify(responseObject.toString(), PurchaseProductModel.class);
            dealeProductModel = ProductModel.getDealer_product();

            runOnUiThread(new Runnable() {
                @SuppressLint("SetTextI18n")
                @Override
                public void run() {

                    button_add.setEnabled(true);
                    progressBar.setVisibility(View.GONE);

                    if (dealeProductModel == null || dealeProductModel.getProduct() == null) {
                        final ErrorResponseModel errorResponseModel = JsonUtils.objectify(responseObject.toString(), ErrorResponseModel.class);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty())
                                    MyUtils.showToast(add_product_activity.this, errorResponseModel.getErr());
                            }
                        });
                        return;
                    }

                    String uNo = qrcode.getText().toString().trim();
                    qrcode.setText("");

                    //Custome Analytics Fabric
                    Answers.getInstance().logCustom(new CustomEvent("Purchase Secondary")
                            .putCustomAttribute("BAT_CODE", prefManager.getLoginModel().getData().getUsername() + "")
                            .putCustomAttribute("USER_NAME", prefManager.getLoginModel().getData().getFirst_name())
                            .putCustomAttribute("UNIQUE_NUMBER", uNo));

                    //Cal Point API
                    /*if (prefManager.getUserViewFlag()) {
                        if (product_category.equalsIgnoreCase("UPS") || product_category.equalsIgnoreCase("IB")) {
                            AddWarrantyPointAPIModel addWarrantyPointAPIModel = new AddWarrantyPointAPIModel();
                            addWarrantyPointAPIModel.setId(user_id);
                            addWarrantyPointAPIModel.setCat(product_category.toUpperCase());
                            Controller.apiMaintainedCount(add_product_activity.this, addWarrantyPointAPIModel, addWarrantyPointListener);
                            return;
                        }
                    }*/

                    final Dialog dialog = new Dialog(add_product_activity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.popup_massage);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    final Button addMore = dialog.findViewById(R.id.addmore);

                    addMore.setText("Add More Product");
                    final TextView close = dialog.findViewById(R.id.close);
                    final TextView msg = dialog.findViewById(R.id.bettarymsg);

                    if (dealeProductModel != null && dealeProductModel.getProduct() != null) {
                        msg.setText(dealeProductModel.getProduct() + "\n" + " product has been added successfully");
                    } else {
                        final ErrorResponseModel errorResponseModel = JsonUtils.objectify(responseObject.toString(), ErrorResponseModel.class);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty())
                                    MyUtils.showToast(add_product_activity.this, errorResponseModel.getErr());
                            }
                        });
                        return;
                    }

                    addMore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Intent intent1 = new Intent(add_product_activity.this, ScannerActivity.class);
                            intent1.putExtra("AddProduct", "NewProduct");
                            startActivity(intent1);
                            finish();
                        }
                    });
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Intent intent = new Intent(add_product_activity.this, ProductDetailActivity.class);
                            startActivity(intent);
                        }
                    });
                    if (add_product_activity.this != null)
                        add_product_activity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!(add_product_activity.this).isFinishing()) {
                                    try {
                                        dialog.show();
                                    } catch (WindowManager.BadTokenException e) {
                                        Log.e("WindowManagerBad ", e.toString());
                                    } catch (Exception e) {
                                        Log.e("Exception ", e.toString());
                                    }
                                }
                            }
                        });
                }
            });

        }

        @Override
        public void onRequestError(int errorCode, String message) {
            if (add_product_activity.this == null || message == null) {
                return;
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    button_add.setEnabled(true);
                }
            });
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(add_product_activity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(add_product_activity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(add_product_activity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(add_product_activity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        }
    };
    RequestListener addWarrantyPointListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response apiMaintainedCount", responseObject.toString());

            if (add_product_activity.this != null)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);

                        final Dialog dialog = new Dialog(add_product_activity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.popup_massage);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        final Button addMore = dialog.findViewById(R.id.addmore);

                        addMore.setText("Add More Product");
                        final TextView close = dialog.findViewById(R.id.close);
                        final TextView msg = dialog.findViewById(R.id.bettarymsg);

                        msg.setText(dealeProductModel.getProduct() + "\n" + " product has been added successfully");

                        addMore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Intent intent1 = new Intent(add_product_activity.this, ScannerActivity.class);
                                intent1.putExtra("AddProduct", "NewProduct");
                                startActivity(intent1);
                                finish();
                            }
                        });
                        close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Intent intent = new Intent(add_product_activity.this, ProductDetailActivity.class);
                                startActivity(intent);
                            }
                        });
                        if (add_product_activity.this != null)
                            add_product_activity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!(add_product_activity.this).isFinishing()) {
                                        try {
                                            dialog.show();
                                        } catch (WindowManager.BadTokenException e) {
                                            Log.e("WindowManagerBad ", e.toString());
                                        } catch (Exception e) {
                                            Log.e("Exception ", e.toString());
                                        }
                                    }
                                }
                            });
                    }
                });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(add_product_activity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            logout();
                        }
                    });
                } else {
                    final ErrorModel errorResponseModel = JsonUtils.objectify(message, ErrorModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(add_product_activity.this, errorResponseModel.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(add_product_activity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        }
    };
    private String image_path;
    private ProductListModel productListModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product_activity);
        context = this;
        toolbar = findViewById(R.id.toolbar);
        back_button = toolbar.findViewById(R.id.back_button);
        back_button.setOnClickListener(backClick);
        button_add = findViewById(R.id.add_button);
        qrcode = findViewById(R.id.qrcode);
        comment = findViewById(R.id.comment);
        scanner_icon = findViewById(R.id.scanner_icon);
        view_previous = findViewById(R.id.view_previous);
        progressBar = findViewById(R.id.progressbar);
        productImage = findViewById(R.id.productImage);
        uploadImageTv = findViewById(R.id.uploadImageTv);
        result = getIntent().getStringExtra("result");

        prefManager = new PrefManager(add_product_activity.this);
        LoginResponseModel loginResponseModel = prefManager.getLoginModel();
        if (loginResponseModel == null || loginResponseModel.getData() == null) {
            return;
        }
        user_id = String.valueOf(loginResponseModel.getData().getId());

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("product_category"))
            product_category = getIntent().getStringExtra("product_category");

        if (product_category.equals("INV"))
            product_category = "UPS";

        qrcode.setText(result);
        // qrcode.setSelection(qrcode.getText().length());


        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (prefManager.getUserViewFlag()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(add_product_activity.this, "Please scan battery after 3rd july!!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return;
                }*/
                button_add.setEnabled(false);
                if (qrcode.getText().toString().trim().isEmpty()) {
                    button_add.setEnabled(true);
                    Toast.makeText(add_product_activity.this, "Please Enter Unique No", Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("click", "clicked");

                    AddProductModel addProductModel = new AddProductModel();
                    addProductModel.setUnique_code(qrcode.getText().toString().trim().toUpperCase());
                    addProductModel.setSerial_number(result);
                    addProductModel.setComment(comment.getText().toString());
                    addProductModel.setUser_id(user_id);
                    addProductModel.setState(prefManager.getLoginModel().getData().getState());
                    if (product_category.equals("E-RICKSHAW")/* && prefManager.getUserViewFlagWB()*/) {
                        Controller.AddERickProduct(add_product_activity.this, addProductModel, addProductListener);
                    } else {
                        Controller.AddProductDetail(add_product_activity.this, addProductModel, addProductListener);
                    }
                    progressBar.setVisibility(View.VISIBLE);
                }
            }

        });

        view_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(add_product_activity.this, ProductDetailActivity.class);
                intent.putExtra("product_tab", "product");
                intent.putExtra("tab", 1);
                startActivity(intent);
                finish();
            }
        });
        scanner_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(add_product_activity.this, ScannerActivity.class);
                intent.putExtra("AddProduct", "NewProduct");
                startActivity(intent);
                finish();
            }
        });

        uploadImageTv.setOnClickListener(uploadClick);

        productImage.setOnClickListener(uploadClick);
    }

    private Bitmap showCroppedImage(String imagePath) {
        if (imagePath != null) {
            return BitmapFactory.decodeFile(imagePath);

        }
        return null;
    }

    public boolean hasLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(add_product_activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(add_product_activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(add_product_activity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void showAddProfilePicDialog1() {
        PicModeSelectDialogFragment dialogFragment = new PicModeSelectDialogFragment();
        dialogFragment.setiPicModeSelectListener(new PicModeSelectDialogFragment.IPicModeSelectListener() {
            @Override
            public void onPicModeSelected(String mode) {
                String action = mode.equalsIgnoreCase(GOTOConstants.PicModes.CAMERA) ? GOTOConstants.IntentExtras.ACTION_CAMERA : GOTOConstants.IntentExtras.ACTION_GALLERY;
                Intent intent = new Intent(add_product_activity.this, ImageCropActivity.class);
                intent.putExtra("ACTION", action);
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });
        dialogFragment.show(getFragmentManager(), "picModeSelector");
    }

    public void requestLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            ActivityCompat.requestPermissions(add_product_activity.this, PERMISSIONS_LOCATION,
                    REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //Storage permission is enabled
                        showAddProfilePicDialog1();

                    } else if (ActivityCompat.shouldShowRequestPermissionRationale(add_product_activity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            && ActivityCompat.shouldShowRequestPermissionRationale(add_product_activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            && ActivityCompat.shouldShowRequestPermissionRationale(add_product_activity.this, Manifest.permission.CAMERA)) {
                        //User has deny from permission dialog
                        final AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(add_product_activity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setTitle("Reading Permission Denied");
                        alertDialog1.setMessage("Are you sure you want to deny this permission?");
                        alertDialog1.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.setNegativeButton("RETRY", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.show();
                    } else {
                        // User has deny permission and checked never show permission dialog so you can redirect to Application settings page
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(add_product_activity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setMessage("It looks like you have turned off permission required for this feature. It can be enabled under Phone Settings > Apps > Partner > Permissions");
                        alertDialog1.setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        });
                        alertDialog1.show();
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                image_path = data.getStringExtra(GOTOConstants.IntentExtras.IMAGE_PATH);
                progressBar.setVisibility(View.GONE);
                uploadImageTv.setVisibility(View.GONE);
                productImage.setVisibility(View.VISIBLE);
                productImage.setImageBitmap(showCroppedImage(image_path));
            } else if (resultCode == RESULT_CANCELED) {
                //TODO : Handle case
            } else {
                String errorMsg = data.getStringExtra(ImageCropActivity.ERROR_MSG);
                Toast.makeText(add_product_activity.this, errorMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void logout() {
        PrefManager pref = new PrefManager(this);
        pref.clearSession();
        Intent go = new Intent(this, LoginActivity.class);
        startActivity(go);
    }

}
