package com.tagbin.user.livrewards_loyality.Model;

public class SecondaryPointsDelhi {
    String ib_count, ups_count, maintained_points;

    public String getIb_count() {
        return ib_count;
    }

    public void setIb_count(String ib_count) {
        this.ib_count = ib_count;
    }

    public String getUps_count() {
        return ups_count;
    }

    public void setUps_count(String ups_count) {
        this.ups_count = ups_count;
    }

    public String getMaintained_points() {
        return maintained_points;
    }

    public void setMaintained_points(String maintained_points) {
        this.maintained_points = maintained_points;
    }
}
