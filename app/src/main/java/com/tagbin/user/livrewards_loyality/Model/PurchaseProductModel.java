package com.tagbin.user.livrewards_loyality.Model;

/**
 * Created by user on 03-04-2017.
 */

public class PurchaseProductModel {
    public DealeProductModel dealer_product;
    int id, user, loyalty_point;
    String created_timestamp;

    public DealeProductModel getDealer_product() {
        return dealer_product;
    }

    public void setDealer_product(DealeProductModel dealer_product) {
        this.dealer_product = dealer_product;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(int loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }
}
