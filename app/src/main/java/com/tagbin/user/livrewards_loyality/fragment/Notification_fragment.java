package com.tagbin.user.livrewards_loyality.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.NotificationModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.activity.LoginActivity;
import com.tagbin.user.livrewards_loyality.adapter.NotificationRecyclerViewAdapter;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 28-03-2017.
 */

public class Notification_fragment extends Fragment {
    Context context;
    List<NotificationModel> notificationModelList;
    NotificationRecyclerViewAdapter notification_adapter;
    RecyclerView notification_rv;
    //    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefreshLayout;
    RelativeLayout empty_notification_image;
    RequestListener notificationListner = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response Notification", responseObject.toString());
            notificationModelList.clear();
            Type collectionType = new TypeToken<List<NotificationModel>>() {
            }.getType();
            final List<NotificationModel> notifyList = (List<NotificationModel>) new Gson().fromJson(responseObject.toString(), collectionType);
            notificationModelList.addAll(notifyList);
            if ((Activity) getContext() != null) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        if (notificationModelList.isEmpty()) {
                            empty_notification_image.setVisibility(View.VISIBLE);
                        } else {
                            empty_notification_image.setVisibility(View.GONE);
                            notification_rv.setVisibility(View.VISIBLE);
                        }
                        notification_adapter.notifyDataSetChanged();
                    }
                });
            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    if ((Activity) getContext() != null && getContext() != null) {
                        ((Activity) getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeRefreshLayout.setRefreshing(false);
                                Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } else if (errorCode == 401) {
                    if ((Activity) getContext() != null) {
                        ((Activity) getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeRefreshLayout.setRefreshing(false);
                                logout();
                            }
                        });
                    }
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    if ((Activity) getContext() != null && getContext() != null) {
                        ((Activity) getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeRefreshLayout.setRefreshing(false);
                                if (errorResponseModel == null) {
                                    return;
                                }
                                if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                    Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getContext(), "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
            } else {
                if (((Activity) getContext()) != null && getContext() != null) {
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }
    };

    @SuppressLint("ValidFragment")
    public Notification_fragment(Context context1) {
        context = context1;
    }

    public Notification_fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

   /* @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);

        if (isFragmentVisible_) {
            ProductDetailActivity.nCounter = 0;
        }
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notification_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        notification_rv = (RecyclerView) getView().findViewById(R.id.notification_rv);
//        progressBar= (ProgressBar) getView().findViewById(R.id.progressbar);
        empty_notification_image = (RelativeLayout) getView().findViewById(R.id.empty_rl);
        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swiperefresh);
        notificationModelList = new ArrayList<NotificationModel>();
        Controller.GetNotification(getContext(), notificationListner);
//        progressBar.setVisibility(View.VISIBLE);
        notification_adapter = new NotificationRecyclerViewAdapter(getContext(), notificationModelList);
        notification_rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        notification_rv.setAdapter(notification_adapter);
        notification_rv.setHasFixedSize(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Controller.GetNotification(getContext(), notificationListner);
            }
        });
    }

    private void logout() {
        PrefManager pref = new PrefManager(getContext());
        pref.clearSession();
        Intent go = new Intent(getContext(), LoginActivity.class);
        startActivity(go);
    }
}
