package com.tagbin.user.livrewards_loyality.Model;

/**
 * Created by Nav on 08-Aug-17.
 */

public class SelectRedeemModel {
    int redeem_scheme, loyalty_point;

    public int getRedeem_scheme() {
        return redeem_scheme;
    }

    public void setRedeem_scheme(int redeem_scheme) {
        this.redeem_scheme = redeem_scheme;
    }

    public int getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(int loyalty_point) {
        this.loyalty_point = loyalty_point;
    }
}
