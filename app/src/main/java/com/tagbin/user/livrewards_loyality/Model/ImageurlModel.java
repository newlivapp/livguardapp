package com.tagbin.user.livrewards_loyality.Model;

/**
 * Created by user on 05-04-2017.
 */

public class ImageurlModel {
    String user_id, base;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }
}
