package com.tagbin.user.livrewards_loyality.Interface;

import android.view.View;

/**
 * Created by Nav on 08-Aug-17.
 */

public interface RedeemSlabRVClickInterface {
    public void recyclerViewListClicked(View v, int schemeId, int LoyaltyPoints);
}
