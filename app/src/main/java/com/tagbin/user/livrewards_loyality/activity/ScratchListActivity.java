package com.tagbin.user.livrewards_loyality.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.GetCouponAPIModel;
import com.tagbin.user.livrewards_loyality.Model.ScratchCardModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.adapter.ScratchAdapter;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;
import com.tagbin.user.livrewards_loyality.listener.RecyclerItemClickListener;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ScratchListActivity extends BaseActivity {

    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;
    ScratchAdapter scratchAdapter;
    ArrayList<ScratchCardModel> scratchCardModels = new ArrayList<>();
    RelativeLayout rlNoData;
    ImageView back_button;
    TextView toolbar_title;
    Toolbar toolbar;
    ProgressBar progressbar;
    RequestListener couponAPIRequestListener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.e("response getCouponAPI", responseObject.toString());
            scratchCardModels.clear();
            Type collectionType = new TypeToken<List<ScratchCardModel>>() {
            }.getType();
            try {
                List<ScratchCardModel> ca = (List<ScratchCardModel>) new Gson().fromJson(responseObject.toString(), collectionType);
                scratchCardModels.addAll(ca);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (scratchCardModels == null || scratchCardModels.isEmpty()) {
                //Show No records sreen
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressbar.setVisibility(View.GONE);
                        rlNoData.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                });
                handelError(449, responseObject.toString());
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressbar.setVisibility(View.GONE);
                    rlNoData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    if (scratchAdapter == null)
                        setupRecyclerView();
                    else
                        scratchAdapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, final String message) {
            handelError(errorCode, message);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressbar.setVisibility(View.GONE);
                    rlNoData.setVisibility(View.GONE);
                }
            });
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scratch_list);

        initControl();
        setupRecyclerView();

        //all setup now you can set click and lisner here
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                        ScratchCardModel scratchCardModel = scratchCardModels.get(position);

                        //check for already scratch
                        if (scratchCardModel.getRedeem_status().equals("scratched")) {
                            return;
                        }

                        //Date Validation
                        try {
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                            SimpleDateFormat dateOnlySimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            Date scratchDate = dateOnlySimpleDateFormat.parse(dateOnlySimpleDateFormat.format(simpleDateFormat.parse(scratchCardModel.getCoupon_scratch_date())));
                            Date currentDate = dateOnlySimpleDateFormat.parse(dateOnlySimpleDateFormat.format(simpleDateFormat.parse(scratchCardModel.getCurrent_date())));
                            if (currentDate.getTime() < scratchDate.getTime()) {
                                return;
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                            return;
                        }

                        //Coupon text check it is null no need to go Ahead
                        if (scratchCardModel.getCoupon_text() == null || scratchCardModel.getCoupon_text().isEmpty())
                            return;

                        Intent intent = new Intent(ScratchListActivity.this, ScratchCardsActivity.class);
                        intent.putExtra("scratch_model", scratchCardModel);
                        startActivity(intent);
                    }
                })
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        //For Testing Only
        //Defaultscratchcard();

        //Get Coupen Data
        if (PrefManager.getInstance(this).getLoginModel() != null && PrefManager.getInstance(this).getLoginModel().getData() != null) {
            GetCouponAPIModel getCouponAPIModel = new GetCouponAPIModel();
            getCouponAPIModel.setUser_id(String.valueOf(PrefManager.getInstance(this).getLoginModel().getData().getId()));
            //if (!PrefManager.getInstance(ScratchListActivity.this).getUserViewFlag()) {
            progressbar.setVisibility(View.VISIBLE);
            Controller.getCouponAPI(this, getCouponAPIModel, couponAPIRequestListener);
            // }
        } else {
            return;
        }
    }

    private void initControl() {
        recyclerView = findViewById(R.id.recyclerView);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (PrefManager.getInstance(ScratchListActivity.this).getLoginModel() != null && PrefManager.getInstance(ScratchListActivity.this).getLoginModel().getData() != null && PrefManager.getInstance(ScratchListActivity.this).getLoginModel().getData().getDealership_name() != null && !PrefManager.getInstance(ScratchListActivity.this).getLoginModel().getData().getDealership_name().isEmpty()) {
            toolbar_title.setText(PrefManager.getInstance(ScratchListActivity.this).getLoginModel().getData().getDealership_name() + " - Coupon Wallet");
        }
        back_button.setOnClickListener(backClick);

        rlNoData = findViewById(R.id.rlNoData);
        progressbar = findViewById(R.id.progressbar);
        progressbar.setVisibility(View.GONE);
    }

    private void setupRecyclerView() {
        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        scratchAdapter = new ScratchAdapter(this, scratchCardModels);
        recyclerView.setAdapter(scratchAdapter);
        recyclerView.invalidate();
    }

    private void Defaultscratchcard() {
        scratchCardModels.clear();
        Type collectionType = new TypeToken<List<ScratchCardModel>>() {
        }.getType();
        try {
            List<ScratchCardModel> ca = (List<ScratchCardModel>) new Gson().fromJson("[{\"username\":\"BAT23717\",\"coupon_text\":\"TRUCK\",\"redeem_status\":\"Pending\",\"coupon_scratch_date\":\"2019-08-10T11:00:00.000Z\",\"created_timestamp\":\"2019-07-20T04:57:01.131Z\",\"user_id\":\"3500\",\"id\":\"119\",\"current_date\":\"2019-07-08T07:09:56.491Z\"},{\"username\":\"BAT23717\",\"coupon_text\":\"Hurray!! You Have Won an CAR!!!\",\"redeem_status\":\"Pending\",\"coupon_scratch_date\":\"2019-07-03T17:03:21.000Z\",\"created_timestamp\":\"2019-07-05T04:57:01.125Z\",\"user_id\":\"3500\",\"id\":\"117\",\"current_date\":\"2019-07-06T07:09:56.491Z\"},{\"username\":\"BAT23717\",\"coupon_text\":\"Hurray!! You Have Won an Activa!!!\",\"redeem_status\":\"scratched\",\"coupon_scratch_date\":\"2019-07-03T17:03:21.000Z\",\"created_timestamp\":\"2019-07-05T04:57:01.125Z\",\"user_id\":\"3500\",\"id\":\"118\",\"current_date\":\"2019-07-06T07:09:56.491Z\"}]", collectionType);
            scratchCardModels.addAll(ca);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (scratchCardModels == null || scratchCardModels.isEmpty()) {
            //Show No records sreen
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressbar.setVisibility(View.GONE);
                    rlNoData.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            });
            //handelError(449, responseObject.toString());
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressbar.setVisibility(View.GONE);
                rlNoData.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                if (scratchAdapter == null)
                    setupRecyclerView();
                else
                    scratchAdapter.notifyDataSetChanged();
            }
        });
    }
}
