package com.tagbin.user.livrewards_loyality.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.APIErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.AddTokenAPIModel;
import com.tagbin.user.livrewards_loyality.Model.CheckVersionAPIModel;
import com.tagbin.user.livrewards_loyality.Model.GetCouponAPIModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.dialog.InfoDialog;
import com.tagbin.user.livrewards_loyality.fragment.Logs_fragment;
import com.tagbin.user.livrewards_loyality.fragment.Notification_fragment;
import com.tagbin.user.livrewards_loyality.fragment.Target_fragmnet;
import com.tagbin.user.livrewards_loyality.fragment.home_fragment;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.MyUtils;
import com.tagbin.user.livrewards_loyality.helper.NotificationUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ProductDetailActivity extends AppCompatActivity {
    public static int nCounter = 0;
    public Menu menu;
    ViewPager mainViewPager;
    FloatingActionsMenu main_menu;
    RelativeLayout main_realtive_container;
    int position = 0;
    LinearLayout llNotificationPopup;
    TextView tvDetail;
    String currentVersion;
    String Latest_verion;
    Dialog dialog;
    RequestListener apiCheckPointsRequestListener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {

            runOnUiThread(new Runnable() {
                @SuppressLint("LongLogTag")
                @Override
                public void run() {
                    Log.e("response checkCouponScratchPoints", responseObject.toString());
                    APIErrorResponseModel apiErrorResponseModel = JsonUtils.objectify(responseObject.toString(), APIErrorResponseModel.class);
                    if (apiErrorResponseModel != null && apiErrorResponseModel.getStatus() != null && !apiErrorResponseModel.getStatus().isEmpty() && apiErrorResponseModel.getStatus().equalsIgnoreCase("success")) {

                        TextView tvCouponMessage = dialog.findViewById(R.id.tvCouponMessage);
                        tvCouponMessage.setText(apiErrorResponseModel.getMsg());

                        TextView tvClose = dialog.findViewById(R.id.tvClose);
                        tvClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        Button btnCouponWallet = dialog.findViewById(R.id.btnCouponWallet);
                        btnCouponWallet.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                startActivity(new Intent(ProductDetailActivity.this, ScratchListActivity.class));
                            }
                        });

                        if (ProductDetailActivity.this != null)
                            ProductDetailActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!(ProductDetailActivity.this).isFinishing()) {
                                        try {
                                            if (dialog != null && !dialog.isShowing())
                                                dialog.show();
                                        } catch (WindowManager.BadTokenException e) {
                                            Log.e("WindowManagerBad ", e.toString());
                                        } catch (Exception e) {
                                            Log.e("Exception ", e.toString());
                                        }
                                    }
                                }
                            });
                    }
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener apiCheckemail = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.e("response checkemail",responseObject.toString());
            try{
                JSONObject obj = new JSONObject(responseObject.toString());
                Log.e("flagresposne", String.valueOf(obj));
                boolean flag=obj.getBoolean("flag");
                if(flag){
                    Toast.makeText(ProductDetailActivity.this, "Profile Is up to date", Toast.LENGTH_SHORT).show();
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailActivity.this);
                            builder.setTitle("Email Update");
                            builder.setIcon(R.mipmap.ic_launcher);
                            builder.setCancelable(false);
                            builder.setMessage("Please Fill The Email  in  Profile To Continue LivRewards App.")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if(alertEmail!= null&& alertEmail.isShowing())
                                                alertEmail.dismiss();
                                            alertEmail = null ;
                                            Intent intent = new Intent(ProductDetailActivity.this,EditProfileActivity.class);
                                            startActivity(intent);
                                        }
                                    });
                            if(alertEmail == null )
                                alertEmail =builder.create();
                            if (ProductDetailActivity.this != null)
                                ProductDetailActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!ProductDetailActivity.this.isFinishing()) {
                                            try {
                                                if (alertEmail != null && !alertEmail.isShowing())
                                                    alertEmail.show();
                                            } catch (WindowManager.BadTokenException e) {
                                                Log.e("WindowManagerBad ", e.toString());
                                            } catch (Exception e) {
                                                Log.e("Exception ", e.toString());
                                            }
                                        }
                                    }
                                });
                        }
                    });

                }
            }catch (Exception e){
                Log.e("JSONObject Error", e.getMessage() + "--");
            }

        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener apiCheckKYC = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {

            runOnUiThread(new Runnable() {
                @SuppressLint("LongLogTag")
                @Override
                public void run() {
                    Log.e("response CheckKYC", responseObject.toString());
                    try {
                        JSONObject obj = new JSONObject(responseObject.toString());
                        if (obj.has("flag")) {
                            boolean flag = obj.getBoolean("flag");
                            if (!flag) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailActivity.this);
                                builder.setTitle("KYC DETAIL");
                                builder.setIcon(R.mipmap.ic_launcher);
                                builder.setCancelable(false);
                                builder.setMessage("Please fill KYC details to continue using LivReward App.")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (alertKYC != null && alertKYC.isShowing())
                                                    alertKYC.dismiss();
                                                alertKYC = null;
                                                Intent intent = new Intent(ProductDetailActivity.this, EnterDetailsActivity.class);
                                                startActivity(intent);
                                            }
                                        });

                                if (alertKYC == null)
                                    alertKYC = builder.create();
                                if (ProductDetailActivity.this != null)
                                    ProductDetailActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (!ProductDetailActivity.this.isFinishing()) {
                                                try {
                                                    if (alertKYC != null && !alertKYC.isShowing())
                                                        alertKYC.show();
                                                } catch (WindowManager.BadTokenException e) {
                                                    Log.e("WindowManagerBad ", e.toString());
                                                } catch (Exception e) {
                                                    Log.e("Exception ", e.toString());
                                                }
                                            }
                                        }
                                    });
                            }
                        }
                    } catch (Exception e) {
                        Log.e("JSONObject Error", e.getMessage() + "--");
                    }
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    private TabLayout tabLayout;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isNetDialogShowing = false;
    private AlertDialog alertUpdateApp, alertKYC,alertEmail;
    RequestListener apiCheckVersionListener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {

            runOnUiThread(new Runnable() {
                @SuppressLint("LongLogTag")
                @Override
                public void run() {
                    Log.e("response CheckVersionListener", responseObject.toString());
                    try {
                        JSONObject obj = new JSONObject(responseObject.toString());
                        if (obj.has("status")) {
                            boolean status = obj.getBoolean("status");
                            if (status) {
                                showUpdateDialog();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailActivity.this);
                                builder.setTitle("Our App got Update");
                                builder.setIcon(R.mipmap.ic_launcher);
                                builder.setCancelable(false);
                                builder.setMessage("New version available, select update to update our app")
                                        .setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                final String appName = ProductDetailActivity.this.getPackageName();

                                                try {
                                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appName)));
                                                } catch (android.content.ActivityNotFoundException anfe) {
                                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
                                                }

                                            }
                                        });

                                if (alertUpdateApp == null)
                                    alertUpdateApp = builder.create();
                                if (ProductDetailActivity.this != null)
                                    ProductDetailActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (!ProductDetailActivity.this.isFinishing()) {
                                                try {
                                                    if (alertUpdateApp != null && !alertUpdateApp.isShowing())
                                                        alertUpdateApp.show();
                                                } catch (WindowManager.BadTokenException e) {
                                                    Log.e("WindowManagerBad ", e.toString());
                                                } catch (Exception e) {
                                                    Log.e("Exception ", e.toString());
                                                }
                                            }
                                        }
                                    });
                            }
                        }
                    } catch (Exception e) {
                        Log.e("JSONObject Error", e.getMessage() + "--");
                    }
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    private Dialog internetDialog;
    public BroadcastReceiver internetConnectionReciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo activeWIFIInfo = connectivityManager
                    .getNetworkInfo(connectivityManager.TYPE_WIFI);

            if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
                removeInternetDialog();
                if (PrefManager.getInstance(context).getLoginModel() != null && PrefManager.getInstance(context).getLoginModel().getData() != null) {
                    try {
                        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                        String currentVersionApp = (pInfo.versionName);
                        if (currentVersionApp != null && !currentVersionApp.isEmpty()) {
                            CheckVersionAPIModel checkVersionAPIModel = new CheckVersionAPIModel();
                            checkVersionAPIModel.setUser_id(String.valueOf(PrefManager.getInstance(context).getLoginModel().getData().getId()));
                            checkVersionAPIModel.setVersioncode(currentVersionApp);
                            Controller.checkversioncode(context, checkVersionAPIModel, apiCheckVersionListener);
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    /*if (PrefManager.getInstance(context).getUserViewFlag()) {
                         GetCouponAPIModel getCouponAPIModel = new GetCouponAPIModel();
                         getCouponAPIModel.setUser_id(String.valueOf(PrefManager.getInstance(this).getLoginModel().getData().getId()));
                         Controller.checkCouponScratchPointsDelhi(this, getCouponAPIModel, apiCheckPointsRequestListener);
                    } else {*/
                    GetCouponAPIModel getCouponAPIModel = new GetCouponAPIModel();
                    getCouponAPIModel.setUser_id(String.valueOf(PrefManager.getInstance(context).getLoginModel().getData().getId()));
                    Controller.checkCouponScratchPoints(context, getCouponAPIModel, apiCheckPointsRequestListener);
                    Controller.CheckKYC(ProductDetailActivity.this, getCouponAPIModel, apiCheckKYC);
                    Controller.checkemail(ProductDetailActivity.this, getCouponAPIModel,apiCheckemail);
                    //}
                }

            } else {
                showInternetDialog();
            }
        }
    };
    private boolean isInternetReceiverRegistered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mainViewPager = (ViewPager) findViewById(R.id.main_viewpager);
        // mainViewPager.setOffscreenPageLimit(1 /*count of your fragments in viwePager*/);

        llNotificationPopup = findViewById(R.id.llNotificationPopup);
        llNotificationPopup.setVisibility(View.GONE);
        tvDetail = findViewById(R.id.tvDetail);

        setupViewPager(mainViewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabbar);
        tabLayout.setupWithViewPager(mainViewPager);
        setUpTabIcons();

        dialog = new Dialog(ProductDetailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_win_coupon);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //do stuff here
                if (tab.getPosition() == 2) {
                    nCounter = 0;
                    setNotificationCount(nCounter);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        main_realtive_container = (RelativeLayout) findViewById(R.id.background_layout);
        main_menu = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        final FloatingActionButton register_warranty = (FloatingActionButton) findViewById(R.id.register_warranty);
        final FloatingActionButton new_purchanse = (FloatingActionButton) findViewById(R.id.new_purchase);
        final FloatingActionButton serive_request = (FloatingActionButton) findViewById(R.id.service_request);
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            if (getIntent().getStringExtra("product_tab") != null && getIntent().getStringExtra("product_tab").equals("product")) {
                position = extra.getInt("tab");
                mainViewPager.setCurrentItem(position);
            } else if (getIntent().getStringExtra("warranty_tab") != null && getIntent().getStringExtra("warranty_tab").equals("warranty")) {
                position = extra.getInt("tab");
                mainViewPager.setCurrentItem(position);
            } else if (getIntent().getStringExtra("service_tab") != null && getIntent().getStringExtra("service_tab").equals("service")) {
                position = extra.getInt("tab");
                mainViewPager.setCurrentItem(position);
            }
        }


        main_menu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                main_realtive_container.setVisibility(View.VISIBLE);
                main_realtive_container.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        main_menu.collapse();
                        return true;
                    }
                });
            }

            @Override
            public void onMenuCollapsed() {
                main_realtive_container.setVisibility(View.GONE);
                main_realtive_container.setOnTouchListener(null);
            }
        });
        new_purchanse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_menu.collapse();
                Intent intent = new Intent(ProductDetailActivity.this, ScannerActivity.class);
                intent.putExtra("AddProduct", "NewProduct");
                startActivity(intent);
            }
        });
        register_warranty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_menu.collapse();
                Intent intent1 = new Intent(ProductDetailActivity.this, ScannerActivity.class);
                intent1.putExtra("AddWarranty", "NewWarranty");
                startActivity(intent1);
            }
        });
        serive_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_menu.collapse();
                Intent intent = new Intent(ProductDetailActivity.this, Service_request_activity.class);
                startActivity(intent);
            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals("REGISTRATION_COMPLETE")) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    displayFirebaseRegId();

                } else if (intent.getAction().equals("PUSH_NOTIFICATION")) {
                    // new push notification is received

                    nCounter++;
                    setNotificationCount(nCounter);
                    String message = intent.getStringExtra("message");
                    if (message != null && message.length() > 0) {
                        tvDetail.setText(message);
                        Log.i("Push notification", message);
                    }
                    animateViewWhenNotify();
                }
            }
        };

        displayFirebaseRegId();

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("message")) {
            Log.e("notification", getIntent().getExtras().getString("message"));
            mainViewPager.setCurrentItem(2);
        }

    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("lvg_fcm_info", 0);
        String regId = pref.getString("regId", "");

        Log.e("Token", "Firebase reg id: " + regId);

        PrefManager prefManager = new PrefManager(this);
        if (prefManager.getLoginModel() == null || prefManager.getLoginModel().getData() == null) {
            return;
        }
        String id = String.valueOf(prefManager.getLoginModel().getData().getId());
        AddTokenAPIModel addTokenAPIModel = new AddTokenAPIModel();
        addTokenAPIModel.setId(id);
        addTokenAPIModel.setToken(regId);
        Controller.addTokenAPI(this, addTokenAPIModel, new RequestListener() {
            @Override
            public void onRequestStarted() {

            }

            @Override
            public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {

            }

            @Override
            public void onRequestError(int errorCode, String message) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkInternet();
        if (PrefManager.getInstance(this).getLoginModel() != null && PrefManager.getInstance(this).getLoginModel().getData() != null) {
            try {
                PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
                String currentVersionApp = (pInfo.versionName);
                if (currentVersionApp != null && !currentVersionApp.isEmpty()) {
                    CheckVersionAPIModel checkVersionAPIModel = new CheckVersionAPIModel();
                    checkVersionAPIModel.setUser_id(String.valueOf(PrefManager.getInstance(this).getLoginModel().getData().getId()));
                    checkVersionAPIModel.setVersioncode(currentVersionApp);
                    Controller.checkversioncode(this, checkVersionAPIModel, apiCheckVersionListener);
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            /* if (PrefManager.getInstance(this).getUserViewFlag()) {
                GetCouponAPIModel getCouponAPIModel = new GetCouponAPIModel();
                getCouponAPIModel.setUser_id(String.valueOf(PrefManager.getInstance(this).getLoginModel().getData().getId()));
                Controller.checkCouponScratchPointsDelhi(this, getCouponAPIModel, apiCheckPointsRequestListener);
            } else {*/
            GetCouponAPIModel getCouponAPIModel = new GetCouponAPIModel();
            getCouponAPIModel.setUser_id(String.valueOf(PrefManager.getInstance(this).getLoginModel().getData().getId()));
            Controller.checkCouponScratchPoints(this, getCouponAPIModel, apiCheckPointsRequestListener);
            Controller.CheckKYC(this, getCouponAPIModel, apiCheckKYC);
            //}
        }

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter("REGISTRATION_COMPLETE"));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter("PUSH_NOTIFICATION"));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    private void checkInternet() {
        registerReceiver(internetConnectionReciever, new IntentFilter(
                "android.net.conn.CONNECTIVITY_CHANGE"));
        isInternetReceiverRegistered = true;
    }

    private void removeInternetDialog() {
        if (alertUpdateApp != null && alertUpdateApp.isShowing()) {
            alertUpdateApp.dismiss();
        }
        if (internetDialog != null && internetDialog.isShowing()) {
            internetDialog.dismiss();
            isNetDialogShowing = false;
            internetDialog = null;
        }
    }

    private void showInternetDialog() {

        if (alertUpdateApp != null && alertUpdateApp.isShowing()) {
            alertUpdateApp.dismiss();
        }

        MyUtils.removeCustomeProgressDialog();
        isNetDialogShowing = true;

        if (internetDialog == null) {
            internetDialog = new InfoDialog(this,
                    getString(R.string.dialog_no_inter_message_restart),
                    R.string.dialog_enable) {

                @Override
                public void onPositiveClicked(DialogInterface dialog) {
                    // TODO Auto-generated method stub
                    removeInternetDialog();
                    Intent intent = new Intent(
                            android.provider.Settings.ACTION_SETTINGS);
                    startActivity(intent);
                }

                @Override
                public void onNegativeClicked(DialogInterface dialog) {
                    // TODO Auto-generated method stub
               /* removeInternetDialog();
                finish();
                Intent intent = new Intent(BaseActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/
                }
            }.create();
            internetDialog.show();
        }

        /*AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailActivity.this);
        builder.setCancelable(false);
        builder.setMessage("You have not active internet connection, please start internet.")
                .setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                        Intent intent = new Intent(
                                android.provider.Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                });
        alert = builder.create();
        if (ProductDetailActivity.this != null)
            ProductDetailActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!ProductDetailActivity.this.isFinishing()) {
                        try {
                            if (alert != null && !alert.isShowing())
                                alert.show();
                        } catch (WindowManager.BadTokenException e) {
                            Log.e("WindowManagerBad ", e.toString());
                        } catch (Exception e) {
                            Log.e("Exception ", e.toString());
                        }
                    }
                }
            });*/

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        if (isInternetReceiverRegistered) {
            unregisterReceiver(internetConnectionReciever);
            isInternetReceiverRegistered = false;

        }
        super.onPause();
    }

    private void setNotificationCount(int nCount) {
        View tabView = tabLayout.getTabAt(2).getCustomView();
        TextView tvNotificationCount = tabView.findViewById(R.id.tvNotificationCount);
        if (nCount == 0)
            tvNotificationCount.setVisibility(View.GONE);
        else
            tvNotificationCount.setVisibility(View.VISIBLE);
        tvNotificationCount.setText(String.valueOf(nCount));
    }

    private void setUpTabIcons() {

        View tabOneView = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tvTabOne = tabOneView.findViewById(R.id.tab);
        tvTabOne.setText("Home");
        tvTabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home_tab_image, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOneView);

        View tabTwoView = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tvTabTwo = tabTwoView.findViewById(R.id.tab);
        tvTabTwo.setText("Logs");
        tvTabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.log_tab_image, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwoView);

        View tabThreeView = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tvTabThree = tabThreeView.findViewById(R.id.tab);
        tvTabThree.setText("Notifications");
        tvTabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification_tab_image, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThreeView);

        View tabForView = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tvTabFor = tabForView.findViewById(R.id.tab);
        tvTabFor.setText("Scheme");
        tvTabFor.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.target_tab_image, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabForView);


        /*TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Home");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home_tab_image, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);*/

        /*TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Logs");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.log_tab_image, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);*/

        /*TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("Notifications");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification_tab_image, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);*/

        /*TextView tabFour = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour.setText("Scheme");
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.target_tab_image, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabFour);*/
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new home_fragment(this), "Home");
        adapter.addFragment(new Logs_fragment(this), "Logs");
        adapter.addFragment(new Notification_fragment(this), "Notifications");
        adapter.addFragment(new Target_fragmnet(this), "Scheme");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_logout, menu);
        this.menu = menu;
        /*if (PrefManager.getInstance(this).getUserViewFlag()) {
            this.menu.getItem(3).setVisible(false);
        }*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.change_password) {
            Intent intent = new Intent(ProductDetailActivity.this, Change_password_activity.class);
            intent.putExtra("change_pass", "change");
            startActivity(intent);
        } else if (item.getItemId() == R.id.logout) {
            final Dialog dialog = new Dialog(ProductDetailActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.popup_logout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            final Button addmore = (Button) dialog.findViewById(R.id.addmore);
            final TextView close = (TextView) dialog.findViewById(R.id.close);

            addmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    PrefManager prefManager = new PrefManager(ProductDetailActivity.this);
                    prefManager.clearSession();
                    Intent intent1 = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    startActivity(intent1);
                    ProductDetailActivity.this.finish();
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            if (ProductDetailActivity.this != null)
                ProductDetailActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!(ProductDetailActivity.this).isFinishing()) {
                            try {
                                dialog.show();
                            } catch (WindowManager.BadTokenException e) {
                                Log.e("WindowManagerBad ", e.toString());
                            } catch (Exception e) {
                                Log.e("Exception ", e.toString());
                            }
                        }
                    }
                });

        } else if (item.getItemId() == R.id.save_report) {
            Intent intent = new Intent(this, SaveReportActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.profile_detail) {
            Intent intent = new Intent(this, EditProfileActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.scratch_cards) {
            startActivity(new Intent(ProductDetailActivity.this, ScratchListActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    private void animateViewWhenNotify() {
        if (llNotificationPopup.getVisibility() == View.GONE) {
            llNotificationPopup.setVisibility(View.VISIBLE);
            llNotificationPopup.setAlpha(0.0f);
            llNotificationPopup.animate()
                    .translationY(0)
                    .alpha(1.0f)
                    .setDuration(300)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            //hide view after 3 second
                            llNotificationPopup.postDelayed(new Runnable() {
                                public void run() {
                                    llNotificationPopup.animate()
                                            .translationY(-100)
                                            .alpha(0.0f)
                                            .setDuration(300)
                                            .setListener(new AnimatorListenerAdapter() {
                                                @Override
                                                public void onAnimationEnd(Animator animation) {
                                                    super.onAnimationEnd(animation);
                                                    llNotificationPopup.setVisibility(View.GONE);
                                                }
                                            });
                                }
                            }, 3000);

                        }
                    });
        }
    }

    private void showUpdateDialog() {
        //------getting version code for application----\\
        GetVersionCode getVersionCode = new GetVersionCode();
        try {
            Latest_verion = getVersionCode.execute().get();
            Log.d("version", String.valueOf(Latest_verion));
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            PackageInfo pInfo = ProductDetailActivity.this.getPackageManager().getPackageInfo(ProductDetailActivity.this.getPackageName(), 0);
            currentVersion = (pInfo.versionName);
            Log.d("version", String.valueOf(currentVersion));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
//-------Rendering to playstore is current version is less or more  than playstore-----\\
        if (Latest_verion != null)
            if (!currentVersion.equals(Latest_verion)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailActivity.this);
                builder.setTitle("Our App got Update");
                builder.setIcon(R.mipmap.ic_launcher);
                builder.setCancelable(false);
                builder.setMessage("New version available, select update to update our app")
                        .setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final String appName = ProductDetailActivity.this.getPackageName();

                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
                                }

                            }
                        });

                if (alertUpdateApp == null)
                    alertUpdateApp = builder.create();
                if (ProductDetailActivity.this != null)
                    ProductDetailActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!ProductDetailActivity.this.isFinishing()) {
                                try {
                                    if (alertUpdateApp != null && !alertUpdateApp.isShowing())
                                        alertUpdateApp.show();
                                } catch (WindowManager.BadTokenException e) {
                                    Log.e("WindowManagerBad ", e.toString());
                                } catch (Exception e) {
                                    Log.e("Exception ", e.toString());
                                }
                            }
                        }
                    });
            }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
