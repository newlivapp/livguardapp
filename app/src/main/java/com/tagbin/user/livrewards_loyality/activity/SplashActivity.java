package com.tagbin.user.livrewards_loyality.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.tagbin.user.livrewards_loyality.Model.LoginResponseModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;

public class SplashActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    LoginResponseModel loginResponseModel;
    PrefManager prefManager;
//    String currentVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //  new GetVersionCode().execute();
//        try {
//            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
//
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
        prefManager = new PrefManager(SplashActivity.this);
        loginResponseModel = prefManager.getLoginModel();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (prefManager.getForgot() == true) {
                    Intent intent = new Intent(SplashActivity.this, Change_password_activity.class);
                    intent.putExtra("change_pass", "change");
                    startActivity(intent);
                    finish();
                } else if (loginResponseModel != null && loginResponseModel.getToken() != null) {
                    Intent intent = new Intent(SplashActivity.this, ProductDetailActivity.class);

                    if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("message")) {
                        intent.putExtra("message", getIntent().getExtras().getString("message"));
                    }
                    startActivity(intent);
                    finish();
                } else {
                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                    finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }


}



