package com.tagbin.user.livrewards_loyality.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.ERickPointsWB;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.FinalPointsRedeemModel;
import com.tagbin.user.livrewards_loyality.Model.LoginResponseModel;
import com.tagbin.user.livrewards_loyality.Model.LoyalityModel;
import com.tagbin.user.livrewards_loyality.Model.RedeemResponseModel;
import com.tagbin.user.livrewards_loyality.Model.RedeemSlabModel;
import com.tagbin.user.livrewards_loyality.Model.ResponseRedeemPointsModel;
import com.tagbin.user.livrewards_loyality.Model.SelectRedeemModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.adapter.CouponAdapter;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.MyUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;
import com.tagbin.user.livrewards_loyality.listener.RecyclerItemClickListener;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class ERickRedeemActivity extends AppCompatActivity {
    public static boolean manualy;
    List<RedeemSlabModel> choice_list = new ArrayList<RedeemSlabModel>();
    TextView view_redeem_history;
    TextView total_loyalty_points;
    TextView enter_manually_tv;
    TextView scheme;
    EditText enter_manualy;
    Toolbar toolbar;
    ImageView back_button;
    ProgressBar progressBar;
    Button redeem_Button;
    int redeemSchemeId;
    int loyalty_points;
    PrefManager prefManager;
    int user_id;
    RadioGroup redeem_choice_rg;
    RadioButton radioButton;
    InputMethodManager imm;
    RelativeLayout empty_rl, redeem_slab_rl;
    int points;
    PrefManager pref;

    CouponAdapter couponAdapter;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    int points_redeem = 0;

    LoginResponseModel loginResponseModel;
    RequestListener reedemRequestListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("responce Erick slab", responseObject.toString());
            choice_list.clear();
            Type collection = new TypeToken<List<RedeemSlabModel>>() {
            }.getType();
            try {
                final List<RedeemSlabModel> rsm = (List<RedeemSlabModel>) new Gson().fromJson(responseObject.toString(), collection);
                choice_list.addAll(rsm);
            } catch (Exception e) {
                final ErrorResponseModel errorResponseModel = JsonUtils.objectify(responseObject.toString(), ErrorResponseModel.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty())
                            MyUtils.showToast(ERickRedeemActivity.this, errorResponseModel.getErr());
                    }
                });
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    if (choice_list.isEmpty()) {
                        empty_rl.setVisibility(View.VISIBLE);
                        redeem_slab_rl.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        empty_rl.setVisibility(View.GONE);
                        redeem_slab_rl.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.VISIBLE);
                        if (couponAdapter == null)
                            setupRecyclerView();
                        else
                            couponAdapter.notifyDataSetChanged();
                    }
                }
            });
           /* runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    if (choice_list.isEmpty()) {
                        empty_rl.setVisibility(View.VISIBLE);
                        redeem_slab_rl.setVisibility(View.GONE);
                    } else {
                        empty_rl.setVisibility(View.GONE);
                        redeem_slab_rl.setVisibility(View.VISIBLE);
                        for (int i = 0; i < choice_list.size(); i++) {
                            radioButton = new RadioButton(ERickRedeemActivity.this);
                            radioButton.setId(i);
                            radioButton.setText("" + choice_list.get(i).getLoyalty_point());
                            radioButton.setTextColor(Color.parseColor("#4F4F4F"));
                            redeem_choice_rg.addView(radioButton);
                        }
                        if (choice_list.get(0).is_manual() == true) {
                            //enter_manually_tv.setVisibility(View.VISIBLE);
                            enter_manualy.setVisibility(View.GONE);
                            scheme.setVisibility(View.VISIBLE);
                        } else {
                            // enter_manually_tv.setVisibility(View.GONE);
                            enter_manualy.setVisibility(View.GONE);
                        }
                    }
                }
            });*/

        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response err Erick", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            MyUtils.showToast(ERickRedeemActivity.this, "UnAuthorised!");
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
//                               logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            MyUtils.showToast(ERickRedeemActivity.this, errorResponseModel.getErr());
                        }
                    });
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        MyUtils.showToast(ERickRedeemActivity.this, "No Internet Connection");
                    }
                });
            }
        }
    };
    //    RequestListener loyalityListner =new RequestListener() {
//        @Override
//        public void onRequestStarted() {
//
//        }
//
//        @Override
//        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
//            Log.d("response",responseObject.toString());
//            runOnUiThread(new Runnable() {
//                @SuppressLint("SetTextI18n")
//                @Override
//                public void run() {
//                    int loyality= pref.getLoginModel().getData().getTotal_loyalty()+Integer.parseInt(String.valueOf(responseObject));
//                    total_loyalty_points.setText(""+loyality);
//                }
//            });
//
//        }
//
//        @Override
//        public void onRequestError(int errorCode, String message) {
//
//        }
//    };
    View.OnClickListener historyDetail = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent inten = new Intent(ERickRedeemActivity.this, RedeemHistoryActivity.class);
            startActivity(inten);
        }
    };
    View.OnClickListener redeemClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (loyalty_points == 0 && enter_manualy.getText().toString().equals("")) {
                MyUtils.showToast(ERickRedeemActivity.this, "Please select your Redeem points");
            } else if (!enter_manualy.getText().toString().equals("") && Integer.parseInt(enter_manualy.getText().toString()) > points_redeem) {
                MyUtils.showToast(ERickRedeemActivity.this, "You do not have enough loyalty points to redeem");
            } else if (loyalty_points > points_redeem) {
                MyUtils.showToast(ERickRedeemActivity.this, "You do not have enough loyalty points to redeem");
            } else {
                SelectRedeemModel selectRedeemModel = new SelectRedeemModel();
                selectRedeemModel.setRedeem_scheme(redeemSchemeId);
                if (enter_manualy.getText().toString().equals("")) {
                    selectRedeemModel.setLoyalty_point(loyalty_points);
                } else {
                    selectRedeemModel.setLoyalty_point(Integer.parseInt(enter_manualy.getText().toString()));
                }

                progressBar.setVisibility(View.VISIBLE);
                Controller.SelectRedeem(ERickRedeemActivity.this, selectRedeemModel, new RequestListener() {
                    @Override
                    public void onRequestStarted() {

                    }

                    @Override
                    public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
                        Log.d("response SelectRedeem", responseObject.toString());
                        RedeemResponseModel getRedeemHistoryModel = JsonUtils.objectify(responseObject.toString(), RedeemResponseModel.class);
                        assert getRedeemHistoryModel != null;
                        user_id = getRedeemHistoryModel.getId();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                final Dialog dialog = new Dialog(ERickRedeemActivity.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.otp_massage_popup);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                final TextView msgText = (TextView) dialog.findViewById(R.id.msg_text);
                                final ImageView dismiss = (ImageView) dialog.findViewById(R.id.dissmiss);
                                final TextView phone_no_msg = (TextView) dialog.findViewById(R.id.phone_no_massage);
                                final EditText otp_text = (EditText) dialog.findViewById(R.id.otp_text);
                                final TextView resendOtp_tv = (TextView) dialog.findViewById(R.id.resend_otp_tv);
                                SpannableString content = new SpannableString("RE-Send OTP");
                                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                                resendOtp_tv.setText(content);
                                resendOtp_tv.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        FinalPointsRedeemModel finalPointsRedeemModel = new FinalPointsRedeemModel();
                                        finalPointsRedeemModel.setId(user_id);
                                        finalPointsRedeemModel.setResend_otp(1);
                                        progressBar.setVisibility(View.VISIBLE);
                                        Controller.TotalPointsRedeem(ERickRedeemActivity.this, user_id, finalPointsRedeemModel, new RequestListener() {
                                            @Override
                                            public void onRequestStarted() {

                                            }

                                            @Override
                                            public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
                                                Log.d("response TotalPointsRedeem", responseObject.toString());
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        MyUtils.showToast(ERickRedeemActivity.this, "OTP sent successfully");
                                                        progressBar.setVisibility(View.GONE);
                                                    }
                                                });
                                            }

                                            @Override
                                            public void onRequestError(int errorCode, String message) {
                                                Log.d("response", message);
                                                if (errorCode >= 400 && errorCode < 500) {
                                                    if (errorCode == 403) {
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                progressBar.setVisibility(View.GONE);
                                                                MyUtils.showToast(ERickRedeemActivity.this, "UnAuthorised!");
                                                            }
                                                        });
                                                    } else if (errorCode == 401) {

                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                progressBar.setVisibility(View.GONE);
                                                                //                                logout();
                                                            }
                                                        });
                                                    } else {
                                                        final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                progressBar.setVisibility(View.GONE);
                                                                MyUtils.showToast(ERickRedeemActivity.this, errorResponseModel.getErr());
                                                            }
                                                        });
                                                    }
                                                } else {
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            progressBar.setVisibility(View.GONE);
                                                            MyUtils.showToast(ERickRedeemActivity.this, "No Internet Connection");
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                });

                                if (prefManager.getLoginModel() != null) {
                                    phone_no_msg.setText("Enter OTP send to +91" + prefManager.getLoginModel().getData().getPhone());
                                }
                                msgText.setVisibility(View.GONE);
                                final Button verify = (Button) dialog.findViewById(R.id.verify_button);
                                verify.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        FinalPointsRedeemModel finalPointsRedeemModel = new FinalPointsRedeemModel();
                                        finalPointsRedeemModel.setOtp(otp_text.getText().toString());
                                        Controller.TotalPointsRedeem(ERickRedeemActivity.this, user_id, finalPointsRedeemModel, new RequestListener() {
                                            @Override
                                            public void onRequestStarted() {

                                            }

                                            @Override
                                            public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
                                                Log.d("response TotalPointsRedeem", responseObject.toString());
                                                ResponseRedeemPointsModel responseRedeemPointsModel = JsonUtils.objectify(responseObject.toString(), ResponseRedeemPointsModel.class);
                                                assert responseRedeemPointsModel != null;
                                                int deductLoyaltyPoints = responseRedeemPointsModel.getLoyalty_point();
                                                LoginResponseModel obj1 = prefManager.getLoginModel();
                                                int previousLoyaltyPoints = obj1.getData().getTotal_loyalty();
                                                int finalLoyaltyPoints = previousLoyaltyPoints + deductLoyaltyPoints;
                                                obj1.getData().setTotal_loyalty(finalLoyaltyPoints);
                                                prefManager.saveLoginModel(obj1);
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        MyUtils.showToast(ERickRedeemActivity.this, "Redeem request successful");
                                                        finish();
                                                    }
                                                });


                                            }

                                            @Override
                                            public void onRequestError(int errorCode, String message) {
                                                Log.d("response", message);
                                                if (errorCode >= 400 && errorCode < 500) {
                                                    if (errorCode == 403) {
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                progressBar.setVisibility(View.GONE);
                                                                MyUtils.showToast(ERickRedeemActivity.this, "UnAuthorised!");
                                                            }
                                                        });
                                                    } else if (errorCode == 401) {

                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                progressBar.setVisibility(View.GONE);
                                                                //  logout();
                                                            }
                                                        });
                                                    } else {
                                                        final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                progressBar.setVisibility(View.GONE);
                                                                MyUtils.showToast(ERickRedeemActivity.this, errorResponseModel.getErr());
                                                            }
                                                        });
                                                    }
                                                } else {
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            progressBar.setVisibility(View.GONE);
                                                            MyUtils.showToast(ERickRedeemActivity.this, "No Internet Connection");
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                });
                                dismiss.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });
                                if (ERickRedeemActivity.this != null)
                                    ERickRedeemActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (!(ERickRedeemActivity.this).isFinishing()) {
                                                try {
                                                    dialog.show();
                                                    dialog.setCancelable(false);
                                                } catch (WindowManager.BadTokenException e) {
                                                    Log.e("WindowManagerBad ", e.toString());
                                                } catch (Exception e) {
                                                    Log.e("Exception ", e.toString());
                                                }
                                            }
                                        }
                                    });
                            }
                        });
                    }

                    @Override
                    public void onRequestError(int errorCode, String message) {
                        Log.d("response", message);
                        if (errorCode >= 400 && errorCode < 500) {
                            if (errorCode == 403) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressBar.setVisibility(View.GONE);
                                        MyUtils.showToast(ERickRedeemActivity.this, "UnAuthorised!");
                                    }
                                });
                            } else if (errorCode == 401) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressBar.setVisibility(View.GONE);
//                                logout();
                                    }
                                });
                            } else {
                                final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressBar.setVisibility(View.GONE);
                                        MyUtils.showToast(ERickRedeemActivity.this, errorResponseModel.getErr());
                                    }
                                });
                            }
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.GONE);
                                    MyUtils.showToast(ERickRedeemActivity.this, "No Internet Connection");
                                }
                            });
                        }

                    }
                });
            }
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_e_rick);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        enter_manualy = (EditText) findViewById(R.id.manually_points);
        total_loyalty_points = (TextView) findViewById(R.id.total_loyalty_points);
        redeem_Button = (Button) findViewById(R.id.redeem_button);
        redeem_choice_rg = (RadioGroup) findViewById(R.id.redeem_choice_rb);
        scheme = (TextView) findViewById(R.id.choice_list);
        // enter_manually_tv= (TextView) findViewById(R.id.enter_manually_tv);
        empty_rl = (RelativeLayout) findViewById(R.id.empty_rl);
        redeem_slab_rl = (RelativeLayout) findViewById(R.id.loyalty_points_slab);
        redeem_choice_rg.clearCheck();
        back_button.setOnClickListener(backClick);
        view_redeem_history = (TextView) findViewById(R.id.view_redeem_history);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        prefManager = new PrefManager(ERickRedeemActivity.this);
        SpannableString content = new SpannableString("View Redeem History");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        view_redeem_history.setText(content);
        view_redeem_history.setVisibility(View.GONE);
        view_redeem_history.setOnClickListener(historyDetail);
        redeem_Button.setOnClickListener(redeemClick);
        redeem_Button.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView = findViewById(R.id.recyclerView);

        pref = new PrefManager(ERickRedeemActivity.this);
        loginResponseModel = pref.getLoginModel();
        LoyalityModel loyalityModel = new LoyalityModel();
        if (loginResponseModel == null) {
            return;
        }
        String user_id = String.valueOf(loginResponseModel.getData().getId());
        loyalityModel.setUser_id(user_id);
        // Controller.getloyality(ERickRedeemActivity.this, loyalityModel, loyalityListner);

       /* if (pref.getUserViewFlag()) {
            String ibupsPoint = "0";
            SecondaryPointsDelhi secondaryPointsDelhi = pref.getSecondaryPointsDelhi();
            if (secondaryPointsDelhi != null)
                ibupsPoint = secondaryPointsDelhi.getMaintained_points();

            total_loyalty_points.setText("" + ibupsPoint);
        }
        else {
            int loyality = pref.getLoginModel().getData().getTotal_loyalty() + 0;
            total_loyalty_points.setText("" + loyality);
        }*/
        String count = "0";
        ERickPointsWB eRickPointsWB = pref.getERickPointsWB();
        if (eRickPointsWB != null)
            count = eRickPointsWB.getCount();

        total_loyalty_points.setText("" + count);
        if (!total_loyalty_points.getText().toString().trim().equals("")) {
            points_redeem = Integer.parseInt(total_loyalty_points.getText().toString().trim());
        }

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        redeem_choice_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                enter_manualy.setText(null);
                if (choice_list.get(0).is_manual() == true) {
                    //   enter_manually_tv.setVisibility(View.GONE);
                } else {
                    // enter_manually_tv.setVisibility(View.GONE);
                }
                enter_manualy.setEnabled(false);
                int checkId = group.getCheckedRadioButtonId();
                radioButton = (RadioButton) findViewById(checkId);
                if (checkedId == -1) {
                    //  enter_manually_tv.setVisibility(View.GONE);
                    enter_manualy.setEnabled(true);
                    imm.showSoftInput(enter_manualy, 0);
                } else {
                    loyalty_points = Integer.parseInt(radioButton.getText().toString());
                }
            }
        });
//        enter_manually_tv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                enter_manually_tv.setVisibility(View.GONE);
//                enter_manualy.requestFocus();
//                imm.showSoftInput(enter_manualy,0);
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        redeem_choice_rg.clearCheck();
//                    }
//                });
//            }
//        });
        /*if (prefManager.getUserViewFlag())
            Controller.GetDelhisecondaryredeem(ERickRedeemActivity.this, reedemRequestListener);
        else*/
        Controller.GetWBsecondaryredeem(ERickRedeemActivity.this, reedemRequestListener);
        //else
        //Controller.GetRedeemSlab(ERickRedeemActivity.this, reedemRequestListener);

        setupRecyclerView();
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                        if (position >= 0 && position < choice_list.size()) {
                        } else {
                            return;
                        }
                        RedeemSlabModel redeemSlabModel = choice_list.get(position);
                        if (redeemSlabModel == null)
                            return;

                        loyalty_points = redeemSlabModel.getLoyalty_point();

                        if (loyalty_points == 0 || loyalty_points > points_redeem) {
                            loyalty_points = 0;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MyUtils.showToast(ERickRedeemActivity.this, "You don't have enough points for redeem.");
                                }
                            });
                            for (RedeemSlabModel re : choice_list)
                                re.setSelected(false);
                            couponAdapter.notifyDataSetChanged();
                            return;
                        }

                        for (RedeemSlabModel re : choice_list)
                            re.setSelected(false);

                        redeemSlabModel.setSelected(true);
                        couponAdapter.notifyDataSetChanged();
                    }
                })
        );

    }

    private void setupRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        couponAdapter = new CouponAdapter(this, choice_list, 1);
        recyclerView.setAdapter(couponAdapter);
        recyclerView.invalidate();
    }
}
