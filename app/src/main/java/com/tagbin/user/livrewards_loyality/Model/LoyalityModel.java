package com.tagbin.user.livrewards_loyality.Model;

public class LoyalityModel {
    String user_id, total_loyality;

    public String getTotal_loyality() {
        return total_loyality;
    }

    public void setTotal_loyality(String total_loyality) {
        this.total_loyality = total_loyality;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
