package com.tagbin.user.livrewards_loyality.Model;

/**
 * Created by Nav on 08-Aug-17.
 */

public class FinalPointsRedeemModel {
    String otp;
    int id, resend_otp;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getResend_otp() {
        return resend_otp;
    }

    public void setResend_otp(int resend_otp) {
        this.resend_otp = resend_otp;
    }
}
