package com.tagbin.user.livrewards_loyality.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tagbin.user.livrewards_loyality.AutoScrollViewPager;
import com.tagbin.user.livrewards_loyality.Database.DatabaseBackend;
import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Interface.onItemClickListner;
import com.tagbin.user.livrewards_loyality.Model.AddWarrantyPointAPIModel;
import com.tagbin.user.livrewards_loyality.Model.BannerModel;
import com.tagbin.user.livrewards_loyality.Model.DataModel;
import com.tagbin.user.livrewards_loyality.Model.ERickPointsWB;
import com.tagbin.user.livrewards_loyality.Model.EditProfileModel;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.GetViewFlag;
import com.tagbin.user.livrewards_loyality.Model.HomeCountModel;
import com.tagbin.user.livrewards_loyality.Model.LoginResponseModel;
import com.tagbin.user.livrewards_loyality.Model.LoyalityModel;
import com.tagbin.user.livrewards_loyality.Model.SecondaryPointsDelhi;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.activity.ActivityWarrantyRedeem;
import com.tagbin.user.livrewards_loyality.activity.ERickRedeemActivity;
import com.tagbin.user.livrewards_loyality.activity.EditProfileActivity;
import com.tagbin.user.livrewards_loyality.activity.LoginActivity;
import com.tagbin.user.livrewards_loyality.activity.RedeemActivity;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class home_fragment extends Fragment implements onItemClickListner {

    protected View view;
    ProgressBar progressBar;
    TextView partnerNameTv, dealershipNameTv, dealer_name_tv, phone_no, total_loyalty_points, total_warranty_points, total_loyalty_points2,
            total_purchase_points, total_ib_points, total_ups_points, tvERickTotal, tvERickTotalCount, tv_purchase, tv_tertiary;
    CircleImageView profile_pic;
    Context context;
    Spinner manufacturer_spinner, car_model_spinner, fuel_spinner;
    PrefManager pref;
    RecyclerView battery_model_rv;
    RecyclerView segmnet_rv;
    RelativeLayout points_RL, rlIBPlusUPS, rlERick, points_RL2;
    RelativeLayout points_wrranty;
    FirebaseAnalytics firebaseAnalytics;
    List<String> banner;
    SwipeRefreshLayout mySwiprRefreshLayout;
    int car_segment_id;
    RelativeLayout profilePicRL;
    DatabaseBackend databaseBackend;
    String strVehicleSegment;
    String user_id;
    /*String currentVersion;
    String Latest_verion;*/
    DataModel dataModel;
    LinearLayout llButtons, llButtonParent, llPurchasePointParent, llERick, llIBplusUPS, llIBAndUPSParent;
    RelativeLayout rlPurchase, rlTertiary;
    int loyality;
    int ivCount = 0;
    int ibCount = 0;
    RequestListener loyalityListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.d("response Loyality", responseObject.toString());
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    total_warranty_points.setText("" + responseObject.toString());
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener onApiViewFlagAndData = new RequestListener() {
        @Override
        public void onRequestStarted() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onRequestCompleted(Object responseObject) {
            Log.d("response State Flag", responseObject.toString());
            try {
                JSONObject jsonObject = new JSONObject(responseObject.toString());
                boolean flag = jsonObject.getBoolean("flag");
                //pref.setUserViewFlag(flag);
                if (flag) {
                    String state = jsonObject.getString("state");
                    if (state.equals("delhi")) {
                        Log.e("state", state);
                        pref.setUserViewFlag(flag);
                        pref.setUserViewFlagWB(false);
                        /*if (flag) {
                            if (getActivity() != null && ((ProductDetailActivity) getActivity()).menu != null)
                                ((ProductDetailActivity) getActivity()).menu.getItem(3).setVisible(false);
                        }*/
                    } else if (state.equals("westbengal")) {
                        Log.e("state", state);
                        pref.setUserViewFlagWB(flag);
                        pref.setUserViewFlag(false);
                    }
                } else {
                    pref.setUserViewFlagWB(false);
                    pref.setUserViewFlag(false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    //also set here extra layout
                    /*if (pref.getUserViewFlag()) {
                        llButtons.setVisibility(View.VISIBLE);
                        llButtonParent.setVisibility(View.GONE);
                        llPurchasePointParent.setVisibility(View.GONE);
                    } else {*/

                    if (llButtons != null)
                        llButtons.setVisibility(View.VISIBLE);
                    if (llButtonParent != null)
                        llButtonParent.setVisibility(View.GONE);
                    if (llPurchasePointParent != null)
                        llPurchasePointParent.setVisibility(View.GONE);
                    /*llButtons.setVisibility(View.VISIBLE);
                    llButtonParent.setVisibility(View.GONE);
                    llPurchasePointParent.setVisibility(View.GONE);
                    llButtons.setVisibility(View.GONE);
                    llPurchasePointParent.setVisibility(View.GONE);
                    //visible one One thing and hendal this button click
                    llButtonParent.setVisibility(View.VISIBLE);*/
                    getData();
                    /*}
                    getData();*/
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            if (getActivity() == null || message == null || errorCode == 0) {
                return;
            }
            Log.d("message", message);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };
    RequestListener secondaryPointsDelhi = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.d("response secndrypnDl", responseObject.toString());

            pref.setSecondaryPointsDelhi(responseObject.toString());

            final SecondaryPointsDelhi secondaryPointsDelhi = JsonUtils.objectify(responseObject.toString(), SecondaryPointsDelhi.class);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Here we are add the Point
                    total_loyalty_points.setText(secondaryPointsDelhi.getMaintained_points());
                    total_loyalty_points2.setText(secondaryPointsDelhi.getMaintained_points());
                    total_purchase_points.setText(secondaryPointsDelhi.getMaintained_points());
                    total_ib_points.setText(secondaryPointsDelhi.getIb_count());
                    total_ups_points.setText(secondaryPointsDelhi.getUps_count());
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener ERickPointsWB = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.d("response ERickPoints", responseObject.toString());

            pref.setERickPointsWB(responseObject.toString());

            final com.tagbin.user.livrewards_loyality.Model.ERickPointsWB eRickPointsWB = JsonUtils.objectify(responseObject.toString(), ERickPointsWB.class);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int count = Integer.parseInt(eRickPointsWB.getCount());
                    //if (!pref.getUserViewFlag() && !pref.getUserViewFlagWB())
                    // total_purchase_points.setText("" + dataModel.getTotal_loyalty());
                    tvERickTotalCount.setText(count + "");
                    if (count == 0) {
                        tvERickTotal.setText("0");
                    } else if (count >= 300 && count <= 599) {
                        tvERickTotal.setText("300");
                    } else if (count >= 600 && count <= 1199) {
                        tvERickTotal.setText("600");
                    } else if (count >= 1200 && count <= 2399) {
                        tvERickTotal.setText("1200");
                    } else if (count >= 2400 && count <= 3599) {
                        tvERickTotal.setText("2400");
                    } else if (count >= 3600 && count <= 4799) {
                        tvERickTotal.setText("3600");
                    } else if (count >= 4800) {
                        tvERickTotal.setText("4800");
                    }
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);

            handleAPIError(message, errorCode);
        }
    };
    RequestListener editProfileListener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response Profile_Update", responseObject.toString());
            DataModel profileModel = JsonUtils.objectify(responseObject.toString(), DataModel.class);
            final PrefManager pref = new PrefManager(getContext());
            LoginResponseModel obj1 = pref.getLoginModel();
            obj1.setData(profileModel);
            pref.saveLoginModel(obj1);
            getActivity().runOnUiThread(new Runnable() {
                @SuppressLint("SetTextI18n")
                @Override
                public void run() {
                    if (pref != null && pref.getLoginModel() != null && pref.getLoginModel().getData() != null && pref.getLoginModel().getData().getSmall_image() != null) {
                        ImageLoader.getInstance().loadImage(pref.getLoginModel().getData().getSmall_image(), new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                profile_pic.setImageBitmap(loadedImage);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                            }
                        });
                    }
                    Log.d("response", String.valueOf(pref.getLoginModel().getData().getFirst_name()));
                    partnerNameTv.setText("Welcome Partner " + pref.getLoginModel().getData().getFirst_name() + " " + pref.getLoginModel().getData().getLast_name());
                    dealer_name_tv.setText(pref.getLoginModel().getData().getDistributor_name());
                    phone_no.setText("" + pref.getLoginModel().getData().getPhone());
                    dealershipNameTv.setText(pref.getLoginModel().getData().getDealership_name());
                    //if (!pref.getUserViewFlag())
                    total_loyalty_points.setText("" + pref.getLoginModel().getData().getTotal_loyalty());
                    total_loyalty_points2.setText("" + pref.getLoginModel().getData().getTotal_loyalty());
                    // if (!pref.getUserViewFlag()) {
                    total_purchase_points.setText("" + pref.getLoginModel().getData().getTotal_loyalty());
                    //}
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener homecountListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.e("response homecountL", responseObject.toString());
            JSONObject jObj = new JSONObject(responseObject.toString());

            boolean isShow = false;
            if (jObj.has("is_show"))
                isShow = jObj.getBoolean("is_show");
            if (!isShow)
                return;
//            if (jObj.has("counts")){
//                JSONArray countsArray = jObj.getJSONArray("counts");
//                for(int i = 0; i < countsArray.length(); i++){
//                    JSONObject countObject = countsArray.getJSONObject(i);
//                    String name = countObject.getString("name");
//                    int count = countObject.getInt("count");
//                    if(name.equalsIgnoreCase("i2-verter") || name.equalsIgnoreCase("i-verter") || name.equalsIgnoreCase("iH-verter")){
//                        ivCount += count;
//                    } else {
//                        ibCount += count;
//                    }
//                }
//            }
            try{
                final String msg = (jObj.getString("msg"));
                final String title = (jObj.getString("title"));
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Handler mHandler = new Handler();
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                // int point = 1500 - Integer.parseInt(String.valueOf(loyality));
                                if (getActivity() != null) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setTitle(title);
                                    builder.setIcon(R.mipmap.ic_launcher);
                                    builder.setCancelable(false);
                                    builder.setMessage(msg)
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            });
                                    final AlertDialog alert = builder.create();
                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (!getActivity().isFinishing()) {
                                                    try {
                                                        alert.show();
                                                    } catch (WindowManager.BadTokenException e) {
                                                        Log.e("WindowManagerBad ", e.toString());
                                                    } catch (Exception e) {
                                                        Log.e("Exception ", e.toString());
                                                    }
                                                }
                                            }
                                        });
                                }
                            }
                        });
                    }
                });
            }catch (Exception e){
                return;
            }



        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.e("error homecountListner", "errorcode=" + errorCode + "      msg=" + message);
            handleAPIError(message, errorCode);
        }
    };
    private AutoScrollViewPager viewPager;
    RequestListener bannerListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response bannerListener", responseObject.toString());
            banner.clear();
            Type collectionType = new TypeToken<List<BannerModel>>() {
            }.getType();
            final List<BannerModel> ca = (List<BannerModel>) new Gson().fromJson(responseObject.toString(), collectionType);
            for (int i = 0; i < ca.size(); i++) {
                banner.add(ca.get(i).getLarge_image());
            }
            if (((Activity) getContext()) != null) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mySwiprRefreshLayout.setRefreshing(false);
                        viewPager.setplaceImage(R.drawable.empty_graphical_banner);
                        viewPager.setImageUrls(banner);
                        viewPager.start();
                        viewPager.setAutoPagerListener(new AutoScrollViewPager.AutoScrollerViewPagerListener() {
                            @Override
                            public void onPageSelected(int position) {
                            }

                            @Override
                            public void onPageClicked(int position) {
                            }
                        });
                    }
                });
            }

            //this is used to show the spacial view which is set from admin panel
            GetViewFlag getViewFlag = new GetViewFlag();
            getViewFlag.setId(dataModel.getId());
            getViewFlag.setCity(dataModel.getCity());
            getViewFlag.setState(dataModel.getState());
            Controller.getViewFlagAndData(getContext(), getViewFlag, onApiViewFlagAndData);
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            handleAPIError(message, errorCode);
        }
    };


    @SuppressLint("ValidFragment")
    public home_fragment(Context context1) {
        context = context1;
    }


    public home_fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // click_to_redeem = getView().findViewById(R.id.go_to_redeem_click);

        pref = new PrefManager(getContext());
        if (isUserInfoNotFound()) {
            logout();
            return;
        }

        progressBar = getView().findViewById(R.id.progressbar);
        profile_pic = getView().findViewById(R.id.profilePic);
        dealer_name_tv = getView().findViewById(R.id.distributorNameTv);
        dealershipNameTv = getView().findViewById(R.id.dealershipNameTv);
        partnerNameTv = getView().findViewById(R.id.partnerNameTv);
        phone_no = getView().findViewById(R.id.dealer_no);
        viewPager = getView().findViewById(R.id.viewpager);
        total_loyalty_points = getView().findViewById(R.id.total_loyalty_points);
        total_loyalty_points2 = getView().findViewById(R.id.total_loyalty_points2);
        total_warranty_points = getView().findViewById(R.id.total_loyalty_points1);
        manufacturer_spinner = getView().findViewById(R.id.manufacturer_spinner);
        car_model_spinner = getView().findViewById(R.id.car_model_spinner);
        rlIBPlusUPS = getView().findViewById(R.id.rlIBPlusUPS);
        rlERick = getView().findViewById(R.id.rlERick);
        points_RL = getView().findViewById(R.id.points_RL);
        points_RL2 = getView().findViewById(R.id.points_RL2);
        points_wrranty = getView().findViewById(R.id.points_RL1);
        segmnet_rv = getView().findViewById(R.id.segment_recycler_view);
        fuel_spinner = getView().findViewById(R.id.fuel_spinner);
        profilePicRL = getView().findViewById(R.id.profilePicRL);
        battery_model_rv = getView().findViewById(R.id.battery_model);
        mySwiprRefreshLayout = getView().findViewById(R.id.swiperefresh);
        mySwiprRefreshLayout.setEnabled(false);

        llButtons = getView().findViewById(R.id.llButtons);
        llButtons.setVisibility(View.GONE);
        llERick = getView().findViewById(R.id.llERick);
        llERick.setVisibility(View.GONE);
        llIBplusUPS = getView().findViewById(R.id.llIBplusUPS);
        llIBplusUPS.setVisibility(View.GONE);
        llIBAndUPSParent = getView().findViewById(R.id.llIBAndUPSParent);
        llButtonParent = getView().findViewById(R.id.llButtonParent);
        llButtonParent.setVisibility(View.VISIBLE);
        llPurchasePointParent = getView().findViewById(R.id.llPurchasePointParent);
        llPurchasePointParent.setVisibility(View.GONE);

        total_purchase_points = getView().findViewById(R.id.total_purchase_points);
        total_ib_points = getView().findViewById(R.id.total_ib_points);
        total_ups_points = getView().findViewById(R.id.total_ups_points);
        tvERickTotal = getView().findViewById(R.id.tvERickTotal);
        tvERickTotalCount = getView().findViewById(R.id.tvERickTotalCount);
        tv_purchase = getView().findViewById(R.id.tv_purchase);
        tv_tertiary = getView().findViewById(R.id.tv_tertiary);

        rlPurchase = getView().findViewById(R.id.rlPurchase);
        rlPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* if (pref.getLoginModel() != null || pref.getLoginModel().getData() != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("USER_ID", pref.getLoginModel().getData().getId() + "");
                    bundle.putString("BAT_CODE", pref.getLoginModel().getData().getUsername() + "");
                    bundle.putString("USER_NAME", pref.getLoginModel().getData().getFirst_name());
                    bundle.putString("KEY_EVENT", "Purchase_button");
                    firebaseAnalytics.logEvent("Home_Fragment", bundle);
                }*/

                tv_purchase.setTextColor(Color.parseColor("#FFFFFF"));
                tv_tertiary.setTextColor(Color.parseColor("#c71006"));
                rlPurchase.setBackground(getResources().getDrawable(R.drawable.black_rounded_background));
                rlTertiary.setBackground(getResources().getDrawable(R.drawable.white_rounded_background));

              /*  llButtonParent.setVisibility(View.VISIBLE);
                llPurchasePointParent.setVisibility(View.VISIBLE);

                points_RL.setVisibility(View.GONE);
                points_wrranty.setVisibility(View.GONE);

                getDataForWB();
                getDataForDelhi();*/

               /* if (pref.getUserViewFlag()) {
                    llButtonParent.setVisibility(View.VISIBLE);
                    llPurchasePointParent.setVisibility(View.VISIBLE);
                    llIBplusUPS.setVisibility(View.VISIBLE);
                    llIBAndUPSParent.setVisibility(View.VISIBLE);
                    llERick.setVisibility(View.VISIBLE);

                    points_RL.setVisibility(View.GONE);
                    points_wrranty.setVisibility(View.GONE);

                    getDataForDelhi();
                    getDataForWB();
                } else */
               /* if (pref.getUserViewFlagWB()) {
                    llButtonParent.setVisibility(View.VISIBLE);
                    llPurchasePointParent.setVisibility(View.VISIBLE);
                    llERick.setVisibility(View.VISIBLE);
                    llIBplusUPS.setVisibility(View.VISIBLE);

                    points_RL.setVisibility(View.GONE);
                    points_RL2.setVisibility(View.GONE);
                    points_wrranty.setVisibility(View.GONE);
                    llIBAndUPSParent.setVisibility(View.GONE);

                    getDataForWB();
                } else {
                    llButtonParent.setVisibility(View.VISIBLE);
                    llPurchasePointParent.setVisibility(View.VISIBLE);
                    llERick.setVisibility(View.VISIBLE);
                    llIBplusUPS.setVisibility(View.VISIBLE);

                    points_RL.setVisibility(View.GONE);
                    points_wrranty.setVisibility(View.GONE);
                    llIBAndUPSParent.setVisibility(View.GONE);

                    getDataForWB();

                }*/

               /* llButtonParent.setVisibility(View.VISIBLE);
                llPurchasePointParent.setVisibility(View.VISIBLE);
                llERick.setVisibility(View.VISIBLE);
                points_RL2.setVisibility(View.VISIBLE);

                llIBplusUPS.setVisibility(View.GONE);
                points_RL.setVisibility(View.GONE);
                points_wrranty.setVisibility(View.GONE);
                llIBAndUPSParent.setVisibility(View.GONE);

                getDataForWB();*/

                /*if (pref.getUserViewFlag()) {
                    llButtonParent.setVisibility(View.VISIBLE);
                    llPurchasePointParent.setVisibility(View.VISIBLE);
                    llERick.setVisibility(View.VISIBLE);
                    llIBplusUPS.setVisibility(View.VISIBLE);

                    points_RL.setVisibility(View.GONE);
                    points_RL2.setVisibility(View.GONE);
                    points_wrranty.setVisibility(View.GONE);
                    llIBAndUPSParent.setVisibility(View.VISIBLE);

                    getDataForDelhi();
                } else {*/
                llButtonParent.setVisibility(View.VISIBLE);
                llPurchasePointParent.setVisibility(View.VISIBLE);
                llERick.setVisibility(View.VISIBLE);
                llIBplusUPS.setVisibility(View.VISIBLE);

                points_RL.setVisibility(View.GONE);
                points_RL2.setVisibility(View.GONE);
                points_wrranty.setVisibility(View.GONE);
                llIBAndUPSParent.setVisibility(View.GONE);

                getDataForWB();
                //}
            }
        });

        rlTertiary = getView().findViewById(R.id.rlTertiary);
        rlTertiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_purchase.setTextColor(Color.parseColor("#c71006"));
                tv_tertiary.setTextColor(Color.parseColor("#FFFFFF"));
                rlPurchase.setBackground(getResources().getDrawable(R.drawable.white_rounded_background));
                rlTertiary.setBackground(getResources().getDrawable(R.drawable.black_rounded_background));

                llButtonParent.setVisibility(View.VISIBLE);
                llPurchasePointParent.setVisibility(View.GONE);

                points_RL.setVisibility(View.GONE);
                points_wrranty.setVisibility(View.VISIBLE);

                getData();
            }
        });

        rlERick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ERickRedeemActivity.class);
                startActivity(intent);
            }
        });

        //showUpdateDialog();

        user_id = String.valueOf(dataModel.getId());

        HomeCountModel homeCountModel = new HomeCountModel();
        homeCountModel.setUser_id(user_id);
        Controller.HomeCount(getContext(), homeCountModel, homecountListner);

        databaseBackend = DatabaseBackend.getInstance(getContext());
        banner = new ArrayList<>();
        Controller.GetBanner(getContext(), bannerListener);

        partnerNameTv.setText("Welcome Partner " + dataModel.getFirst_name() + " " + dataModel.getLast_name());
        dealer_name_tv.setText(dataModel.getDistributor_name());
        phone_no.setText("" + dataModel.getPhone());
        dealershipNameTv.setText(dataModel.getDealership_name());
        //if (!pref.getUserViewFlag())
        total_loyalty_points.setText("" + dataModel.getTotal_loyalty());
        total_loyalty_points2.setText("" + dataModel.getTotal_loyalty());

        points_RL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), RedeemActivity.class);
                startActivity(intent);
            }
        });
        points_RL2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), RedeemActivity.class);
                startActivity(intent);
            }
        });

        rlIBPlusUPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), RedeemActivity.class);
                startActivity(intent);
            }
        });

        points_wrranty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ActivityWarrantyRedeem.class);
                startActivity(intent);
            }
        });
        profilePicRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                startActivity(intent);
            }
        });

       /* if (pref.getUserViewFlag())
            getDataForDelhi();*/

        /*if (pref.getUserViewFlag() || pref.getUserViewFlagWB()) {
            if (llButtons != null)
                llButtons.setVisibility(View.VISIBLE);
            if (llButtonParent != null)
                llButtonParent.setVisibility(View.GONE);
            if (llPurchasePointParent != null)
                llPurchasePointParent.setVisibility(View.GONE);
        } else {
            if (llButtons != null)
                llButtons.setVisibility(View.VISIBLE);
            if (llButtonParent != null)
                llButtonParent.setVisibility(View.GONE);
            if (llPurchasePointParent != null)
                llPurchasePointParent.setVisibility(View.GONE);
            if (llButtons != null)
                llButtons.setVisibility(View.VISIBLE);
            if (llPurchasePointParent != null)
                llPurchasePointParent.setVisibility(View.GONE);
            //visible one One thing and hendal this button click
            if (llButtonParent != null)
                llButtonParent.setVisibility(View.VISIBLE);
        }*/
        if (llButtons != null)
            llButtons.setVisibility(View.VISIBLE);
        if (llButtonParent != null)
            llButtonParent.setVisibility(View.GONE);
        if (llPurchasePointParent != null)
            llPurchasePointParent.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        super.onResume();

        getData();

        getDataForWB();

        //showUpdateDialog();

        if (tv_purchase != null)
            tv_purchase.setTextColor(Color.parseColor("#c71006"));
        if (tv_tertiary != null)
            tv_tertiary.setTextColor(Color.parseColor("#c71006"));
        if (rlPurchase != null)
            rlPurchase.setBackground(getResources().getDrawable(R.drawable.white_rounded_background));
        if (rlTertiary != null)
            rlTertiary.setBackground(getResources().getDrawable(R.drawable.white_rounded_background));

        /*if (pref.getUserViewFlag() || pref.getUserViewFlagWB()) {
            if (llButtons != null)
                llButtons.setVisibility(View.VISIBLE);
            if (llButtonParent != null)
                llButtonParent.setVisibility(View.GONE);
            if (llPurchasePointParent != null)
                llPurchasePointParent.setVisibility(View.GONE);
        } else {
            if (llButtons != null)
                llButtons.setVisibility(View.VISIBLE);
            if (llButtonParent != null)
                llButtonParent.setVisibility(View.GONE);
            if (llPurchasePointParent != null)
                llPurchasePointParent.setVisibility(View.GONE);
            if (llButtons != null)
                llButtons.setVisibility(View.VISIBLE);
            if (llPurchasePointParent != null)
                llPurchasePointParent.setVisibility(View.GONE);
            //visible one One thing and hendal this button click
            if (llButtonParent != null)
                llButtonParent.setVisibility(View.VISIBLE);
        }*/
        if (llButtons != null)
            llButtons.setVisibility(View.VISIBLE);
        if (llButtonParent != null)
            llButtonParent.setVisibility(View.GONE);
        if (llPurchasePointParent != null)
            llPurchasePointParent.setVisibility(View.GONE);
      /*  if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    EditProfileModel editProfileModel = new EditProfileModel();
                    Controller.profileDetailUpadate(getContext(), editProfileModel, editProfileListener);
                }E3
            });

        }*/

        /*if (llButtons != null)
            llButtons.setVisibility(View.VISIBLE);
        if (llButtonParent != null)
            llButtonParent.setVisibility(View.GONE);
        if (llPurchasePointParent != null)
            llPurchasePointParent.setVisibility(View.GONE);*/

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    EditProfileModel editProfileModel = new EditProfileModel();
                    editProfileModel.setUser_id(String.valueOf(pref.getLoginModel().getData().getId()));
                    Controller.profileDetailUpadate(getContext(), editProfileModel, editProfileListener);
                }
            });

        }
    }

    @Override
    public void onItemClick(int value, String name) {
        Log.d("segment_click", "" + name);
        car_segment_id = value;
        strVehicleSegment = name;
        segmnet_rv.setFocusable(false);
    }


    private void logout() {
        pref.clearSession();
        Intent go = new Intent(getContext(), LoginActivity.class);
        startActivity(go);
    }

    private boolean isUserInfoNotFound() {

        LoginResponseModel loginResponseModel = pref.getLoginModel();
        //Check the user info
        if (loginResponseModel == null) {
            return true;
        }

        dataModel = loginResponseModel.getData();
        if (dataModel == null || dataModel.getUsername().isEmpty()) {
            return true;
        }

        return false;
    }

    private void handleAPIError(String message, int errorCode) {
        if ((Activity) getContext() == null || message == null || errorCode == 0 && getContext() != null) {
            return;
        }
        if (errorCode >= 400 && errorCode < 500) {
            if (errorCode == 403) {
                if ((Activity) getContext() != null && getContext() != null) {
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mySwiprRefreshLayout.setRefreshing(false);
                            Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } else if (errorCode == 401) {
                if ((Activity) getContext() != null && getContext() != null) {
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mySwiprRefreshLayout.setRefreshing(false);
//                            logout();
                        }
                    });
                }
            } else {
                final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                if ((Activity) getContext() != null && getContext() != null) {
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mySwiprRefreshLayout.setRefreshing(false);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        } else {
            if ((Activity) getContext() != null && getContext() != null) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mySwiprRefreshLayout.setRefreshing(false);
                        Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    private void getData() {
        LoyalityModel loyalityModel = new LoyalityModel();
        if (dataModel == null) {
            return;
        }
        //user_id = String.valueOf(dataModel.getId());
        loyalityModel.setUser_id(user_id);
        Controller.getloyality(getContext(), loyalityModel, loyalityListner);
    }

    private void getDataForDelhi() {
        AddWarrantyPointAPIModel addWarrantyPointAPIModel = new AddWarrantyPointAPIModel();
        addWarrantyPointAPIModel.setId(String.valueOf(dataModel.getId()));
        Controller.getSecondaryPointsDelhi(getContext(), addWarrantyPointAPIModel, secondaryPointsDelhi);
    }

    private void getDataForWB() {
        if (dataModel != null && String.valueOf(dataModel.getId()) != null && !String.valueOf(dataModel.getId()).isEmpty()) {
            AddWarrantyPointAPIModel addWarrantyPointAPIModel = new AddWarrantyPointAPIModel();
            addWarrantyPointAPIModel.setUser_id(String.valueOf(dataModel.getId()));
            Controller.getERickPointsWB(getContext(), addWarrantyPointAPIModel, ERickPointsWB);
        }
    }

    /*private void showUpdateDialog() {
        //------getting version code for application----\\
        GetVersionCode getVersionCode = new GetVersionCode();
        try {
            Latest_verion = getVersionCode.execute().get();
            Log.d("version", String.valueOf(Latest_verion));
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            currentVersion = (pInfo.versionName);
            Log.d("version", String.valueOf(currentVersion));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
//-------Rendering to playstore is current version is less or more  than playstore-----\\
        if (Latest_verion != null)
            if (!currentVersion.equals(Latest_verion)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Our App got Update");
                builder.setIcon(R.mipmap.ic_launcher);
                builder.setCancelable(false);
                builder.setMessage("New version available, select update to update our app")
                        .setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final String appName = getActivity().getPackageName();

                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
                                }

                            }
                        });

                final AlertDialog alert = builder.create();
                if (getActivity() != null)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!getActivity().isFinishing()) {
                                try {
                                    alert.show();
                                } catch (WindowManager.BadTokenException e) {
                                    Log.e("WindowManagerBad ", e.toString());
                                } catch (Exception e) {
                                    Log.e("Exception ", e.toString());
                                }
                            }
                        }
                    });
            }
    }*/
}


