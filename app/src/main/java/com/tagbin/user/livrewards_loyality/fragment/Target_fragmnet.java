package com.tagbin.user.livrewards_loyality.fragment;

import android.Manifest;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.horizontalnumberpicker.HorizontalNumberPicker;
import com.tagbin.user.livrewards_loyality.Database.DatabaseBackend;
import com.tagbin.user.livrewards_loyality.Interface.OnItemRemoveListner;
import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.CalculateLoyaltyPointsModel;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.ProductListForCalculateModel;
import com.tagbin.user.livrewards_loyality.Model.ProductLoyalityModel;
import com.tagbin.user.livrewards_loyality.Model.ResultCalculatedPointsModel;
import com.tagbin.user.livrewards_loyality.Model.TargetsModel;
import com.tagbin.user.livrewards_loyality.Model.WarrantyLoyalityModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.adapter.LoyaltyPointsProductAdapter;
import com.tagbin.user.livrewards_loyality.adapter.TargetRecyclerViewAdapter;
import com.tagbin.user.livrewards_loyality.adapter.WarrantyPointProductAdpter;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.MyUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class Target_fragmnet extends Fragment implements com.horizontalnumberpicker.HorizontalNumberPickerListener, OnItemRemoveListner {
    private static String[] PERMISSIONS_READ_WRITE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    Spinner battery_current_spinner, battery_warranty_spinner, battery_current_spinner1, battery_warranty_spinner1;
    TextView purchase_date, clear_all_tv, clear_all_tv1, purchase_date1;
    RecyclerView totalProdct_rv, totalProdct_rv1;
    LoyaltyPointsProductAdapter loyaltyPointsProductAdapter;
    WarrantyPointProductAdpter warrantyPointProductAdpter;
    List<ProductLoyalityModel> addedProductList;
    List<WarrantyLoyalityModel> addedProductList1;
    RecyclerView target_rv;
    //SwipeRefreshLayout swipeRefreshLayout,warrantyRefreshLayout;
    RelativeLayout swipeRefreshLayout, warrantyRefreshLayout;
    List<TargetsModel> targetsList, targetsList1;
    Button calculate_button, calculate_button1;
    Button pdf_button, pdf_button1, pdf_button2;
    TargetRecyclerViewAdapter targetAdapter;
    ProgressBar progressBar;
    Context context;
    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int day = calendar.get(Calendar.DAY_OF_MONTH);
    AnimatorSet animatorSet;
    RelativeLayout calculater_RL, offer_RL;
    TextView calculator_tv, offer_tv;
    LinearLayout tab_layout1;
    List<ProductLoyalityModel> productLoyalityList;
    List<WarrantyLoyalityModel> productLoyalityList1;
    DatabaseBackend databaseBackend;
    List<String> battery_Ah_List;
    List<String> batterry_Ah_List1;
    List<String> warrantyList;
    List<String> warrantyList1;
    RelativeLayout pdfdownload;
    RelativeLayout empty_relative_layout;
    String selected_bettary_ah, selected_warranty, selected_bettary_ah1, selected_warranty1;
    String str_start_date;
    CalculateLoyaltyPointsModel calculateLoyaltyPointsModel, calculateLoyaltyPointsModel1;
    List<ProductListForCalculateModel> productListForCalculateModelsList, productListForCalculateModelsList1;
    LinearLayoutManager targetManager, calculatormanager;
    ArrayAdapter<String> battery_warranty_adapte, battery_warranty_adapte1;
    ScrollView scrollView, scrollView1;
    TextView pdfLink, pdflink1, pdflink2;
    DownloadManager downloadManager;
    Long refrence;
    TextView Purchasecalculator, WarrantyCalculator;
    PrefManager prefManager;
    View.OnClickListener calculateClick = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onClick(View v) {
            if (addedProductList.size() == 0) {
                Toast.makeText(getActivity(), "Please select your battery", Toast.LENGTH_SHORT).show();
            } else {
                Log.d("AdapterArraySize", "" + addedProductList.size());
                productListForCalculateModelsList.clear();
                calculateLoyaltyPointsModel = new CalculateLoyaltyPointsModel();
                calculateLoyaltyPointsModel.setEnd_date(str_start_date);
                for (int i = 0; i < addedProductList.size(); i++) {
                    ProductListForCalculateModel productListForCalculateModel = new ProductListForCalculateModel();
                    productListForCalculateModel.setId(String.valueOf(addedProductList.get(i).getId()));
                    productListForCalculateModel.setQuantity(String.valueOf(addedProductList.get(i).getQuantity()));
                    productListForCalculateModelsList.add(productListForCalculateModel);
                }
                calculateLoyaltyPointsModel.setProducts(productListForCalculateModelsList);
                progressBar.setVisibility(View.VISIBLE);
                Controller.CalculateLoyalty(getContext(), calculateLoyaltyPointsModel, new RequestListener() {
                    @Override
                    public void onRequestStarted() {
                    }

                    @Override
                    public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
                        Log.d("CalculateLoyalty", responseObject.toString());
                        final ResultCalculatedPointsModel resultCalculatedPointsModel = JsonUtils.objectify(responseObject.toString(), ResultCalculatedPointsModel.class);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.flip);
                                animatorSet.setTarget(calculate_button);
                                calculate_button.setText("Points earned: " + resultCalculatedPointsModel.getTotal_point());
                                calculate_button.setBackgroundResource(R.drawable.green_rounded_button_background);
                                calculate_button.setTextColor(Color.WHITE);
                                calculate_button.setClickable(false);
                                animatorSet.start();
                            }
                        });
                    }

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onRequestError(int errorCode, String message) {
                        Log.d("response", message);
                        if (errorCode >= 400 && errorCode < 500) {
                            if (errorCode == 403) {
                                if ((Activity) getContext() != null) {
                                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressBar.setVisibility(View.GONE);
                                            Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            } else if (errorCode == 401) {
                                if ((Activity) getContext() != null) {
                                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressBar.setVisibility(View.GONE);
//                                          logout();
                                        }
                                    });
                                }
                            } else {
                                final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                                if ((Activity) getContext() != null) {
                                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressBar.setVisibility(View.GONE);
                                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                                Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(getContext(), "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }
                            }
                        } else {
                            if ((Activity) getContext() != null) {
                                ((Activity) getContext()).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressBar.setVisibility(View.GONE);
                                        Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    }
                });
            }
        }
    };
    View.OnClickListener calculatewarrantyclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (addedProductList1.size() == 0) {
                Toast.makeText(getActivity(), "Please select your battery", Toast.LENGTH_SHORT).show();
            } else {
                Log.d("AdapterArraySize", "" + addedProductList1.size());
                productListForCalculateModelsList1.clear();
                calculateLoyaltyPointsModel1 = new CalculateLoyaltyPointsModel();
                calculateLoyaltyPointsModel1.setEnd_date(str_start_date);
                for (int i = 0; i < addedProductList1.size(); i++) {
                    ProductListForCalculateModel productListForCalculateModel = new ProductListForCalculateModel();
                    productListForCalculateModel.setId(String.valueOf(addedProductList1.get(i).getId()));
                    productListForCalculateModel.setQuantity(String.valueOf(addedProductList1.get(i).getQuantity()));
                    productListForCalculateModelsList1.add(productListForCalculateModel);
                }
                calculateLoyaltyPointsModel1.setProducts(productListForCalculateModelsList1);
                progressBar.setVisibility(View.VISIBLE);
                Controller.CalculateWarrantyLoyalty(getContext(), calculateLoyaltyPointsModel1, new RequestListener() {
                    @Override
                    public void onRequestStarted() {
                    }

                    @Override
                    public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
                        Log.d("response", responseObject.toString());
                        final ResultCalculatedPointsModel resultCalculatedPointsModel = JsonUtils.objectify(responseObject.toString(), ResultCalculatedPointsModel.class);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.flip);
                                animatorSet.setTarget(calculate_button1);
                                calculate_button1.setText("Points earned: " + resultCalculatedPointsModel.getTotal_point());
                                calculate_button1.setBackgroundResource(R.drawable.green_rounded_button_background);
                                calculate_button1.setTextColor(Color.WHITE);
                                calculate_button1.setClickable(false);
                                animatorSet.start();
                            }
                        });
                    }

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onRequestError(int errorCode, String message) {
                        Log.d("response", message);
                        if (errorCode >= 400 && errorCode < 500) {
                            if (errorCode == 403) {
                                if ((Activity) getContext() != null) {
                                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressBar.setVisibility(View.GONE);
                                            Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            } else if (errorCode == 401) {
                                if ((Activity) getContext() != null) {
                                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressBar.setVisibility(View.GONE);
//                                          logout();
                                        }
                                    });
                                }
                            } else {
                                final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                                if ((Activity) getContext() != null) {
                                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressBar.setVisibility(View.GONE);
                                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                                Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(getContext(), "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }
                            }
                        } else {
                            if ((Activity) getContext() != null) {
                                ((Activity) getContext()).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressBar.setVisibility(View.GONE);
                                        Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    }
                });
            }
        }
    };
    View.OnClickListener purchaseClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                @TargetApi(Build.VERSION_CODES.M)
                @Override
                public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {
                    final String select_date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            purchase_date.setText(select_date);
                            str_start_date = MyUtils.getYearlyFormat(select_date);
                            refreshButton();
                            battery_current_spinner.setSelection(0);
                            battery_warranty_spinner.setSelection(0);
                        }
                    });
                }
            }, year, month, day);
           // dpd.setMaxDate(calendar);
            dpd.show(((Activity) context).getFragmentManager(), "DATE_PICKER_TAG");
        }
    };
    View.OnClickListener warrantyclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                @TargetApi(Build.VERSION_CODES.M)
                @Override
                public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {
                    final String select_date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            purchase_date1.setText(select_date);
                            str_start_date = MyUtils.getYearlyFormat(select_date);
                            refreshButton();
                            battery_current_spinner1.setSelection(0);
                            battery_warranty_spinner1.setSelection(0);
                        }
                    });
                }
            }, year, month, day);
//            dpd.setMaxDate(calendar);
            dpd.show(((Activity) context).getFragmentManager(), "DATE_PICKER_TAG");
        }
    };
    View.OnClickListener clear_all_click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (addedProductList.size() > 0) {
                final Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_logout);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                TextView main_heading = (TextView) dialog.findViewById(R.id.successmsg);
                TextView msg = (TextView) dialog.findViewById(R.id.bettarymsg);
                final Button addmore = (Button) dialog.findViewById(R.id.addmore);
                final TextView close = (TextView) dialog.findViewById(R.id.close);
                main_heading.setText("Clear All");
                msg.setText("Are you sure you want to clear all\nthe added product");
                addmore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        refreshButton();
                        battery_current_spinner.setSelection(0);
                        battery_warranty_spinner.setSelection(0);
                        addedProductList.clear();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loyaltyPointsProductAdapter.notifyDataSetChanged();
                            }
                        });
                        dialog.cancel();
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                if (getActivity() != null)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!(getActivity()).isFinishing()) {
                                try {
                                    dialog.show();
                                } catch (WindowManager.BadTokenException e) {
                                    Log.e("WindowManagerBad ", e.toString());
                                } catch (Exception e) {
                                    Log.e("Exception ", e.toString());
                                }
                            }
                        }
                    });
            }
        }
    };
    View.OnClickListener clearwarranty_all_click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (addedProductList1.size() > 0) {
                final Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_logout);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                TextView main_heading = (TextView) dialog.findViewById(R.id.successmsg);
                TextView msg = (TextView) dialog.findViewById(R.id.bettarymsg);
                final Button addmore = (Button) dialog.findViewById(R.id.addmore);
                final TextView close = (TextView) dialog.findViewById(R.id.close);
                main_heading.setText("Clear All");
                msg.setText("Are you sure you want to clear all\nthe added product");
                addmore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        refreshButton();
                        battery_current_spinner1.setSelection(0);
                        battery_warranty_spinner1.setSelection(0);
                        addedProductList1.clear();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                warrantyPointProductAdpter.notifyDataSetChanged();
                            }
                        });
                        dialog.cancel();
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                if (getActivity() != null)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!(getActivity()).isFinishing()) {
                                try {
                                    dialog.show();
                                } catch (WindowManager.BadTokenException e) {
                                    Log.e("WindowManagerBad ", e.toString());
                                } catch (Exception e) {
                                    Log.e("Exception ", e.toString());
                                }
                            }
                        }
                    });
            }
        }
    };
    RequestListener loyalityListner = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @SuppressLint("LongLogTag")
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.d("response GetProductLoyality", responseObject.toString());
            productLoyalityList.clear();
            databaseBackend.deleteFilterData();
            final Type collection = new TypeToken<List<ProductLoyalityModel>>() {
            }.getType();
            List<ProductLoyalityModel> plp = (List<ProductLoyalityModel>) new Gson().fromJson(responseObject.toString(), collection);
            for (int i = 0; i < plp.size(); i++) {
                ProductLoyalityModel productLoyalityModel = plp.get(i);
                productLoyalityModel.setQuantity(1);
                databaseBackend.createFilterData(productLoyalityModel);
            }
            productLoyalityList.addAll(plp);
            warrantyList.add("Battery Warranty");
            List<String> apiBattryAh = new ArrayList<>();
            for (int i = 0; i < productLoyalityList.size(); i++) {
                apiBattryAh.add(productLoyalityList.get(i).getCapacity());
            }
            Set<String> brandhash = new HashSet<>();
            brandhash.addAll(apiBattryAh);
            apiBattryAh.clear();
            apiBattryAh.addAll(brandhash);
            battery_Ah_List.addAll(apiBattryAh);
            ((Activity) getContext()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    calculate_button.setEnabled(true);
                    final ArrayAdapter<String> battery_current_adapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_items, battery_Ah_List);
                    battery_current_adapter.setDropDownViewResource(R.layout.spinner_drop_down);
                    battery_current_spinner.setAdapter(battery_current_adapter);
                    battery_current_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position == 0) {
                                battery_current_spinner.setSelected(false);
                                battery_warranty_spinner.setEnabled(false);
                            } else {
                                battery_warranty_spinner.setEnabled(true);
                                selected_bettary_ah = battery_Ah_List.get(position);
                                battery_warranty_spinner.setSelection(0);
                                warrantyList.removeAll(warrantyList);
                                warrantyList.add("Battery Warranty");
                                for (int i = 0; i < databaseBackend.getFilterableBatteryWarranty(selected_bettary_ah).size(); i++) {
                                    warrantyList.add(databaseBackend.getFilterableBatteryWarranty(selected_bettary_ah).get(i));
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                    battery_warranty_adapte = new ArrayAdapter<String>(getContext(), R.layout.spinner_items, warrantyList);
                    battery_warranty_adapte.setDropDownViewResource(R.layout.spinner_drop_down);
                    battery_warranty_spinner.setAdapter(battery_warranty_adapte);
                    battery_warranty_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position == 0) {
                                battery_warranty_spinner.setSelected(false);
                            } else {
                                selected_warranty = warrantyList.get(position);
                                refreshButton();
                                int strproductid = databaseBackend.getFilterableProduct(selected_bettary_ah, selected_warranty).get(0).getId();
                                Log.d("onselctId", "" + strproductid);
                                boolean flag = true;
                                if (addedProductList.size() > 0) {
                                    for (int i = 0; i < addedProductList.size(); i++) {
                                        if (strproductid == addedProductList.get(i).getId()) {
                                            flag = false;
                                            int quantity = addedProductList.get(i).getQuantity();
                                            addedProductList.get(i).setQuantity(quantity + 1);
                                            break;
                                        } else {
                                            flag = true;
                                        }
                                    }
                                    if (flag == true) {
                                        addedProductList.add(databaseBackend.getFilterableProduct(selected_bettary_ah, selected_warranty).get(0));
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                loyaltyPointsProductAdapter.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loyaltyPointsProductAdapter.notifyDataSetChanged();
                                            battery_current_spinner.setSelection(0);
                                            battery_warranty_spinner.setSelection(0);
                                        }
                                    });
                                } else {
                                    Log.d("CheckAdapter", "Adapter is null");
                                    addedProductList.add(databaseBackend.getFilterableProduct(selected_bettary_ah, selected_warranty).get(0));
                                    calculatormanager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                                    loyaltyPointsProductAdapter = new LoyaltyPointsProductAdapter(getContext(), addedProductList, Target_fragmnet.this, Target_fragmnet.this);
                                    totalProdct_rv.setLayoutManager(calculatormanager);
                                    totalProdct_rv.setAdapter(loyaltyPointsProductAdapter);
                                    totalProdct_rv.setNestedScrollingEnabled(false);
                                    totalProdct_rv.setHasFixedSize(true);
                                    totalProdct_rv.setNestedScrollingEnabled(false);
                                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loyaltyPointsProductAdapter.notifyDataSetChanged();
                                            battery_current_spinner.setSelection(0);
                                            battery_warranty_spinner.setSelection(0);
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                    /*
                    *By Sanjay
                    * totalProdct_rv.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                        @Override
                        public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                            try {
                                int firstPos = calculatormanager.findFirstCompletelyVisibleItemPosition();
                                if (firstPos > 0) {
                                    swipeRefreshLayout.setEnabled(false);
                                } else {
                                    swipeRefreshLayout.setEnabled(true);
                                    if(totalProdct_rv.getScrollState() == 1)
                                        if(swipeRefreshLayout.isRefreshing())
                                            totalProdct_rv.stopScroll();
                                }
                            }catch(Exception e) {
                                Log.e(TAG, "Scroll Error : "+e.getLocalizedMessage());
                            }
                        }
                    });*/
                }
            });
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if ((Activity) getContext() == null) {
                return;
            }
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
//                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };
    RequestListener TertiaryListener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response", responseObject.toString());
            final String URl = responseObject.toString();
            pdf_button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (hasReadWritePermissionGranted()) {
                        downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                        String s = URl.replaceAll(" ", "%20");
                        String uri = (s);
                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(uri));
                        String fileName = "LATEST_OFFERS_TERTIARY.pdf";
                        //If file is exits delete first
                        /*try{
                            File oldFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Liv-Guard/" + fileName);
                            if(oldFile.exists())
                                oldFile.delete();
                        }catch (Exception e){
                            e.printStackTrace();
                        }*/
                        request.setDestinationInExternalPublicDir("/Liv-Guard", fileName);
                        request.setTitle(fileName);
                        request.setNotificationVisibility(1);
                        request.allowScanningByMediaScanner();
                        request.setMimeType("application/pdf");
                        refrence = downloadManager.enqueue(request);
                        Toast.makeText(getContext(), "DOWNLOADING", Toast.LENGTH_SHORT).show();
                    } else {
                        requestReadWritePermission();
                    }

                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener SchemeListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response", responseObject.toString());
            final String URl = responseObject.toString();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pdf_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (hasReadWritePermissionGranted()) {
                                downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                                String s = URl.replaceAll(" ", "%20");
                                String uri = (s);
                                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(uri));
                                String fileName = "LATEST_OFFERS_SECONDARY.pdf";
                                //If file is exits delete first
                                /*try{
                                    File oldFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Liv-Guard/" + fileName);
                                    if(oldFile.exists())
                                        oldFile.delete();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }*/
                                request.setDestinationInExternalPublicDir("/Liv-Guard", fileName);
                                request.setTitle(fileName);
                                request.setNotificationVisibility(1);
                                request.allowScanningByMediaScanner();
                                request.setMimeType("application/pdf");
                                refrence = downloadManager.enqueue(request);
                                Toast.makeText(getContext(), "DOWNLOADING", Toast.LENGTH_SHORT).show();
                            } else {
                                requestReadWritePermission();
                            }

                        }
                    });
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener SchemeListnererickshaw = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response Erick", responseObject.toString());
            final String URl = responseObject.toString();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pdf_button2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (hasReadWritePermissionGranted()) {
                                downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                                String s = URl.replaceAll(" ", "%20");
                                String uri = (s);
                                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(uri));
                                String fileName = "LATEST_OFFERS_E_RICK.pdf";
                                //If file is exits delete first
                                /*try{
                                    File oldFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Liv-Guard/" + fileName);
                                    if(oldFile.exists())
                                        oldFile.delete();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }*/
                                request.setDestinationInExternalPublicDir("/Liv-Guard", fileName);
                                request.setTitle(fileName);
                                request.setNotificationVisibility(1);
                                request.allowScanningByMediaScanner();
                                request.setMimeType("application/pdf");
                                refrence = downloadManager.enqueue(request);
                                Toast.makeText(getContext(), "DOWNLOADING", Toast.LENGTH_SHORT).show();
                            } else {
                                requestReadWritePermission();
                            }

                        }
                    });
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener WarrantybatteryListner = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.d("response", responseObject.toString());
            productLoyalityList1.clear();
            databaseBackend.deleteFilterData();
            final Type collection = new TypeToken<List<WarrantyLoyalityModel>>() {
            }.getType();
            List<WarrantyLoyalityModel> plp = (List<WarrantyLoyalityModel>) new Gson().fromJson(responseObject.toString(), collection);
            for (int i = 0; i < plp.size(); i++) {
                WarrantyLoyalityModel warrantyLoyalityModel = plp.get(i);
                warrantyLoyalityModel.setQuantity(1);
                databaseBackend.createFilterWarrantyData(warrantyLoyalityModel);
            }
            productLoyalityList1.addAll(plp);
            warrantyList1.add("Battery Warranty");
            List<String> apiBattryAh = new ArrayList<>();
            for (int i = 0; i < productLoyalityList1.size(); i++) {
                apiBattryAh.add(productLoyalityList1.get(i).getCapacity());
            }
            Set<String> brandhash = new HashSet<>();
            brandhash.addAll(apiBattryAh);
            apiBattryAh.clear();
            apiBattryAh.addAll(brandhash);
            batterry_Ah_List1.addAll(apiBattryAh);
            ((Activity) getContext()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    calculate_button1.setEnabled(true);
                    final ArrayAdapter<String> battery_current_adapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_items, batterry_Ah_List1);
                    battery_current_adapter.setDropDownViewResource(R.layout.spinner_warranty_drop_down);
                    battery_current_spinner1.setAdapter(battery_current_adapter);
                    battery_current_spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position == 0) {
                                battery_current_spinner1.setSelected(false);
                                battery_warranty_spinner1.setEnabled(false);
                            } else {
                                battery_warranty_spinner1.setEnabled(true);
                                selected_bettary_ah1 = batterry_Ah_List1.get(position);
                                Log.d("batteryah", selected_bettary_ah1);
                                battery_warranty_spinner1.setSelection(0);
                                warrantyList1.removeAll(warrantyList1);
                                warrantyList1.add("Battery Warranty");
                                warrantyList1.addAll(databaseBackend.getFilterableBatteryWarnty(selected_bettary_ah1));

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                    battery_warranty_adapte1 = new ArrayAdapter<String>(getContext(), R.layout.spinner_items, warrantyList1);
                    battery_warranty_adapte1.setDropDownViewResource(R.layout.spinner_warranty_drop_down);
                    battery_warranty_spinner1.setAdapter(battery_warranty_adapte1);
                    battery_warranty_spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position == 0) {
                                battery_warranty_spinner1.setSelected(false);

                            } else {
                                selected_warranty1 = warrantyList1.get(position);
                                Log.d("warrantyah", selected_warranty1);
                                refreshButton();
                                int strproductid = databaseBackend.getFilterableProductByWarranty(selected_bettary_ah1, selected_warranty1).get(0).getId();
                                Log.d("onselctId", "" + strproductid);
                                boolean flag = true;
                                if (addedProductList1.size() > 0) {

                                    for (int i = 0; i < addedProductList1.size(); i++) {
                                        if (strproductid == addedProductList1.get(i).getId()) {
                                            flag = false;
                                            int quantity = addedProductList1.get(i).getQuantity();
                                            addedProductList1.get(i).setQuantity(quantity + 1);
                                            break;
                                        } else {
                                            flag = true;
                                        }
                                    }
                                    if (flag == true) {
                                        addedProductList1.add(databaseBackend.getFilterableProductByWarranty(selected_bettary_ah1, selected_warranty1).get(0));
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                warrantyPointProductAdpter.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            warrantyPointProductAdpter.notifyDataSetChanged();
                                            battery_current_spinner1.setSelection(0);
                                            battery_warranty_spinner1.setSelection(0);
                                        }
                                    });
                                } else {
                                    Log.d("CheckAdapter", "Adapter is null");
                                    addedProductList1.add(databaseBackend.getFilterableProductByWarranty(selected_bettary_ah1, selected_warranty1).get(0));
                                    calculatormanager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                                    warrantyPointProductAdpter = new WarrantyPointProductAdpter(getContext(), addedProductList1, Target_fragmnet.this, Target_fragmnet.this);
                                    totalProdct_rv1.setLayoutManager(calculatormanager);
                                    totalProdct_rv1.setAdapter(warrantyPointProductAdpter);
                                    totalProdct_rv1.setNestedScrollingEnabled(false);
                                    totalProdct_rv1.setHasFixedSize(true);
                                    totalProdct_rv1.setNestedScrollingEnabled(false);
                                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            warrantyPointProductAdpter.notifyDataSetChanged();
                                            battery_current_spinner1.setSelection(0);
                                            battery_warranty_spinner1.setSelection(0);
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                    /*totalProdct_rv1.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                        @Override
                        public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                            try {
                                int firstPos = calculatormanager.findFirstCompletelyVisibleItemPosition();
                                if (firstPos > 0) {
                                    warrantyRefreshLayout.setEnabled(false);
                                } else {
                                    warrantyRefreshLayout.setEnabled(true);
                                    if(totalProdct_rv1.getScrollState() == 1)
                                        if(warrantyRefreshLayout.isRefreshing())
                                            totalProdct_rv1.stopScroll();
                                }
                            }catch(Exception e) {
                                Log.e(TAG, "Scroll Error : "+e.getLocalizedMessage());
                            }
                        }
                    });*/
                }
            });
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onRequestError(int errorCode, String message) {
            if ((Activity) getContext() == null || message == null || errorCode == 0) {
                return;
            }
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
//                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                if ((Activity) getContext() == null) {
                    return;
                }
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };
    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;

    @SuppressLint("ValidFragment")
    public Target_fragmnet(Context context1) {
        context = context1;
    }

    public Target_fragmnet() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.targets_fragment, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        battery_current_spinner = (Spinner) getView().findViewById(R.id.battery_current_spinner);
        battery_current_spinner1 = (Spinner) getView().findViewById(R.id.battery_current_spinner1);
        battery_warranty_spinner = (Spinner) getView().findViewById(R.id.battery_warranty_spinner);
        battery_warranty_spinner1 = (Spinner) getView().findViewById(R.id.battery_warranty_spinner1);
        purchase_date = (TextView) getView().findViewById(R.id.purchase_date);
        purchase_date1 = (TextView) getView().findViewById(R.id.purchase_date1);
        totalProdct_rv = (RecyclerView) getView().findViewById(R.id.product_list_rv);
        totalProdct_rv1 = (RecyclerView) getView().findViewById(R.id.product_list_rv1);
        clear_all_tv = (TextView) getView().findViewById(R.id.clear_all_tv);
        clear_all_tv1 = (TextView) getView().findViewById(R.id.clear_all_tv1);
        calculate_button = (Button) getView().findViewById(R.id.calculate_button);
        calculate_button1 = (Button) getView().findViewById(R.id.calculate_button1);
        empty_relative_layout = (RelativeLayout) getView().findViewById(R.id.empty_rl);
        calculater_RL = (RelativeLayout) getView().findViewById(R.id.calculater_RL);
        offer_RL = (RelativeLayout) getView().findViewById(R.id.offer_RL);
        pdfdownload = (RelativeLayout) getView().findViewById(R.id.pdfdownload);
        pdfLink = (TextView) getView().findViewById(R.id.pdflink);
        pdflink1 = (TextView) getView().findViewById(R.id.pdflink1);
        pdflink2 = (TextView) getView().findViewById(R.id.pdflink2);
        swipeRefreshLayout = getView().findViewById(R.id.swiperefresh);
        warrantyRefreshLayout = getView().findViewById(R.id.swiperefresh1);
        calculator_tv = (TextView) getView().findViewById(R.id.calculator_tv);
        Purchasecalculator = (TextView) getView().findViewById(R.id.calculator_tv1);
        WarrantyCalculator = (TextView) getView().findViewById(R.id.offer_tv1);
        tab_layout1 = (LinearLayout) getView().findViewById(R.id.tab_layout1);
        offer_tv = (TextView) getView().findViewById(R.id.offer_tv);
        purchase_date.setOnClickListener(purchaseClick);
        purchase_date1.setOnClickListener(warrantyclick);
        scrollView = (ScrollView) getView().findViewById(R.id.scrollView);
        scrollView1 = (ScrollView) getView().findViewById(R.id.scrollView1);
        databaseBackend = DatabaseBackend.getInstance(getContext());
        calculate_button.setEnabled(false);
        calculate_button1.setEnabled(false);
        target_rv = (RecyclerView) getView().findViewById(R.id.target_rv);
        progressBar = (ProgressBar) getView().findViewById(R.id.progressbar);
        pdf_button = (Button) getView().findViewById(R.id.buttonpdf);
        pdf_button1 = (Button) getView().findViewById(R.id.buttonpdf1);
        pdf_button2 = (Button) getView().findViewById(R.id.buttonpdf2);
        //change by mohit
        SpannableString content1 = null;
       /* if (PrefManager.getInstance(getContext()).getUserViewFlag()) {
            content1 = new SpannableString("DOWNLOAD OFFERS FOR SECONDARY");
        } else {*/
        content1 = new SpannableString("DOWNLOAD OFFERS FOR SECONDARY 2.0");
        //}
        content1.setSpan(new UnderlineSpan(), 0, content1.length(), 0);
        pdfLink.setText(content1);

        SpannableString content2 = new SpannableString("DOWNLOAD OFFERS FOR TERTIARY");
        content2.setSpan(new UnderlineSpan(), 0, content2.length(), 0);
        pdflink1.setText(content2);

        SpannableString content3 = new SpannableString("CLICK DOWNLOAD FOR E-RICKSHAW");
        content3.setSpan(new UnderlineSpan(), 0, content3.length(), 0);
        pdflink2.setText(content3);

        targetsList = new ArrayList<TargetsModel>();
        targetsList1 = new ArrayList<TargetsModel>();
        productLoyalityList = new ArrayList<ProductLoyalityModel>();
        productLoyalityList1 = new ArrayList<WarrantyLoyalityModel>();
        battery_Ah_List = new ArrayList<>();
        batterry_Ah_List1 = new ArrayList<>();
        addedProductList = new ArrayList<>();
        addedProductList1 = new ArrayList<>();
        progressBar.setVisibility(View.VISIBLE);
        Controller.GetProductLoyality(getContext(), loyalityListner);
        Controller.GetWarrantyLoyality(getContext(), WarrantybatteryListner);
        //Controller.GetTargets(getContext(),targetsListner);
        if (prefManager == null)
            prefManager = new PrefManager(getContext());

       /* if (prefManager.getUserViewFlag()) {
            Controller.getSchemeDelhi(getContext(), SchemeListner);
        } else {*/
        Controller.getScheme(getContext(), SchemeListner);
        //}
        Controller.gettertiaryScheme(getContext(), TertiaryListener);
        Controller.geterickScheme(getContext(), SchemeListnererickshaw);

        //change by mohit
        SpannableString content = new SpannableString("Clear all");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        clear_all_tv.setText(content);
        SpannableString content4 = new SpannableString("Clear all");
        content4.setSpan(new UnderlineSpan(), 0, content4.length(), 0);
        clear_all_tv1.setText(content4);
        productListForCalculateModelsList = new ArrayList<>();
        productListForCalculateModelsList1 = new ArrayList<>();
        /*
        * by Sanjay we don't want the this for calculation
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                pdf_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Controller.getScheme(getContext(),SchemeListner);
                    }
                });
                pdf_button1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Controller.gettertiaryScheme(getContext(),TertiaryListener);
                    }
                });
                // Controller.GetTargets(getContext(),targetsListner);
            }
        });*/
//        warrantyRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                pdf_button.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Controller.getScheme(getContext(),SchemeListner);
//                    }
//                });
//                pdf_button1.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Controller.gettertiaryScheme(getContext(),TertiaryListener);
//                    }
//                });
//            }
//        });
        targetAdapter = new TargetRecyclerViewAdapter(getContext(), targetsList);
        targetManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        target_rv.setLayoutManager(targetManager);
        target_rv.setAdapter(targetAdapter);
        target_rv.setNestedScrollingEnabled(false);
        target_rv.setHasFixedSize(true);
        final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        final Date date = new Date();
        String todate = dateFormat.format(date);
        String warrantydate = dateFormat.format(date);
        purchase_date.setText(todate);
        purchase_date1.setText(warrantydate);
        str_start_date = MyUtils.getYearlyFormat(purchase_date.getText().toString());
        warrantyList = new ArrayList<>();
        warrantyList1 = new ArrayList<>();
        clear_all_tv.setOnClickListener(clear_all_click);
        clear_all_tv1.setOnClickListener(clearwarranty_all_click);
        calculate_button.setOnClickListener(calculateClick);
        calculate_button1.setOnClickListener(calculatewarrantyclick);
        checkFlagAndSetView();
        calculator_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculater_RL.setVisibility(View.VISIBLE);
                //swipeRefreshLayout.setVisibility(View.VISIBLE);
                offer_RL.setVisibility(View.GONE);
                pdfdownload.setVisibility(View.GONE);
                //tab_layout1.setVisibility(View.VISIBLE);
                //warrantyRefreshLayout.setVisibility(View.GONE);

                Purchasecalculator.setVisibility(View.VISIBLE);
                Purchasecalculator.setBackgroundResource(R.drawable.left_red_rounded_box);
                WarrantyCalculator.setBackgroundResource(R.drawable.righ_white_rounded_box);
                Purchasecalculator.setTextColor(Color.WHITE);
                WarrantyCalculator.setTextColor(Color.RED);
                //  empty_relative_layout.setVisibility(View.GONE);
                calculator_tv.setBackgroundResource(R.drawable.left_red_rounded_box);
                offer_tv.setBackgroundResource(R.drawable.righ_white_rounded_box);
                calculator_tv.setTextColor(Color.WHITE);
                offer_tv.setTextColor(Color.RED);

                checkFlagAndSetView();
            }
        });
        offer_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculater_RL.setVisibility(View.GONE);
                // offer_RL.setVisibility(View.VISIBLE);
                pdfdownload.setVisibility(View.VISIBLE);
                tab_layout1.setVisibility(View.GONE);
                warrantyRefreshLayout.setVisibility(View.GONE);
                //  empty_relative_layout.setVisibility(View.VISIBLE);
                offer_tv.setBackgroundResource(R.drawable.right_red_rounded_box);
                calculator_tv.setBackgroundResource(R.drawable.left_white_rounded_box);
                offer_tv.setTextColor(Color.WHITE);
                calculator_tv.setTextColor(Color.RED);
            }
        });

        //---for purchase calculator and warranty calculator-----\\
        Purchasecalculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warrantyRefreshLayout.setVisibility(View.GONE);
                swipeRefreshLayout.setVisibility(View.VISIBLE);
                Purchasecalculator.setBackgroundResource(R.drawable.left_red_rounded_box);
                WarrantyCalculator.setBackgroundResource(R.drawable.righ_white_rounded_box);
                Purchasecalculator.setTextColor(Color.WHITE);
                WarrantyCalculator.setTextColor(Color.RED);
            }
        });
        WarrantyCalculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipeRefreshLayout.setVisibility(View.GONE);
                warrantyRefreshLayout.setVisibility(View.VISIBLE);
                WarrantyCalculator.setBackgroundResource(R.drawable.right_red_rounded_box);
                Purchasecalculator.setBackgroundResource(R.drawable.left_white_rounded_box);
                WarrantyCalculator.setTextColor(Color.WHITE);
                Purchasecalculator.setTextColor(Color.RED);

            }
        });
        /*
        * By Sanjay
        scrollView.setOnScrollChangeListener( new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                try {
                    int firstPos = targetManager.findFirstCompletelyVisibleItemPosition();
                    if (firstPos > 0) {
                        swipeRefreshLayout.setEnabled(false);
                    } else {
                        swipeRefreshLayout.setEnabled(true);
                        if(target_rv.getScrollState() == 1)
                            if(swipeRefreshLayout.isRefreshing())
                                target_rv.stopScroll();
                    }
                }catch(Exception e) {
                    Log.e(TAG, "Scroll Error : "+e.getLocalizedMessage());
                }
            }
        });
        scrollView1.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                try {
                    int firstPos = targetManager.findFirstCompletelyVisibleItemPosition();
                    if (firstPos > 0) {
                        warrantyRefreshLayout.setEnabled(false);
                    } else {
                        warrantyRefreshLayout.setEnabled(true);
                        if(target_rv.getScrollState() == 1)
                            if(warrantyRefreshLayout.isRefreshing())
                                target_rv.stopScroll();
                    }
                }catch(Exception e) {
                    Log.e(TAG, "Scroll Error : "+e.getLocalizedMessage());
                }
            }
        });*/

        battery_Ah_List.add("Battery Current");
        battery_warranty_spinner.setEnabled(false);
        batterry_Ah_List1.add("Battery Current");
        battery_warranty_spinner1.setEnabled(false);
    }

    /**
     * Code By Sanjay
     * Show the calculator as per the api respomnse
     * in home fragment i got flag based on flag i am going to
     * show and hide the Calculator
     */
    private void checkFlagAndSetView() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (prefManager == null)
                    prefManager = new PrefManager(getContext());

                /*if (prefManager.getUserViewFlag()) {
                    swipeRefreshLayout.setVisibility(View.GONE);
                    warrantyRefreshLayout.setVisibility(View.VISIBLE);
                    tab_layout1.setVisibility(View.GONE);
                } else {
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                    warrantyRefreshLayout.setVisibility(View.GONE);
                    tab_layout1.setVisibility(View.VISIBLE);
                }*/
                swipeRefreshLayout.setVisibility(View.VISIBLE);
                warrantyRefreshLayout.setVisibility(View.GONE);
                tab_layout1.setVisibility(View.VISIBLE);
            }
        });

    }

    private void refreshButton() {
        calculate_button.setText("Calculate");
        calculate_button1.setText("Calculate");
        calculate_button.setBackgroundResource(R.drawable.red_rounded_button_background);
        calculate_button1.setTextColor(Color.WHITE);
        calculate_button1.setBackgroundResource(R.drawable.red_rounded_button_background);
        calculate_button.setTextColor(Color.WHITE);
        calculate_button.setClickable(true);
        calculate_button1.setClickable(true);
    }

    public boolean hasReadWritePermissionGranted() {
        return ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestReadWritePermission() {
        Log.e("permistion", "need permistion");
        if (Build.VERSION.SDK_INT >= 23) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS_READ_WRITE,
                    1002);
        }
    }

    //    RequestListener targetsListner=new RequestListener() {
//        @Override
//        public void onRequestStarted() {
//        }
//        @Override
//        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
//            Log.d("response",responseObject.toString());
//            targetsList.clear();
//            Type collectionType = new TypeToken<List<TargetsModel>>() {}.getType();
//            List<TargetsModel> ca = (List<TargetsModel>) new Gson().fromJson( responseObject.toString(), collectionType);
//            targetsList.addAll(ca);
//            ((Activity)getContext()).runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    swipeRefreshLayout.setRefreshing(false);
//                    if (!targetsList.isEmpty()){
//                        empty_relative_layout.setVisibility(View.GONE);
//                    }
//                    progressBar.setVisibility(View.GONE);
//                    targetAdapter.notifyDataSetChanged();
//                }
//            });
//        }
//        @RequiresApi(api = Build.VERSION_CODES.M)
//        @Override
//        public void onRequestError(int errorCode, String message)  {
//            Log.d("response",message);
//            if (errorCode >= 400 && errorCode < 500) {
//                if (errorCode == 403){
//                    ((Activity)getContext()).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            swipeRefreshLayout.setRefreshing(false);
//                            Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//                else if(errorCode==401) {
//                    ((Activity)getContext()).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            swipeRefreshLayout.setRefreshing(false);
////                            logout();
//                        }
//                    });
//                }
//                else {
//                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
//                    ((Activity)getContext()).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            swipeRefreshLayout.setRefreshing(false);
//                            Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//            } else {
//                ((Activity)getContext()).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        swipeRefreshLayout.setRefreshing(false);
//                        Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        }
//    };
    @Override
    public void onHorizontalNumberPickerChanged(HorizontalNumberPicker horizontalNumberPicker, int id, int value) {
        refreshButton();
        for (int i = 0; i < addedProductList.size(); i++) {
            if (id == addedProductList.get(i).getId()) {
                addedProductList.get(i).setQuantity(value);
                break;
            }
        }

        for (int i = 0; i < addedProductList1.size(); i++) {
            if (id == addedProductList1.get(i).getId()) {
                addedProductList1.get(i).setQuantity(value);
                break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshButton();
        battery_current_spinner.setSelection(0);
        battery_warranty_spinner.setSelection(0);
        battery_current_spinner1.setSelection(0);
        battery_warranty_spinner1.setSelection(0);
    }

    @Override
    public void onItemRemove(boolean result) {
        if (result = true) {
            refreshButton();
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loyaltyPointsProductAdapter.notifyDataSetChanged();
                warrantyPointProductAdpter.notifyDataSetChanged();
            }
        });
    }
}
