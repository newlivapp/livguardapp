package com.tagbin.user.livrewards_loyality.Model;

import java.io.Serializable;

public class ScratchCardModel implements Serializable {
    private String id, image, coupon_text, redeem_status, coupon_scratch_date, current_date, coupon_status;

    public String getCoupon_status() {
        return coupon_status;
    }

    public void setCoupon_status(String coupon_status) {
        this.coupon_status = coupon_status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCoupon_text() {
        return coupon_text;
    }

    public void setCoupon_text(String coupon_text) {
        this.coupon_text = coupon_text;
    }

    public String getRedeem_status() {
        return redeem_status;
    }

    public void setRedeem_status(String redeem_status) {
        this.redeem_status = redeem_status;
    }

    public String getCoupon_scratch_date() {
        return coupon_scratch_date;
    }

    public void setCoupon_scratch_date(String coupon_scratch_date) {
        this.coupon_scratch_date = coupon_scratch_date;
    }

    public String getCurrent_date() {
        return current_date;
    }

    public void setCurrent_date(String current_date) {
        this.current_date = current_date;
    }
}
