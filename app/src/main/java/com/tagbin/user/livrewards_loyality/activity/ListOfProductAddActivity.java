package com.tagbin.user.livrewards_loyality.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tagbin.user.livrewards_loyality.Model.ProductListModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.adapter.ListOfAddedProductAdapter;

import java.util.ArrayList;
import java.util.List;

public class ListOfProductAddActivity extends AppCompatActivity {
    RecyclerView listOfProductRv;

    private List<ProductListModel> productListModelList;
    private ListOfAddedProductAdapter productListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_product_add);

        listOfProductRv = findViewById(R.id.listOfProductRv);

        productListModelList = new ArrayList<>();

        initRecyclerView();

    }

    private void initRecyclerView() {
        listOfProductRv.setLayoutManager(new LinearLayoutManager(ListOfProductAddActivity.this));
        listOfProductRv.setHasFixedSize(true);
        listOfProductRv.setNestedScrollingEnabled(false);
        productListAdapter = new ListOfAddedProductAdapter(this, productListModelList);
        listOfProductRv.setAdapter(productListAdapter);
    }
}
