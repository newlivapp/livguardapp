package com.tagbin.user.livrewards_loyality.Model;

public class CheckVersionAPIModel {
    private String user_id, versioncode;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVersioncode() {
        return versioncode;
    }

    public void setVersioncode(String versioncode) {
        this.versioncode = versioncode;
    }
}
