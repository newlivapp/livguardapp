package com.tagbin.user.livrewards_loyality.helper;


import android.content.Context;
import android.content.SharedPreferences;

import com.tagbin.user.livrewards_loyality.Model.ERickPointsWB;
import com.tagbin.user.livrewards_loyality.Model.LoginModel;
import com.tagbin.user.livrewards_loyality.Model.LoginResponseModel;
import com.tagbin.user.livrewards_loyality.Model.SecondaryPointsDelhi;
import com.tagbin.user.livrewards_loyality.Model.SegmentModel;

/**
 * Created by user on 30-03-2017.
 */

public class PrefManager {
    private static final String PREF_NAME = "LIVEGUARD_APP";
    private static final String LOGIN_MODEL_1 = "login_model_1";
    private static final String LOGIN_MODEL = "login_model";
    private static final String STATUS = "status";
    private static final String KEY_FLAG = "key_flag";
    private static final String KEY_FLAG_WB = "key_flag_wb";
    private static final String KEY_SECONDARY_POINT_DELHI = "key_secondarypointsdelhi";
    private static final String KEY_ERICK_POINT_WB = "key_rrickpointswb";
    private static final String SEGMENT_MODEL = "segment_model";
    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;
    private static Context _context;
    private static PrefManager prefManager;
    int PRIVATE_MODE = 0;

    public PrefManager(Context ctx) {
        _context = ctx;
        pref = ctx.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.apply();
    }

    public static PrefManager getInstance(Context context) {
        if (prefManager == null)
            prefManager = new PrefManager(context);

        return prefManager;
    }

    static void init(Context ctx) {
        _context = ctx;
        pref = ctx.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.apply();
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public void saveLoginModel1(LoginModel locationModel) {
        String userJson = JsonUtils.jsonify(locationModel);
        editor.putString(LOGIN_MODEL_1, userJson);
        editor.apply();
    }

    public void saveLoginModel(LoginResponseModel locationModel) {
        String userJson = JsonUtils.jsonify(locationModel);
        editor.putString(LOGIN_MODEL, userJson);
        editor.apply();
    }

    public LoginResponseModel getLoginModel() {
        String userJson = pref.getString(LOGIN_MODEL, null);
        LoginResponseModel locationModel = JsonUtils.objectify(userJson, LoginResponseModel.class);
        return locationModel;
    }

    public void saveSegmentModel(SegmentModel segmentModel) {
        String userJson = JsonUtils.jsonify(segmentModel);
        editor.putString(SEGMENT_MODEL, userJson);
        editor.apply();
    }

    public SegmentModel getSegmentModel() {
        String userJson = pref.getString(SEGMENT_MODEL, null);
        SegmentModel segmentModel = JsonUtils.objectify(userJson, SegmentModel.class);
        return segmentModel;
    }

    public boolean getForgot() {
        return pref.getBoolean(STATUS, false);
    }

    public void setForgot(boolean status) {
        editor.putBoolean(STATUS, status);
        editor.apply();
    }

    //please change key for delhischeme data to KEY_FLAG
    public boolean getUserViewFlag() {
        return pref.getBoolean(KEY_FLAG, false);
    }

    public void setUserViewFlag(boolean flag) {
        editor.putBoolean(KEY_FLAG, flag);
        editor.apply();
    }

    //please change key for delhischeme data to KEY_FLAG
    public boolean getUserViewFlagWB() {
        return pref.getBoolean(KEY_FLAG_WB, false);
    }

    public void setUserViewFlagWB(boolean flag) {
        editor.putBoolean(KEY_FLAG_WB, flag);
        editor.apply();
    }

    public SecondaryPointsDelhi getSecondaryPointsDelhi() {
        String SecondaryPointsDelhiStr = pref.getString(KEY_SECONDARY_POINT_DELHI, "");

        if (SecondaryPointsDelhiStr.isEmpty())
            return null;

        return JsonUtils.objectify(SecondaryPointsDelhiStr, SecondaryPointsDelhi.class);
    }

    public void setSecondaryPointsDelhi(String date) {
        editor.putString(KEY_SECONDARY_POINT_DELHI, date);
        editor.apply();
    }

    public ERickPointsWB getERickPointsWB() {
        String SecondaryPointsDelhiStr = pref.getString(KEY_ERICK_POINT_WB, "");

        if (SecondaryPointsDelhiStr.isEmpty())
            return null;

        return JsonUtils.objectify(SecondaryPointsDelhiStr, ERickPointsWB.class);
    }

    public void setERickPointsWB(String date) {
        editor.putString(KEY_ERICK_POINT_WB, date);
        editor.apply();
    }

}
