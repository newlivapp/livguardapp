package com.tagbin.user.livrewards_loyality.Interface;

import com.horizontalnumberpicker.HorizontalNumberPicker;

public interface HorizontalNumberPickerListener {
    void onHorizontalNumberPickerChanged(HorizontalNumberPicker horizontalNumberPicker, String id, int value);
}