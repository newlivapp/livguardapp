package com.tagbin.user.livrewards_loyality.Interface;

import org.json.JSONException;

import java.text.ParseException;


public interface RequestListener {

    public void onRequestStarted();

    public void onRequestCompleted(Object responseObject) throws JSONException, ParseException;

    public void onRequestError(final int errorCode, final String message);
}
