package com.tagbin.user.livrewards_loyality.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.GetRedeemHistoryModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.adapter.RedeemHistoryRVAdapter;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class RedeemHistoryActivity extends AppCompatActivity {
    Toolbar toolbar;
    ImageView back_button;
    RecyclerView redeemhistory_rv;
    RedeemHistoryRVAdapter redeemHistoryRVAdapter;
    List<GetRedeemHistoryModel> redeemhistoryList;
    //    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefreshLayout;
    RelativeLayout emplty_rl;
    RequestListener redeemListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response GetRedeemHistory", responseObject.toString());
            redeemhistoryList.clear();
            Type collection = new TypeToken<List<GetRedeemHistoryModel>>() {
            }.getType();
            List<GetRedeemHistoryModel> grhm = (List<GetRedeemHistoryModel>) new Gson().fromJson(responseObject.toString(), collection);
            redeemhistoryList.addAll(grhm);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (redeemhistoryList.isEmpty()) {
                        emplty_rl.setVisibility(View.VISIBLE);
                        redeemhistory_rv.setVisibility(View.GONE);
                    } else {
                        emplty_rl.setVisibility(View.GONE);
                        redeemhistory_rv.setVisibility(View.VISIBLE);
                    }
                    redeemHistoryRVAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(RedeemHistoryActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(false);
//                                logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(false);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(RedeemHistoryActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RedeemHistoryActivity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(RedeemHistoryActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_history);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        redeemhistory_rv = (RecyclerView) findViewById(R.id.redeemhistory_rv);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        emplty_rl = (RelativeLayout) findViewById(R.id.empty_rl);
        back_button.setOnClickListener(backClick);
//        progressBar= (ProgressBar) findViewById(R.id.progressbar);
        redeemhistoryList = new ArrayList<GetRedeemHistoryModel>();
//        progressBar.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Controller.GetRedeemHistory(RedeemHistoryActivity.this, redeemListner);
            }
        });

        Controller.GetRedeemHistory(RedeemHistoryActivity.this, redeemListner);

        redeemHistoryRVAdapter = new RedeemHistoryRVAdapter(RedeemHistoryActivity.this, redeemhistoryList);
        redeemhistory_rv.setLayoutManager(new LinearLayoutManager(RedeemHistoryActivity.this, LinearLayoutManager.VERTICAL, false));
        redeemhistory_rv.setAdapter(redeemHistoryRVAdapter);
        redeemhistory_rv.setHasFixedSize(true);
    }
}
