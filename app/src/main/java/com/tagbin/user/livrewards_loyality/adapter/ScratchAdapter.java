package com.tagbin.user.livrewards_loyality.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tagbin.user.livrewards_loyality.Model.ScratchCardModel;
import com.tagbin.user.livrewards_loyality.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 02-04-2017.
 */

public class ScratchAdapter extends RecyclerView.Adapter<ScratchAdapter.MyViewHolder> {
    private final int VIEW_TYPE_ITEM_SCRATCH = 0;
    private final int VIEW_TYPE_ITEM_UN_SCRATCH = 1;
    Context context;
    List<ScratchCardModel> productList;
    SimpleDateFormat simpleDateFormat, simpleDateFormatForDisplay, dateOnlySimpleDateFormat;

    public ScratchAdapter(Context context1, List<ScratchCardModel> productList1) {
        context = context1;
        productList = productList1;
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        simpleDateFormatForDisplay = new SimpleDateFormat("dd-MM-yyyy");
        dateOnlySimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }

    @Override
    public ScratchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;
        if (viewType == VIEW_TYPE_ITEM_SCRATCH) {
            v = inflater.inflate(R.layout.row_scratch_item, parent, false);
        } else {
            v = inflater.inflate(R.layout.row_un_scratch_item, parent, false);
        }

        MyViewHolder mvh = new MyViewHolder(v, viewType);
        return mvh;
    }

    @Override
    public int getItemViewType(int position) {
        if (productList.get(position).getRedeem_status().equalsIgnoreCase("scratched")) {
            return VIEW_TYPE_ITEM_SCRATCH;
        } else {
            return VIEW_TYPE_ITEM_UN_SCRATCH;
        }
    }

    @Override
    public void onBindViewHolder(final ScratchAdapter.MyViewHolder holder, int position) {
        ScratchCardModel scratchCardModel = productList.get(position);
        if (scratchCardModel.getRedeem_status().equalsIgnoreCase("scratched")) {
            holder.tvScratchCards.setText(scratchCardModel.getCoupon_text());
            if (scratchCardModel.getCoupon_status() != null && !scratchCardModel.getCoupon_status().isEmpty()) {
                holder.tvSettled.setVisibility(View.VISIBLE);
                holder.tvSettled.setText(scratchCardModel.getCoupon_status());

                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(500); //You can manage the blinking time with this parameter
                anim.setStartOffset(20);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                holder.tvSettled.startAnimation(anim);
            } else {
                holder.tvSettled.setVisibility(View.GONE);
            }
        } else {
            holder.ivScratchCards.setImageDrawable(context.getResources().getDrawable(R.drawable.coupon_img));
//            String dateStr = scratchCardModel.getCoupon_scratch_date();
//            try {
//                //Date scratchDate = simpleDateFormat.parse(dateStr);
//                //Date currentDate = simpleDateFormat.parse(scratchCardModel.getCurrent_date());
//                Date scratchDate = dateOnlySimpleDateFormat.parse(dateOnlySimpleDateFormat.format(simpleDateFormat.parse(dateStr)));
//                Date currentDate = dateOnlySimpleDateFormat.parse(dateOnlySimpleDateFormat.format(simpleDateFormat.parse(scratchCardModel.getCurrent_date())));
//                if (currentDate.getTime() < scratchDate.getTime()) {
//                    holder.llDateParent.setVisibility(View.VISIBLE);
//                } else {
//                    holder.llDateParent.setVisibility(View.GONE);
//                }
//
//                dateStr = simpleDateFormatForDisplay.format(scratchDate);
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            holder.tvScratchCardsDate.setText(dateStr);
        }
    }

    @Override
    public int getItemCount() {
        return productList == null ? 0 : productList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvScratchCards, tvScratchCardsDate, tvSettled;
        ImageView ivScratchCards;
        LinearLayout llDateParent;

        public MyViewHolder(View itemView, int viewType) {
            super(itemView);
            if (viewType == VIEW_TYPE_ITEM_SCRATCH) {
                tvScratchCards = (TextView) itemView.findViewById(R.id.tvScratchCards);
                tvSettled = (TextView) itemView.findViewById(R.id.tvSettled);
            } else {
                ivScratchCards = (ImageView) itemView.findViewById(R.id.ivScratchCards);
//                tvScratchCardsDate = (TextView) itemView.findViewById(R.id.tvScratchCardsDate);
                llDateParent = (LinearLayout) itemView.findViewById(R.id.llDateParent);
            }


        }
    }
}
