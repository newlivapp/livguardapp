package com.tagbin.user.livrewards_loyality.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tagbin.user.livrewards_loyality.Model.RedeemSlabModel;
import com.tagbin.user.livrewards_loyality.R;

import java.util.List;

/**
 * Created by user on 02-04-2017.
 */

public class CouponAdapter extends RecyclerView.Adapter<CouponAdapter.MyViewHolder> {
    Context context;
    List<RedeemSlabModel> productList;
    int is_e_rick = 0;

    String[] details = {"Hyundai Santro D Lite or Switzerland (5D/4N) - 2 PAX + 30 Gm Gold",
            "Alto K10 LX or Switzerland (5D/4N) - 2 PAX + 20 Gm Gold",
            "Renault Kwid RXE or Switzerland (5D/4N) - 2 PAX + 10 Gm Gold",
            "Tanishq Voucher Worth Rs 3 Lacs or Switzerland (5D/4N) - 2 PAX",
            "Datsun Redi Go D or Italy (5D/4N) - 2 PAX + 5 Gm Gold",
            "Tanishq Voucher Worth Rs 2 Lacs + Bajaj CT 100 or Italy (5D/4N) - 2 PAX",
            "Tanishq Voucher Worth 2 Lacs + 3 Units LG1100 or Hong Kong Macau(6D/5N) - 2 PAX + 5 Gm Gold",
            "Royal Enfield Bullet 350 + Honda Activa 5G or Hong Kong Macau(6D/5N) - 2 PAX",
            "Royal Enfield Classic 350 + 2 Units LG1100 or Singapore 4D/3N- 2PAX",
            "Royal Enfield Bullet 350 + 3 Units LG1100 or Dubai (4D/3N) - 2 PAX",
            "Tanishq Voucher worth Rs 1.1 Lacs or Malaysia + Genting (4D/3N) - 2 PAX",
            "Bajaj Avenger Street 160 + 2 Units LG1100 or Hong Kong Macau(6D/5N) - 1 PAX",
            "Honda Activa 125 or Dubai (4D/3N) - 1 PAX",
            "Hero Splendor Plus or Malaysia + Genting (4D/3N) - 1 PAX",
            "Bajaj CT 100 / Voltas 1.5 Tonne Split AC 3 Star or Thailand(4D/3N) - 1 PAX ",
            "5 Gm Gold + LS1700 or Goa(3D/2N) - 1 PAX",
            "3 Gm Gold + LGS1100",
            "Livguard LG1700 + LGS1100 Inverter",
            "Livguard LGS1100 Inverter",
            "Livguard LG700 Inverter",
            "Rs 1100 Voucher"};

    String[] detailsERick = {"Dubai (1Pax 4D3N) + Royal Enfield Thunderbird 500 + 30 gm Gold Coin",
            "Dubai (1Pax 4D3N) + Royal Enfield Bullet 350 + 10 gm Gold Coin",
            "Dubai (1Pax 4D3N) + Honda X Blade",
            "Dubai (1Pax 4D3N)",
            "10 gm Gold Coin"};

    public CouponAdapter(Context context1, List<RedeemSlabModel> productList1) {
        context = context1;
        productList = productList1;
    }

    public CouponAdapter(Context context1, List<RedeemSlabModel> productList1, int e_rick) {
        context = context1;
        productList = productList1;
        is_e_rick = e_rick;
    }

    @Override
    public CouponAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_coupon_item, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final CouponAdapter.MyViewHolder holder, int position) {
        int loyalty_points = productList.get(position).getLoyalty_point();
        holder.tvLoyalityPoints.setText(String.valueOf(loyalty_points));
        //holder.tvDescription.setText(productList.get(position).getRedeem_desc());
        if (is_e_rick == 0) {
            holder.tvDescription.setVisibility(View.GONE);
        } else {
            holder.tvDescription.setVisibility(View.VISIBLE);
        }
        if (productList.get(position).isSelected()) {
            holder.ivSelector.setVisibility(View.VISIBLE);
        } else {
            holder.ivSelector.setVisibility(View.INVISIBLE);
        }
        if (is_e_rick == 1) {
            if (productList.get(position).getRedeem_desc() != null && !productList.get(position).getRedeem_desc().isEmpty()) {
                holder.tvDescription.setText(productList.get(position).getRedeem_desc());
            }
            /*if (productList.size() == detailsERick.length) {
                holder.tvDescription.setText(detailsERick[position]);
            }*/
           /* if (loyalty_points == 600) {
                holder.tvDescription.setText("10 gm Gold Coin");
            } else if (loyalty_points == 1200) {
                holder.tvDescription.setText("Dubai (1Pax 4D3N)");
            } else if (loyalty_points == 2400) {
                holder.tvDescription.setText("Dubai (1Pax 4D3N) + Honda X Blade");
            } else if (loyalty_points == 3600) {
                holder.tvDescription.setText("Dubai (1Pax 4D3N) + Royal Enfield Bullet 350 + 10 gm Gold Coin");
            } else if (loyalty_points == 4800) {
                holder.tvDescription.setText("Dubai (1Pax 4D3N) + Royal Enfield Thunderbird 500 + 30 gm Gold Coin");
            }*/
        } else if (is_e_rick == 2) {
            if (productList.size() == details.length) {
                holder.tvDescription.setText(details[position]);
            }
            /*if (loyalty_points == 1500) {
                holder.tvDescription.setText(" LG-Sq-700");
            } else if (loyalty_points == 2500) {
                holder.tvDescription.setText("LG-sine1100");
            } else if (loyalty_points == 5000) {
                holder.tvDescription.setText("LG-VFP1536");
            } else if (loyalty_points == 7500) {
                holder.tvDescription.setText("Redmi Note7");
            } else if (loyalty_points == 10000) {
                holder.tvDescription.setText("Samsung 24inch +i2-Verter 700");
            } else if (loyalty_points == 12500) {
                holder.tvDescription.setText("Oppo-F9-Pro");
            } else if (loyalty_points == 15000) {
                holder.tvDescription.setText("Samsung 32inch TV");
            } else if (loyalty_points == 20000) {
                holder.tvDescription.setText("Lenovo Idepad or Thailand(3n/4d)");
            } else if (loyalty_points == 25000) {
                holder.tvDescription.setText("Thailand(3n/4d)+VFP1536");
            } else if (loyalty_points == 30000) {
                holder.tvDescription.setText("Dubai(3n/4d)");
            } else if (loyalty_points == 35000) {
                holder.tvDescription.setText("Activa+IT1560STT");
            } else if (loyalty_points == 40000) {
                holder.tvDescription.setText("Singapore(4n/5d)");
            } else if (loyalty_points == 45000) {
                holder.tvDescription.setText("Avenger 180 or Activa+Thailand(3n/4d)");
            } else if (loyalty_points == 50000) {
                holder.tvDescription.setText("Samsung 55inch TV");
            } else if (loyalty_points == 55000) {
                holder.tvDescription.setText("RoyalEnfield Bullet 350");
            } else if (loyalty_points == 65000) {
                holder.tvDescription.setText("Singapore(4n/5d)+Activa+IT1560STT");
            } else if (loyalty_points == 75000) {
                holder.tvDescription.setText("Bullet 350+ Activa");
            } else if (loyalty_points == 85000) {
                holder.tvDescription.setText("Bullet 500+Thailand(3n/4d)");
            } else if (loyalty_points == 100000) {
                holder.tvDescription.setText("Bullet 500+Iphone Xs-64GB");
            } else if (loyalty_points == 120000) {
                holder.tvDescription.setText("Renault QWID - RXE");
            }*/
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvLoyalityPoints, tvDescription;
        ImageView ivSelector;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivSelector = itemView.findViewById(R.id.ivSelector);
            tvLoyalityPoints = itemView.findViewById(R.id.tvLoyalityPoints);
            tvDescription = itemView.findViewById(R.id.tvDescription);
        }
    }
}
