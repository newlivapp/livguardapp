package com.tagbin.user.livrewards_loyality.Model;

public class APIErrorResponseModel {
    public String Status, msg;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
