package com.tagbin.user.livrewards_loyality.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.DataModel;
import com.tagbin.user.livrewards_loyality.Model.EditDetaiModel;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.ImageurlModel;
import com.tagbin.user.livrewards_loyality.Model.LoginResponseModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.fragment.PicModeSelectDialogFragment;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.MyUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;
import com.tagbin.user.livrewards_loyality.helper.ProfileImage.GOTOConstants;
import com.tagbin.user.livrewards_loyality.helper.ProfileImage.ImageCropActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;

import de.hdodenhof.circleimageview.CircleImageView;

public class EnterDetailsActivity extends AppCompatActivity {
    private static int SELECT_PICTURE = 1;
    private static String[] PERMISSIONS_LOCATION = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    final int REQUEST_LOCATION = 2;
    Button button_save;
    EditText customer_aadhar_no, customer_address, customer_city, customer_pincode, customer_gst_number;
    ProgressBar progressBar;
    CircleImageView profile_pic;
    ImageView edit_profile_pic, back_button;
    Toolbar toolbar;
    String imageUrl;
    PrefManager prefManager;
    RequestListener conversion = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(final Object responseObject) {
            Log.d("response Conversion", responseObject.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    imageUrl = responseObject.toString();
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    };
    RequestListener kycDetail = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(final Object responseObject) {
            Log.d("response Conversion", responseObject.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(responseObject.toString());
                        if (obj.has("err")) {
                            if (obj.getString("err").equalsIgnoreCase("Saved")) {
                                finish();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    };
    RequestListener mUploadListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) {
            Log.d("response profileUpload", responseObject.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });
            LoginResponseModel responsemodel = JsonUtils.objectify(responseObject.toString(), LoginResponseModel.class);
            responsemodel.setToken(prefManager.getLoginModel().getToken());
            PrefManager pref = new PrefManager(EnterDetailsActivity.this);
            pref.saveLoginModel(responsemodel);
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(EnterDetailsActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(EnterDetailsActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(EnterDetailsActivity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(EnterDetailsActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        }
    };
    RequestListener editProfilewithotpListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response Profile_Update", responseObject.toString());
            DataModel profileModel = JsonUtils.objectify(responseObject.toString(), DataModel.class);
            PrefManager pref = new PrefManager(EnterDetailsActivity.this);
            LoginResponseModel obj1 = pref.getLoginModel();
            obj1.setData(profileModel);
            pref.saveLoginModel(obj1);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(EnterDetailsActivity.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(EnterDetailsActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(EnterDetailsActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(EnterDetailsActivity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(EnterDetailsActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };
    /*RequestListener editProfileListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response Profile_Update", responseObject.toString());
            if (prefManager.getLoginModel() != null) {
                DataModel profileModel = JsonUtils.objectify(responseObject.toString(), DataModel.class);
                PrefManager pref = new PrefManager(EnterDetailsActivity.this);
                LoginResponseModel obj1 = pref.getLoginModel();
                obj1.setData(profileModel);
                pref.saveLoginModel(obj1);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(EnterDetailsActivity.this, "your profile is updated Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(EnterDetailsActivity.this, "OTP is send successfully", Toast.LENGTH_SHORT).show();
                        final Dialog dialog = new Dialog(EnterDetailsActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.otp_massage_popup);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        final TextView msgText = (TextView) dialog.findViewById(R.id.msg_text);
                        final ImageView dismiss = (ImageView) dialog.findViewById(R.id.dissmiss);
                        final TextView phone_no_msg = (TextView) dialog.findViewById(R.id.phone_no_massage);
                        final EditText otp_text = (EditText) dialog.findViewById(R.id.otp_text);
                        final TextView resendOtp_tv = (TextView) dialog.findViewById(R.id.resend_otp_tv);
                        SpannableString content = new SpannableString("RE-Send OTP");
                        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                        resendOtp_tv.setText(content);
                        resendOtp_tv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                EditProfileModel editProfileModel = new EditProfileModel();
                                editProfileModel.setPhone(customer_address.getText().toString());
                                Controller.profileDetailUpadate(EnterDetailsActivity.this, editProfileModel, new RequestListener() {
                                    @Override
                                    public void onRequestStarted() {

                                    }

                                    @Override
                                    public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
                                        Log.d("response Profile_Update", responseObject.toString());
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(EnterDetailsActivity.this, "OTP sent successfully", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onRequestError(int errorCode, String message) {
                                        Log.d("response", message);
                                        if (errorCode >= 400 && errorCode < 500) {
                                            if (errorCode == 403) {
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        progressBar.setVisibility(View.GONE);
                                                        Toast.makeText(EnterDetailsActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                            } else if (errorCode == 401) {

                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        progressBar.setVisibility(View.GONE);
//                                           logout();
                                                    }
                                                });
                                            } else {
                                                final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        progressBar.setVisibility(View.GONE);
                                                        if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                                            Toast.makeText(EnterDetailsActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Toast.makeText(EnterDetailsActivity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            }
                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    progressBar.setVisibility(View.GONE);
                                                    Toast.makeText(EnterDetailsActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                        phone_no_msg.setText("Enter OTP send to +91" + prefManager.getLoginModel().getData().getPhone());
                        msgText.setText("Once you confirm the OTP, we will update" + "\n" + "your new no. to your account");
                        final Button verify = (Button) dialog.findViewById(R.id.verify_button);
                        verify.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                EditProfileModel editProfileModel = new EditProfileModel();
                                editProfileModel.setFirst_name(customer_aadhar_no.getText().toString());
                                editProfileModel.setEmail(customer_city.getText().toString());
//                        int phone_no = Integer.parseInt(customer_address.getText().toString());
                                editProfileModel.setPhone(customer_address.getText().toString());
                                editProfileModel.setVerification_otp(otp_text.getText().toString());
                                Controller.profileDetailUpadate(EnterDetailsActivity.this, editProfileModel, editProfilewithotpListner);
                            }
                        });
                        dismiss.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        if (EnterDetailsActivity.this != null)
                            EnterDetailsActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!(EnterDetailsActivity.this).isFinishing()) {
                                        try {
                                            dialog.show();
                                            dialog.setCancelable(false);
                                        } catch (WindowManager.BadTokenException e) {
                                            Log.e("WindowManagerBad ", e.toString());
                                        } catch (Exception e) {
                                            Log.e("Exception ", e.toString());
                                        }
                                    }
                                }
                            });
                    }
                });


            }

        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(EnterDetailsActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (errorResponseModel == null) {
                                return;
                            }
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(EnterDetailsActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(EnterDetailsActivity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(EnterDetailsActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };*/
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    public static String getFileToByte(String filePath) {
        Bitmap bmp = null;
        ByteArrayOutputStream bos = null;
        byte[] bt = null;
        String encodeString = null;
        try {
            bmp = BitmapFactory.decodeFile(filePath);
            bos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bt = bos.toByteArray();
            encodeString = Base64.encodeToString(bt, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encodeString;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_details);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        button_save = (Button) findViewById(R.id.save_button);
        customer_aadhar_no = (EditText) findViewById(R.id.customer_aadhar_no);
        customer_address = (EditText) findViewById(R.id.customer_address);
        customer_city = (EditText) findViewById(R.id.customer_city);
        customer_gst_number = (EditText) findViewById(R.id.customer_gst_number);
        customer_pincode = (EditText) findViewById(R.id.customer_pincode);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        profile_pic = (CircleImageView) findViewById(R.id.ivProfilePicmain);
        edit_profile_pic = (ImageView) findViewById(R.id.ivProfileChange);

        prefManager = new PrefManager(EnterDetailsActivity.this);
        edit_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasLocationPermissionGranted())
                    showAddProfilePicDialog1();
                else
                    requestLocationPermission();
            }
        });
        /*if (prefManager.getLoginModel() != null) {
            phone_no = String.valueOf(prefManager.getLoginModel().getData().getPhone());
            customer_aadhar_no.setText(prefManager.getLoginModel().getData().getFirst_name() + " " + prefManager.getLoginModel().getData().getLast_name());
            customer_aadhar_no.setSelection(customer_aadhar_no.getText().length());
            customer_address.setText("" + prefManager.getLoginModel().getData().getPhone());
            customer_address.setSelection(customer_address.getText().length());
            customer_city.setText(prefManager.getLoginModel().getData().getEmail());
            customer_city.setSelection(customer_city.getText().length());
            ImageLoader.getInstance().loadImage(prefManager.getLoginModel().getData().getLarge_image(), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    profile_pic.setImageBitmap(loadedImage);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });

        }*/
        back_button.setOnClickListener(backClick);
        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (customer_aadhar_no == null || customer_aadhar_no.getText().toString().trim().isEmpty()) {
                    MyUtils.showToast(EnterDetailsActivity.this, "Please Enter Aadhar Number");
                    customer_aadhar_no.requestFocus();
                    return;
                }
                if (customer_aadhar_no.getText().toString().trim().length() != 12) {
                    MyUtils.showToast(EnterDetailsActivity.this, "Please Enter Valid Aadhar Number");
                    customer_aadhar_no.requestFocus();
                    return;
                }*/
                if (customer_address == null || customer_address.getText().toString().trim().isEmpty()) {
                    MyUtils.showToast(EnterDetailsActivity.this, "Please Enter Address");
                    customer_address.requestFocus();
                    return;
                }
                if (customer_city == null || customer_city.getText().toString().trim().isEmpty()) {
                    MyUtils.showToast(EnterDetailsActivity.this, "Please Enter City");
                    customer_city.requestFocus();
                    return;
                }
                if (customer_pincode == null || customer_pincode.getText().toString().trim().isEmpty()) {
                    MyUtils.showToast(EnterDetailsActivity.this, "Please Enter Pincode");
                    customer_pincode.requestFocus();
                    return;
                }
                if (customer_pincode.getText().toString().trim().length() != 6) {
                    MyUtils.showToast(EnterDetailsActivity.this, "Please Enter Valid Pincode");
                    customer_pincode.requestFocus();
                    return;
                }
                if (imageUrl == null || imageUrl.isEmpty() || imageUrl.equalsIgnoreCase("")) {
                    MyUtils.showToast(EnterDetailsActivity.this, "Please Select Image");
                    return;
                }

                /*if (prefManager.getLoginModel() != null && phone_no.equals(customer_address.getText().toString())) {
                    EditProfileModel editProfileModel = new EditProfileModel();
                    editProfileModel.setFirst_name(customer_aadhar_no.getText().toString());
                    editProfileModel.setEmail(customer_city.getText().toString());
                    Controller.profileDetailUpadate(EnterDetailsActivity.this, editProfileModel, editProfileListner);
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    EditProfileModel editProfileModel = new EditProfileModel();
                    editProfileModel.setFirst_name(customer_aadhar_no.getText().toString());
                    editProfileModel.setEmail(customer_city.getText().toString());
                    editProfileModel.setPhone(customer_address.getText().toString());
                    Controller.profileDetailUpadate(EnterDetailsActivity.this, editProfileModel, editProfileListner);
                }*/

                EditDetaiModel editDetaiModel = new EditDetaiModel();
                editDetaiModel.setUser_id(prefManager.getLoginModel().getData().getId() + "");
                String customerdhaarNo = "";
                if (customer_aadhar_no.getText() != null && !customer_aadhar_no.getText().toString().isEmpty()) {
                    customerdhaarNo = customer_aadhar_no.getText().toString().trim();
                }
                editDetaiModel.setAdhaar_no(customerdhaarNo);
                editDetaiModel.setAddress(customer_address.getText().toString().trim());
                editDetaiModel.setCity(customer_city.getText().toString().trim());
                editDetaiModel.setPincode(customer_pincode.getText().toString().trim());
                editDetaiModel.setProfile_pic(imageUrl);
                String customerGstNumber = "";
                if (customer_gst_number.getText() != null && !customer_gst_number.getText().toString().isEmpty()) {
                    customerGstNumber = customer_gst_number.getText().toString().trim();
                }
                editDetaiModel.setGst_no(customerGstNumber);
                Controller.KYCDetails(EnterDetailsActivity.this, editDetaiModel, kycDetail);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                String imagePath = data.getStringExtra(GOTOConstants.IntentExtras.IMAGE_PATH);
                String imgBase64Path = getFileToByte(imagePath);
                ImageurlModel imageurlModel = new ImageurlModel();
                imageurlModel.setUser_id(prefManager.getLoginModel().getData().getId() + "");
                imageurlModel.setBase(imgBase64Path);
                progressBar.setVisibility(View.VISIBLE);
                profile_pic.setImageBitmap(showCroppedImage(imagePath));
                imageUrl = "";
                Controller.Conversion(EnterDetailsActivity.this, imageurlModel, conversion);

                /*FileUploadModel fileUploadModel = new FileUploadModel();
                fileUploadModel.setFile(new File(imagePath));
                Controller.profileUpload(EnterDetailsActivity.this, fileUploadModel, mUploadListener);
                progressBar.setVisibility(View.VISIBLE);
                profile_pic.setImageBitmap(showCroppedImage(imagePath));*/
            } else if (resultCode == RESULT_CANCELED) {
                //TODO : Handle case
            } else {
                String errorMsg = data.getStringExtra(ImageCropActivity.ERROR_MSG);
                Toast.makeText(EnterDetailsActivity.this, errorMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

    private Bitmap showCroppedImage(String imagePath) {
        if (imagePath != null) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
            return myBitmap;

        }
        return null;
    }

    public boolean hasLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(EnterDetailsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(EnterDetailsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(EnterDetailsActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void showAddProfilePicDialog1() {
        PicModeSelectDialogFragment dialogFragment = new PicModeSelectDialogFragment();
        dialogFragment.setiPicModeSelectListener(new PicModeSelectDialogFragment.IPicModeSelectListener() {
            @Override
            public void onPicModeSelected(String mode) {
                String action = mode.equalsIgnoreCase(GOTOConstants.PicModes.CAMERA) ? GOTOConstants.IntentExtras.ACTION_CAMERA : GOTOConstants.IntentExtras.ACTION_GALLERY;
                Intent intent = new Intent(EnterDetailsActivity.this, ImageCropActivity.class);
                intent.putExtra("ACTION", action);
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });
        dialogFragment.show(getFragmentManager(), "picModeSelector");
    }

    public void requestLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            ActivityCompat.requestPermissions(EnterDetailsActivity.this, PERMISSIONS_LOCATION,
                    REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //Storage permission is enabled
                        showAddProfilePicDialog1();

                    } else if (ActivityCompat.shouldShowRequestPermissionRationale(EnterDetailsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(EnterDetailsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(EnterDetailsActivity.this, Manifest.permission.CAMERA)) {
                        //User has deny from permission dialog
                        final AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(EnterDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setTitle("Reading Permission Denied");
                        alertDialog1.setMessage("Are you sure you want to deny this permission?");
                        alertDialog1.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.setNegativeButton("RETRY", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.show();
                    } else {
                        // User has deny permission and checked never show permission dialog so you can redirect to Application settings page
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(EnterDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setMessage("It looks like you have turned off permission required for this feature. It can be enabled under Phone Settings > Apps > Partner > Permissions");
                        alertDialog1.setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        });
                        alertDialog1.show();
                    }
                }
                break;
        }
    }

    private void logout() {
        PrefManager pref = new PrefManager(this);
        pref.clearSession();
        Intent go = new Intent(this, LoginActivity.class);
        startActivity(go);
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();
        finish();*/
    }
}
