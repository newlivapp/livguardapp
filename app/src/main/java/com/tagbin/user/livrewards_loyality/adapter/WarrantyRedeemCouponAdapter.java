package com.tagbin.user.livrewards_loyality.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tagbin.user.livrewards_loyality.Model.warrantyRedeemModel;
import com.tagbin.user.livrewards_loyality.R;

import java.util.List;

/**
 * Created by user on 02-04-2017.
 */

public class WarrantyRedeemCouponAdapter extends RecyclerView.Adapter<WarrantyRedeemCouponAdapter.MyViewHolder> {
    Context context;
    List<warrantyRedeemModel> warrantyRedeemModelList;

    public WarrantyRedeemCouponAdapter(Context context1, List<warrantyRedeemModel> warrantyRedeemModelList1) {
        context = context1;
        warrantyRedeemModelList = warrantyRedeemModelList1;
    }

    @Override
    public WarrantyRedeemCouponAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_coupon_item, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final WarrantyRedeemCouponAdapter.MyViewHolder holder, int position) {
        holder.tvDescription.setVisibility(View.VISIBLE);
        holder.tvLoyalityPoints.setText(String.valueOf(warrantyRedeemModelList.get(position).getLoyalty_point()));
        holder.tvDescription.setText("Cash Voucher");
        if (warrantyRedeemModelList.get(position).isSelected()) {
            holder.ivSelector.setVisibility(View.VISIBLE);
        } else {
            holder.ivSelector.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return warrantyRedeemModelList == null ? 0 : warrantyRedeemModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvLoyalityPoints, tvDescription;
        ImageView ivSelector;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivSelector = itemView.findViewById(R.id.ivSelector);
            tvLoyalityPoints = itemView.findViewById(R.id.tvLoyalityPoints);
            tvDescription = itemView.findViewById(R.id.tvDescription);
        }
    }
}
