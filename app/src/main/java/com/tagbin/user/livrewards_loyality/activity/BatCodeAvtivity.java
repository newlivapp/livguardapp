package com.tagbin.user.livrewards_loyality.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.BattCodeResponseModel;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.LoginModel;
import com.tagbin.user.livrewards_loyality.Model.LoginResponseModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;

import org.json.JSONException;

import java.text.ParseException;

public class BatCodeAvtivity extends AppCompatActivity {
    TextView bat_code_msg, main_heading;
    Button confirm_button;
    EditText batCode;
    String strBatCode;
    boolean flag = false;
    ProgressBar progressBar;
    PrefManager pref;
    RequestListener firstTimeListner = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response login", responseObject.toString());
            LoginResponseModel loginResponceModel = JsonUtils.objectify(responseObject.toString(), LoginResponseModel.class);
            pref.saveLoginModel(loginResponceModel);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    Intent intent = new Intent(BatCodeAvtivity.this, Change_password_activity.class);
                    intent.putExtra("firstTime", "firstTimeUser");
                    startActivity(intent);
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("respose", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(BatCodeAvtivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(BatCodeAvtivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(BatCodeAvtivity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(BatCodeAvtivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };
    RequestListener batCodeListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response ForgotPassword", responseObject.toString());
            BattCodeResponseModel battCodeResponseModel = JsonUtils.objectify(responseObject.toString(), BattCodeResponseModel.class);
            Intent intent = new Intent(BatCodeAvtivity.this, Change_password_activity.class);
            intent.putExtra("batcode", "batcode_activity");
            intent.putExtra("username", strBatCode);
            intent.putExtra("phone", String.valueOf(battCodeResponseModel.getPhone_no()));
            startActivity(intent);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    finish();
                    progressBar.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("respose", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(BatCodeAvtivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(BatCodeAvtivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(BatCodeAvtivity.this, "Something went wrong.please try again later!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(BatCodeAvtivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bat_code_avtivity);
        bat_code_msg = (TextView) findViewById(R.id.bat_code_msg);
        confirm_button = (Button) findViewById(R.id.confirm_button);
        main_heading = (TextView) findViewById(R.id.main_heading);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        batCode = (EditText) findViewById(R.id.bat_code);
        pref = new PrefManager(BatCodeAvtivity.this);
        if (getIntent().getStringExtra("first_time") != null && getIntent().getStringExtra("first_time").equals("user")) {
            bat_code_msg.setText("Enter your BATT Code to get started");
            main_heading.setText("Welcome Partner");
        } else {
            bat_code_msg.setText("Enter the BATT Code associated with" + "\n" + "your account & we'll send you an OTP on" + "\n" + "your registered no. to reset your" + "\n" + "password.");
            main_heading.setText("Forgot Password");
        }
        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strBatCode = batCode.getText().toString().trim();
                if (strBatCode.equals("")) {
                    batCode.setError("Enter Batt Code");
                } else {
                    LoginModel loginModel = null;
                    if (flag == false) {
                        loginModel = new LoginModel();
                        loginModel.setBat_code(strBatCode);
                        if (getIntent().getStringExtra("first_time") != null && getIntent().getStringExtra("first_time").equals("user")) {
                            Log.d("BATCode", strBatCode);
                            loginModel.setJob_type("registration");
                            progressBar.setVisibility(View.VISIBLE);
                            Controller.manualLogin(BatCodeAvtivity.this, loginModel, firstTimeListner);
                        } else {
                            PrefManager pref = new PrefManager(BatCodeAvtivity.this);
                            pref.saveLoginModel1(loginModel);
                            progressBar.setVisibility(View.VISIBLE);
                            Controller.BatCode(BatCodeAvtivity.this, loginModel, batCodeListner);
                        }
                    }
                }
            }
        });
    }
}
