package com.tagbin.user.livrewards_loyality.Model;

public class ERickPointsWB {
    String count, loyalty_points;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getLoyalty_points() {
        return loyalty_points;
    }

    public void setLoyalty_points(String loyalty_points) {
        this.loyalty_points = loyalty_points;
    }
}
