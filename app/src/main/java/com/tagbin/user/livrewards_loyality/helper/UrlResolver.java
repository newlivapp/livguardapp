package com.tagbin.user.livrewards_loyality.helper;

import android.util.SparseArray;

public class UrlResolver {
    // public static String BaseUrl = "http://192.168.1.23:9000/";
    public static String BaseUrl = "http://dlpdevelopment-env.ap-south-1.elasticbeanstalk.com/";
    public static String BaseUrl1 = "http://35.154.12.82:3000/";
    public static String BaseUrl2 = "http://13.127.176.185/LivCRM/index.php";
    public static SparseArray<String> urldata = null;
    public static SparseArray<String> urldata1 = null;
    public static SparseArray<String> urldata2 = null;
    public static String API_SCRATCH_STATUS = BaseUrl1 + "scratchstatus";
    public static String API_GET_COUPON = BaseUrl1 + "getcoupon";
    public static String API_SCRATCH_POINTS_CHECK = BaseUrl1 + "pointscheck";
    public static String API_SCRATCH_POINTS_CHECK_DELHI = BaseUrl1 + "pointscheckdelhi";
    public static String API_CHECK_VERSION = BaseUrl1 + "versioncode";
    public static String LOG_DETAIL = BaseUrl1 + "logdetail";
    public static String CONVERSION = BaseUrl1 + "conversion";
    public static String CHECcalcltpntK_KYC = BaseUrl1 + "usercheck";
    public static String KYC_DETAILS = BaseUrl1 + "userdetail";
    public static String EMAIL_CHECK = BaseUrl1 + "emailcheck";

    public static final String path(int item) {
        if (urldata == null) {
            urlmapper();
        }
        String url = BaseUrl.concat(urldata.get(item));
        return url;
    }

    public static final String path1(int item) {
        if (urldata1 == null) {
            urlmapper1();
        }
        String url = BaseUrl1.concat(urldata1.get(item));
        return url;
    }

    public static final String path2(int item) {
        if (urldata2 == null) {
            urlmapper2();
        }
        String url = BaseUrl2.concat(urldata2.get(item));
        return url;
    }

    private static void urlmapper() {
        urldata = new SparseArray<String>();
//        urldata.put(GetUrl.Login, "v1/login/");
//        urldata.put(GetUrl.SetPassword, "v1/setpassword/");
//        urldata.put(GetUrl.ForgotPassword, "v1/forgotpassword/");
//        urldata.put(GetUrl.ProfileUpdate, "v1/profileupdate/");
        urldata.put(GetUrl.Notification, "v1/notification/");
        urldata.put(GetUrl.Product, "v1/dealerproduct/");
//        urldata.put(GetUrl.Warranty, "v1/productwarranty/");
        urldata.put(GetUrl.Targets, "v1/getlatestofferscheme/");
//        urldata.put(GetUrl.Edit_Profile_Pic, "v1/profilepicupload/");
//        urldata.put(GetUrl.Banner, "v1/getproductbanner/");
//        urldata.put(GetUrl.Target_Detail, "v1/productloyalty/");

        urldata.put(GetUrl.Segment, "v1/carsegment/");
        urldata.put(GetUrl.Manufacturer, "v1/carmanufacturer/");
        urldata.put(GetUrl.CarModel, "v1/carmodel");
        urldata.put(GetUrl.SerialToUnique, "v1/serialtounique/");
        urldata.put(GetUrl.GetCitites, "v1/getcities/");
        urldata.put(GetUrl.DealerService, "v1/dealerservice/");
//        urldata.put(GetUrl.GetStock, "v1/getsku/");
        urldata.put(GetUrl.GetRedeemHistory, "v1/dealerredeem/");
        urldata.put(GetUrl.CalculateLoyaly, "v1/newcalculateproductloyalty/");
//        urldata.put(GetUrl.GetRedeemSlab, "v1/getredeemslab/");
    }

    private static void urlmapper1() {
        urldata1 = new SparseArray<String>();
        urldata1.put(GetUrl.Warranty, "serialnumber");
        urldata1.put(GetUrl.Login, "login");
        urldata1.put(GetUrl.GetWarranty, "logdetail1");
        urldata1.put(GetUrl.Loyality, "totalloyalty");
        urldata1.put(GetUrl.SCHEMEURL, "schemeurl");
        urldata1.put(GetUrl.WARRANTYURL, "schemetrurl");
        urldata1.put(GetUrl.ERICKURL, "schemeurlerick");
        urldata1.put(GetUrl.Warranty_Redeem, "addredeemslabwarranty");
        urldata1.put(GetUrl.TargetWarranty_Detail, "calculatorinputs");
        urldata1.put(GetUrl.CalculateWarrantyLoyaly, "calcltpnt");
        urldata1.put(GetUrl.HomeCount, "getDealerpoints1");
        urldata1.put(GetUrl.ADDERICKPRODUCT, "erickpurchase");
        urldata1.put(GetUrl.ADDPRODUCT, "dealerproduct");
        urldata1.put(GetUrl.Banner, "getproductbanner");
        urldata1.put(GetUrl.GetStock, "getsku");
        urldata1.put(GetUrl.Profile_Update, "profileupdate");
        urldata1.put(GetUrl.Edit_Profile_Pic, "profilepicupload");
        urldata1.put(GetUrl.SetPassword, "setpassword");
        urldata1.put(GetUrl.ForgotPassword, "forgotpassword");
        urldata1.put(GetUrl.GetRedeemSlab, "getredeemslab");
        urldata1.put(GetUrl.Target_Detail, "productloyalty");
    }

    private static void urlmapper2() {
        urldata2 = new SparseArray<String>();
        urldata2.put(GetUrl.BATTERYTYPE, "?entryPoint=Drona");
    }

    public interface GetUrl {
        int Login = 0;
        int SetPassword = 1;
        int ForgotPassword = 2;
        int ProfileUpdate = 3;
        int Notification = 4;
        int Product = 5;
        int Warranty = 6;
        int Targets = 7;
        int Edit_Profile_Pic = 8;
        int Banner = 9;
        int Target_Detail = 10;
        int Profile_Update = 11;
        int Segment = 12;
        int Manufacturer = 13;
        int CarModel = 14;
        int SerialToUnique = 15;
        int GetCitites = 16;
        int DealerService = 17;
        int GetStock = 18;
        int GetRedeemHistory = 19;
        int CalculateLoyaly = 20;
        int GetRedeemSlab = 21;
        int GetWarranty = 22;
        int Loyality = 23;
        int SCHEMEURL = 24;
        int BATTERYTYPE = 25;
        int WARRANTYURL = 26;
        int Warranty_Redeem = 27;
        int TargetWarranty_Detail = 28;
        int CalculateWarrantyLoyaly = 29;
        int ADDERICKPRODUCT = 30;
        int ERICKURL = 31;
        int HomeCount = 32;
        int ADDPRODUCT = 33;
    }

    public static class JobType {
        static final String DEALERSTATECITY = BaseUrl1 + "dealerstatecity";
        static final String SCHEMEURLDELHI = BaseUrl1 + "schemeurldelhi";
        static final String APIMAINTAINEDCOUNT = BaseUrl1 + "apimaintainedcount";
        static final String SECONDARYPOINTSDELHI = BaseUrl1 + "secondarypointsdelhi";
        static final String ERICKPOINTSWB = BaseUrl1 + "api_erick";
        static final String DELHISECONDARYREDEEM = BaseUrl1 + "delhisecondaryredeem";
        static final String WBSECONDARYREDEEM = BaseUrl1 + "geterickslabs";
        static final String ADD_TOKEN = BaseUrl1 + "addtoken";
        static final String TAGBIN = BaseUrl2 + "?entryPoint=tagbin";
    }
}
