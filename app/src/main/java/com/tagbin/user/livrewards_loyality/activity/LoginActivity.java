package com.tagbin.user.livrewards_loyality.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.gson.JsonObject;
import com.tagbin.user.livrewards_loyality.Interface.RequestListener;
import com.tagbin.user.livrewards_loyality.Model.ErrorResponseModel;
import com.tagbin.user.livrewards_loyality.Model.LoginModel;
import com.tagbin.user.livrewards_loyality.Model.LoginResponseModel;
import com.tagbin.user.livrewards_loyality.R;
import com.tagbin.user.livrewards_loyality.helper.Controller;
import com.tagbin.user.livrewards_loyality.helper.JsonUtils;
import com.tagbin.user.livrewards_loyality.helper.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    Button login_button;
    TextView forgot_password;
    EditText login, password;
    String strlogin, strpassword;
    boolean flag = false;
    ProgressBar progressBar;
    RelativeLayout first_time_rl;
    TextView click_here;
    View.OnClickListener firstTimeClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this, BatCodeAvtivity.class);
            intent.putExtra("first_time", "user");
            startActivity(intent);
        }
    };
    RequestListener loginlistener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) {
            Log.d("response login", responseObject.toString());
            final LoginResponseModel responsemodel = JsonUtils.objectify(responseObject.toString(), LoginResponseModel.class);
            if (responseObject.toString().equalsIgnoreCase("Invalid credentials or Invalid Password")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, "Invalid credentials or Invalid Password", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {

                final PrefManager pref = new PrefManager(LoginActivity.this);
                pref.saveLoginModel(responsemodel);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        if (responsemodel == null) {
                            Toast.makeText(LoginActivity.this, "Something went wrong ! Please Try Again Later", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (responsemodel.getToken() != null && responsemodel.getData().getVerification_status().equals("Verified")) {
                            Answers.getInstance().logCustom(new CustomEvent("Login")
                                    .putCustomAttribute("BAT_CODE", pref.getLoginModel().getData().getUsername() + "")
                                    .putCustomAttribute("USER_NAME", pref.getLoginModel().getData().getFirst_name()));
                            Intent intent1 = new Intent(LoginActivity.this, ProductDetailActivity.class);
                            startActivity(intent1);
                            finish();
                        } else {
                            Intent intent = new Intent(LoginActivity.this, Change_password_activity.class);
                            intent.putExtra("login", "login_Activity");
                            intent.putExtra("firstTime", "firstTimeUser");
                            startActivity(intent);
                            pref.setForgot(true);
                        }
                    }
                });
            }


        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("message", message);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(LoginActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            logout();
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            if (errorResponseModel != null && errorResponseModel.getErr() != null && !errorResponseModel.getErr().isEmpty()) {
                                Toast.makeText(LoginActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(LoginActivity.this, "Invalid credentials or Invalid Password", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login_button = findViewById(R.id.loginbutton);
        forgot_password = findViewById(R.id.tvForgotPassword);
        login = findViewById(R.id.username);
        password = findViewById(R.id.password);
        first_time_rl = findViewById(R.id.first_time_rl);
        click_here = findViewById(R.id.clickhere_tv);

        SpannableString content = new SpannableString("Click here");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        click_here.setText(content);
        first_time_rl.setOnClickListener(firstTimeClick);
        progressBar = findViewById(R.id.progressbar);
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strlogin = login.getText().toString().trim();
                strpassword = password.getText().toString().trim();
                if (strlogin.equals("")) {
                    login.setError("Enter User Name or Email");
                } else if (strpassword.equals("")) {
                    password.setError("Enter Password");
                } else {
                    LoginModel loginModel = null;
                    if (flag == false) {
                        loginModel = new LoginModel();
                        loginModel.setBat_code(strlogin);
                        loginModel.setPassword(strpassword);
                        loginModel.setJob_type("login");
                    }
                    PrefManager pref = new PrefManager(LoginActivity.this);
                    pref.saveLoginModel1(loginModel);
                    progressBar.setVisibility(View.VISIBLE);
                    Controller.manualLogin(LoginActivity.this, loginModel, loginlistener);

                }
            }
        });
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(LoginActivity.this, BatCodeAvtivity.class);
                intent1.putExtra("forgot", "forgotPass");
                startActivity(intent1);
                finish();
            }
        });
    }

    private void logout() {
        PrefManager pref = new PrefManager(this);
        pref.clearSession();
        Intent go = new Intent(this, LoginActivity.class);
        startActivity(go);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

}
