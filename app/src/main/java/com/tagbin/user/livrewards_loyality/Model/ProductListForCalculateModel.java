package com.tagbin.user.livrewards_loyality.Model;

/**
 * Created by Nav on 19-Aug-17.
 */

public class ProductListForCalculateModel {
    String id, quantity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
